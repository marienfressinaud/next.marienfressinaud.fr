(function () {
    function setTheme(themeValue) {
        if (themeValue === 'clair') {
            document.documentElement.classList.add('theme-clair');
            document.documentElement.classList.remove('theme-sombre');
        } else if (themeValue === 'sombre') {
            document.documentElement.classList.add('theme-sombre');
            document.documentElement.classList.remove('theme-clair');
        } else {
            document.documentElement.classList.remove('theme-sombre', 'theme-clair');
        }
    }

    function initForm() {
        var select = document.querySelector('#theme');
        select.addEventListener('change', (e) => {
            var themeValue = e.target.value;

            if (themeValue !== 'auto' && themeValue !== 'clair' && themeValue !== 'sombre') {
                themeValue = 'auto';
            }

            localStorage.setItem('theme', themeValue);
            setTheme(themeValue);
        });

        var currentThemeValue = localStorage.getItem('theme');
        if (currentThemeValue !== 'auto' && currentThemeValue !== 'clair' && currentThemeValue !== 'sombre') {
            currentThemeValue = 'auto';
        }

        select.value = currentThemeValue;
        setTheme(currentThemeValue);
    }

    initForm();
    document.addEventListener('turbolinks:load', function() {
        initForm();
    });
}());
