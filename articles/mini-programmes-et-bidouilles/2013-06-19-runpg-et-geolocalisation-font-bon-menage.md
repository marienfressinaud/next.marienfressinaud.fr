Date: 2013-06-19 19:20
Title: RunPG et géolocalisation font bon ménage
Slug: runpg-et-geolocalisation-font-bon-menage
Tags: rpg, runpg, java
Summary: En 2013, dans le cadre d’un projet pour mon école, je développais RunPG : une application qui transforme votre dépense d’énergie en points d’expérience dans un jeu style RPG. Le projet était assez rigolo mais je ne l’ai jamais continué.

Voilà bien longtemps que je n’avais rien écrit et aujourd’hui je m’en viens vous parler donc d’un petit projet : [RunPG](https://github.com/marienfressinaud/RunPG). Il s’agit d’un projet web que j’ai réalisé pour l’Ensimag, mais avant tout c’est surtout un petit PoC de jeu (Proof of Concept, un petit jeu permettant de mettre en application une idée) auquel je pensais depuis quelques temps. L’idée donc était à la base de "faire courir les geeks" (ou pas). Le concept est simple : vous allez courir avec votre smartphone, une application (web ici) enregistre votre parcours grâce à la géolocalisation et fait progresser votre personnage en fonction de votre vitesse / durée de course / distance parcourue. Pour compléter la présentation, je vais laisser la parole à mon README :

**Description**

RunPG est un jeu type RPG utilisant la géolocalisation pour faire évoluer votre personnage. Les quêtes consistent à parcourir un certain nombre de kilomètres en un minimum de temps. L’enregistrement se fait grâce à la version web de RunPG et utilise les possibilités de géolocalisation du téléphone et du HTML5.

**Les contraintes**

Il s’agit d’un projet web pour l’Ensimag, un certain nombre de contraintes m’ont été imposées :

- Utilisation du [framework Java, Play](http://www.playframework.com/) (obligatoire) : OK
- Gestion de droits avec différents niveaux d’utilisateurs (obligatoire) : OK (visiteur, joueur, admin)
- Gestion d’un flux RSS (optionnel) : OK (classement et joueur)
- Utilisation d’un webservice (souhaitable) : OK (maps et données de OpenStreetMaps)
- Une partie du site en GWT (souhaitable) : NOK (choix assumé)
- Version mobile de l’application (optionnel) : OK (tout le site est full responsive design)

Voilà donc ce qu’il y a dire sur le projet. Il me faut quand même préciser quelques petites choses :

- Comme je le disais plus haut il s’agit d’un PoC, je n’ai donc pas poussé les fonctionnalités jusqu’au bout. L’administration par exemple est vraiment très basique, ne permettant que de supprimer des joueurs ou de modifier leur score / expérience. Néanmoins, la base est là. Il s’agissait en effet de vraiment se concentrer sur l’aspect géolocalisation vers évolution du personnage.
- On ne peut pas courir plus de 30 minutes sinon la requête qui synchronise votre parcours avec le serveur est trop grosse (trop de points GPS) et ne passe pas (oups :$)
- Il n’y a (pour le moment) aucun serveur hébergeant ce mini-jeu
- Mais [le code de RunPG est disponible sur Github](https://github.com/marienfressinaud/RunPG)
- J’ai placé le tout sous licence [Copyheart](http://copyheart.org) mais tout ce qui est images et code incorporé garde sa licence d’origine
- Le tout est basé sur le framework Java, Play (version 2.1.1), basé sur les concepts MVC. Il vous faudra donc installer le framework sur votre PC. Je pense qu’en s’y connaissant un peu, on prend rapidement ses marques pour comprendre le code. Si jamais je dois reprendre le concept un jour néanmoins, ce sera en Python (j’ai envie de me mettre à ce langage pour de bon ☺)
- Si vous êtes intéressés pour reprendre le code / l’idée du jeu, n’hésitez pas à me poser des questions (notamment pour la phase de débogage qui peut être assez pénible)

Et pour finir, quelques images de la bête :

![L’accueil](images/runpg/accueil.png)

L’accueil

![La page de classement](images/runpg/classement.png)

La page de classement (oui il n’y a que quatre joueurs)

![La vue mobile lorsqu’on enregistre un parcours](images/runpg/mobile.png)

La vue mobile lorsqu’on enregistre un parcours (j’utilise ici le simulateur de Firefox OS, très utile lors du débogage)

![La page d’administration](images/runpg/admin.png)

La page d’administration (très basique)

Amusez-vous bien ! ☺
