Date: 2014-10-31 19:51
Title: En novembre, c’est NaNoWriMo
Slug: en-novembre-cest-nanowrimo
Tags: nanowrimo
Summary: Novembre 2014, le mois où je me remettais à écrire. Le NaNoWriMo fut une expérience enrichissante bien qu’éprouvante. Le roman écrit lors de cet événement n’a jamais été publié (ni divulgué) mais je prévois d’écrire des nouvelles basées sur des parties de l’histoire d’origine.

Alors que j’ai débuté depuis mi-septembre mes neuf mois sabbatiques sur les chapeaux de roues (développement de FreshRSS principalement), le mois de novembre est maintenant à ma porte.

Comme je l’avais évoqué dans mon article du 14 juin, je me lance dans le NaNoWriMo (que j’abrègerai par NaNo désormais). Le NaNo c’est quoi ? C’est avant tout un défi personnel. Chaque mois de novembre, des milliers de personnes se lancent dans l’écriture d’un roman d’au moins 50 000 mots. Il n’y a pas de sujet imposé, pas de contrainte particulière, seulement 50 000 mots à écrire en l’espace d’un mois.

Ayant découvert ce défi l’année dernière à travers les expériences de [Ploum](https://ploum.net/9-ans-de-blog-le-defi-nanowrimo/) et de [Pouhiou](http://www.framablog.org/index.php/?q=Nanowrimo), j’ai eu envie de me lancer pour renouer avec une passion que j’avais au collège et au lycée : l’écriture. J’ai donc eu un an pour me motiver et, maintenant, on y est ! Le sujet de mon histoire est désormais arrêté. Je ne détaillerai pas le scénario ici (ni même de visu, j’attends d’avoir fini l’histoire et d’en être satisfait pour cela ! ;)) mais ce sera de la science fiction et l’histoire se nommera probablement *Asmara*.

Pour le suivi de ce défi, j’essayerai d’écrire régulièrement ici pour tenir au courant mais comme je me connais, je risque de ne pas être très productif. Le mieux reste encore de venir faire un tour sur Diaspora* (vous pouvez vous inscrire sur [Framasphère](https://framasphere.org/), mon identifiant est marienfr@diaspora-fr.org) où je serai probablement plus bavard. Un autre avantage de Diaspora* c’est qu’il y a tout un tas de gens qui se sont lancés dans le NaNo cette année que vous pouvez rencontrer en suivant le tag [#NaNoWriMo](https://framasphere.org/tags/nanowrimo).

Pour finir ce court article, j’ai vraiment hâte de voir le résultat final. Cela doit faire plus de cinq ans que je n’ai pas pris le temps d’écrire une vraie histoire et je me demande vraiment comment je vais gérer le rythme (je me suis fixé 2 000 mots par jour). Vous pourrez me retrouver sur le site du Nano sous le nom de… [Marien Fressinaud](http://nanowrimo.org/participants/marien-fressinaud) !
