---
title: Turbolinks
date: 2022-02-06 21:25
---

Je continue de faire des améliorations sur le site et je viens d’y ajouter [Turbolinks](https://github.com/turbolinks/turbolinks). Le but est d’accélérer le chargement des pages et d’éviter les petits glitchs visuels en les chargeant intelligemment en JavaScript. La navigation était devenue gênante dans certaines situations[^1] avec [le thème sombre](theme-sombre.html).

[^1]: Lorsque vous naviguiez sur une nouvelle page, si votre navigateur déclarait préférer le thème clair mais que vous forciez le thème sombre, la page « clignotait » rapidement.

Turbolinks n’est plus maintenu, remplacé par [Turbo](https://turbo.hotwired.dev/) depuis 2021. Je n’avais toutefois besoin que de la partie « Drive » qui correspond plus ou moins à Turbolinks. J’économise ainsi quelques kilo-octets.

J’en ai également profité pour activer la compression de certains fichiers côté serveur, histoire d’accélérer un peu plus le chargement des pages. Ça devrait être suffisamment rapide pour avoir la sensation de naviguer en local :)