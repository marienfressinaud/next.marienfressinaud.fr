---
title: Introspection professionnelle : valeurs et raison d’être
date: 2018-12-28 11:45
---

Comme je le disais dans [un précédent article](boop-une-introduction.html), la
fin de cette année 2018 est pour moi l’occasion de reposer à plat mes
motivations professionnelles. Si j’ai décidé de quitter mon emploi chez
[Sogilis](http://sogilis.com) afin de tracer ma propre route, c’est pour
différentes raisons parmi lesquelles :

- le sens que je ne trouvais pas à mon travail ;
- des problèmes sociétaux qui me parlent de plus en plus (ex. urgence
  climatique, fractures sociale et démocratique) ;
- mon implication dans [Framasoft](https://framasoft.org) qui m’a fait réaliser
  la dimension politique qui sous-tend le développement logiciel ;
- [des](http://www.mcpaccard.com/) [rencontres](http://www.maiwann.net/)
  [multiples](https://estcequecestdutravail.xyz/) qui m’ont aidé à envisager
  mon travail sous un autre angle ;
- de nombreuses discussions avec des amis proches qui n’ont fait que renforcer
  mes choix.

J’envisage aujourd’hui le travail que je peux fournir uniquement à travers un
prisme politique (au sens d’engagement). Les logiciels que je conçois ont un
impact sur les personnes qui les utilisent, **il est important pour moi qu’ils
fonctionnent à leur service et non pas contre eux**.

C’est ce genre de réflexions que j’aimerais arriver à faire transparaître sur
mon site. Le but est de le transformer en « vitrine » professionnelle  en
mettant en avant mes valeurs, mon mode de fonctionnement, mes compétences, etc.
Mais avant d’entamer cette refonte, je souhaite passer par une phase de
réflexions plus approfondie.

## Le design de soi, ou l’introspection professionnelle

J’ai découvert le concept de « **design de soi** » il y a un peu plus d’un an
grâce à [Marie-Cécile Paccard](http://www.mcpaccard.com/) lorsque je l’ai
contactée par rapport à [Lessy](https://lessy.yuzu.ovh). Le but est d’effectuer un
travail de recherche afin de mieux connaître ses aspirations et de mieux
communiquer en conséquence. Vous pouvez en apprendre plus dans [sa présentation](https://speakerdeck.com/mcpaccard/se-reinventer-et-etre-soi-a-lere-du-numerique)
ou via [la présentation de Marie Guillaumet](https://marieguillaumet.com/design-de-soi-paris-web-2015/)
qui en a parlé lors de l’évènement [Paris Web](https://www.paris-web.fr/) 2015.

J’étais personnellement un peu dubitatif sur ce que pouvait m’apporter
concrètement une telle démarche. Je me suis toutefois prêté au jeu pour Lessy
et le fait de voir émerger le contenu aussi naturellement pour la page
d’accueil et les éléments destinés à la communauté m’a encouragé à retenter
l’expérience sur moi-même (en le documentant, cette fois-ci !)

Sur le sens toutefois, je suis peu à l’aise avec l’expression « design de
soi ». D’une part, le mot « design » continue de véhiculer une confusion avec
le graphisme ; d’autre part, et de façon plus ennuyante, même en prenant le
mot dans son sens de « conception », ce n’est pas tellement l’objet de la
démarche. Cela pourrait en effet laisser penser que le « design de soi » vise à
créer (concevoir) une nouvelle personne qui ne correspond pas forcément à
l’ancienne. Or, je crois que le but est de tirer des traits qui nous
correspondent, à la manière d’un caricaturiste qui cherche à représenter une
personne à travers quelques caractéristiques qui ressortent. Dans la suite de
ma série d’articles, je troquerai donc l’expression par celle d’introspection
professionnelle. Dans mon cas, le but est bien d’**identifier les traits de ma
personnalité** sur lesquels je souhaite communiquer dans un cadre
professionnel.

Pour cela, je vais procéder en plusieurs étapes qui devraient me permettre de
créer le contenu du site :

1. définition de **mes valeurs** ;
2. définition de **ma raison d’être** ;
3. définition de **mes compétences** ;
4. établissement d’**un objectif** personnel (c’est-à-dire comment j’honore ma
   raison d’être) ;
5. définition de mes moyens de **communication** ;
6. conception de **l’architecture du site**.

Le dernier point me permettra d’organiser les différents éléments qui auront
émergé lors des phases précédentes et potentiellement d’en faire émerger de
nouveaux.

Les deux premiers points sont traités dans la suite de cet article.

## Des valeurs pour guider mes choix

J’ai en vérité entamé la première phase, celle de la définition de mes valeurs,
il y a plusieurs mois. Je souhaitais voir comment la perception que j’avais de
moi-même évoluait dans le temps, identifier les éléments qui revenaient,
comprendre pourquoi je n’étais pas à l’aise avec d’autres, etc. À titre
d’exemple, je n’ai pas conservé l’« éthique » dans mes valeurs car ça ne
signifiait pas grand chose pour moi : tout le monde possède une éthique sans
pour autant placer la même chose derrière. Revendiquer avoir une éthique est
nécessairement perçu à travers un prisme différent selon les personnes avec qui
j’intéragis.

À l’éthique, j’ai préféré le terme **d’intégrité**. Affirmer son intégrité
permet de garder un cap et de faciliter la définition de ses priorités. Par
exemple, j’ai comme conviction que les licences libres ainsi que le respect de
la vie privée sont des conditions *sine qua non* pour qu’un logiciel soit
réellement au service de ses utilisateurs et utilisatrices (c’est-à-dire qu’il
fasse bien ce qu’on lui demande, sans malveillance). Vous ne me verrez donc pas
demain accepter une mission qui ne respecte pas ces conditions parce qu’elle
est mieux payée qu’une autre. Cette notion d’intégrité sera par la suite
**complétée par un manifeste** qui détaillera mieux mes engagements.

**Le respect** dans mes échanges ou envers les utilisatrices et utilisateurs
d’un logiciel m’apparaît ensuite nécessaire à toute relation. Rester à
l’écoute, savoir exprimer ses sentiments, considérer la personne en face sur un
pied d’égalité ; je n’imagine pas d’autre manière de fonctionner pour tirer le
meilleur de mes échanges. D’un point de vue technique cela signifie aussi, pour
moi, considérer les techniques d’expérience utilisateur et d’accessibilité
comme intrinsèques à un projet, pas seulement comme des ajouts selon un budget
sur lequel je n’ai pas la main. À l’inverse, d’un point de vue global, cela
signifie prendre aussi en compte les externalités de ce que je développe : hors
de question de développer un outil qui nuirait encore un peu plus à
l’environnement par exemple.

Enfin, **la transmission** doit faire partie de chaque processus de création.
Après avoir lu un peu autour du [concept de
compostabilité](https://ressources.osons.cc/?PetitPrecisDeCompostabiliteDesprojets)
en septembre (via la super présentation de [Claire Zuliani à Sud
Web](https://marges.clairezuliani.com/good_job/2018/05/27/melange-cultures-professionnelles.html)),
j’ai mieux réalisé la finitude de notre présence au sein d’un groupe.
Transmettre mes connaissances et mes compétences m’est alors apparu comme bien
plus essentiel pour ne plus être un rouage « indispensable » sans lequel rien
ne fonctionne. Mécaniquement, j’ai le sentiment que cela pousse aussi à plus
d’humilité. Nous devenons uniques non pas par nos compétences techniques mais
par notre vécu.

En me limitant à trois valeurs, je me suis forcé à creuser toujours plus ce qui
compte vraiment pour moi. J’ai écarté plusieurs points qui me tenaient à cœur
mais que je n’arrivais pas à définir correctement (notamment le principe de
remise en question). Ces valeurs sont évidemment fluctuantes : il s’agit d’un
travail sur moi-même à un instant T et je m’attends à redéfinir leur contour
dans le futur. En revanche, **je ne pense pas m’en départir à moins de changer
fondamentalement la personne que je suis**.

## Une raison d’être pour comprendre où je vais

Se trouver une raison d’être n’est pas la chose la plus évidente à faire. C’est
pourtant une question qui me taraude depuis un moment car elle questionne sur
le sens que l’on souhaite donner à sa vie. Lorsque j’ai pris une année
sabbatique en 2015, c’est quelque chose qui m’a travaillé sans que je n’y
trouve une réponse qui me convenait.

Ces derniers mois ont toutefois été plus efficaces, au point que j’arrive à la
résumer en une courte phrase : **concevoir les outils dont la société aura
besoin demain**. Pour comprendre cette raison d’être, il me paraît important de
bien définir ce dont je parle.

Tout d’abord, lorsque je parle de « concevoir [des] outils », je fais le choix
conscient de ne pas utiliser le terme de « développer ». Pour moi, **un
logiciel n’est pas une fin en soi**. Il y a avant tout une phase d’acquisition
des besoins et de réflexion qui peut notamment mener à la conclusion qu’un
outil informatique ne serait pas une bonne solution. Cela implique aussi que je
dois étendre mes compétences au delà de la technique informatique, en acquérant
notamment plus de compétences liées à l’expérience utilisateur. Je dispose pour
cela de précieuses ressources, notamment deux articles de Raphaël
Yharrassarry : « [Compétences <abbr>UX</abbr> et modèle en T](https://blocnotes.iergo.fr/articles/competences-ux-et-modele-en-t/) »
et « [Se former en <abbr>UX</abbr> Design ?](https://blocnotes.iergo.fr/articles/se-former-en-ux-design/) ».

Ensuite, lorsque je parle de « demain », j’y cache derrière la notion de
« [collapsologie](https://fr.wikipedia.org/wiki/Collapsologie) » (ou
d’effondrement). Je ne me suis pas encore énormément penché sur le sujet, mais
il revient régulièrement dans les discussions qui m’entourent. Je ressens dans
mes échanges **une notion d’urgence et d’inévitabilité d’une société en
mutation profonde**. La question n’est pas forcément de savoir « quand ?»,
encore moins « si ? », mais plutôt « comment ?». À quoi ressembleront nos
sociétés dites « modernes » dans un monde où les catastrophes écologiques
prédominent, où la faune et la flore fondent comme neige au soleil et où les
mouvements migratoires s’accélèrent ? Et si l’on se prenait en main pour
imaginer nous-mêmes ce monde de demain et l’accompagner en douceur ? Il existe
trois axes que j’ai identifié et dans lesquels j’aimerais m’investir :

1. accompagner l’inévitable désastre écologique ;
2. aider les populations en lutte pour affirmer leur légitimité aux yeux du
   reste de la société ;
3. repenser nos espaces démocratiques et l’expression du pouvoir.

Vaste programme que celui-là, j’ignore encore quelles formes peuvent prendre
mes actions dans chacun de ces domaines mais je ne me fais pas de soucis pour
savoir identifier les missions qui entreront en résonance avec ces sujets.

Enfin, le tout fait référence à ce qui se produit dans la sphère technologique
depuis des années : les outils qui sont conçus n’émanent pas de réels besoins
mais le deviennent une fois bien installés. <span lang="en">Apple</span> est
notamment championne dans ce domaine, imposant avec brio et comme évidentes des
technologies comme le <span lang="en">smartphone</span> ou la tablette. On peut
toutefois questionner leurs répercussions sur notre société comme une
accoutumance à l’instantanéité ou une addiction aux écrans difficiles à jauger.
C’est le fameux « [temps de cerveau humain disponible](https://fr.wikipedia.org/wiki/Temps_de_cerveau_humain_disponible) »
qui est attaqué ici. En vérité, **le futur de nos sociétés est aujourd’hui
dessiné par des sociétés commerciales** qui souhaitent avant tout vendre leurs
produits plutôt que de réfléchir au bien commun ; cela me gêne tout
particulièrement.

## Une conclusion partielle pour préparer une suite plus concrète

Ce premier article était un peu abstrait : je n’ai abordé ici que des sujets
qui me serviront de base pour construire le futur contenu du site. Cet article
m’est apparu un peu pompeux à plusieurs reprises lors de sa rédaction, mais je
crois que le résultat est avant tout un travail honnête sur mes aspirations et
me permettra réellement d’affirmer ma manière de travailler avec d’autres
personnes.

Je crois ces engagements indispensables dans nos sociétés toujours plus
numériques, où nous perdons peu à peu la main sur nos données et leur
traitement. Je suis partisan de réinsuffler plus d’échanges humains et une
vision positive dans nos vies numériques, et donc nos vies tout court.

Cet article a une saveure particulière pour moi car j’ai réussi à y condenser
toutes les réflexions que je mène depuis plusieurs mois ; réflexions notamment
alimentées par mes échanges au sein de [Framasoft](https://framasoft.org/) qui
est, pour une bonne partie, à l’origine des graines qui commencent à germer sur
ce site.

Le prochain article devrait aborder des sujets un peu plus concrets et mener
notamment à la rédaction de contenu qui apparaîtra par la suite sur le site.
