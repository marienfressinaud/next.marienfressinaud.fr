---
title: Introspection professionnelle : compétences et objectif
date: 2019-01-02 15:15
---

En 2019, je change de vie professionnelle. Afin d’accompagner ce changement, je
souhaitais entreprendre la refonte de mon site pour qu’il mette mieux en avant
mes aspirations. Dans [mon précédent article](introspection-professionnelle-valeurs-et-raison-detre.html),
j’ai décrit mes valeurs et ma raison d’être (c’est-à-dire ce qui me motive au
quotidien), à travers une démarche d’introspection professionnelle. Pour
rappel, les différentes étapes que je prévois dans le cadre de cette démarche
sont :

1. définition de **mes valeurs** ;
2. définition de **ma raison d’être** ;
3. définition de **mes compétences** ;
4. établissement d’**un objectif** personnel ;
5. définition de mes moyens de **communication** ;
6. conception de **l’architecture du site**.

Les deux premiers points ont donc été détaillés dans le précédent article, je
vais désormais aborder les deux points suivants, à savoir mes compétences et
mon objectif personnel afin d’étudier comment je compte mettre en œuvre ma
raison d’être. Dis autrement, je vois mes compétences comme des outils à mon
service pour atteindre un objectif qui saura satisfaire ma raison d’être et mes
valeurs.

Aujourd’hui mes compétences sont essentiellement techniques, orientées
spécifiquement dans le développement web. À ce niveau-là, je suis plutôt
polyvalent et capable d’apprendre assez aisément. Ce qui m’importe toutefois
aujourd’hui concerne plutôt le champ de compétences que je **souhaite** être
capable de couvrir en totale autonomie. J’ai relevé trois points principaux
(vous remarquerez que j’aime bien ce chiffre !) ainsi que quatre transversaux
pour arriver à **un total de sept compétences que je pourrai mettre en œuvre au
sein d’un projet**.

## Des compétences fondamentales pour définir mon métier

Tout d’abord, je souhaite monter en compétences dans le domaine de
**l’acquisition des besoins métiers orientés utilisateurs**. Je pense que c’est
une démarche saine que de d’abord chercher à comprendre les enjeux d’un métier
et en mesurer sa complexité avant de développer une solution logicielle. Cela
permet notamment d’être force de propositions pertinentes. J’ai par ailleurs
trop souvent vu des clients qui fonçaient bille en tête avec une idée bien
définie, sans questionner les usages des autres utilisateurs et utilisatrices.
J’ai dans ces moments-là eu l’impression de ne développer que pour une personne
et que l’outil ne serait jamais pleinement utilisé ou adopté. Je crois que ce
n’est pas une démarche toujours évidente que de questionner un client sur ce
qu’il a muri en lui-même, en particulier en tant que développeur, et de lui
faire comprendre que **sa solution n’est pas forcément la meilleure pour tout
le monde**. Il sera essentiel que je sache établir une confiance quant à ma
capacité à comprendre le métier.

Ensuite, la deuxième compétence que je saurai mettre en pratique au sein d’un
projet est celle du **développement logiciel**. Comme je le disais plus haut,
il s’agit d’un domaine où je suis polyvalent et capable d’apprendre ; c’est le
cœur de mon expertise aujourd’hui. Je suis à l’aise à la fois en <span lang="en">frontend</span>
comme en <span lang="en">backend</span>. Je connais un certain nombre de bonnes
pratiques en matière de déploiement automatisé/continu et connais relativement
bien les problèmes qui peuvent se poser aux administrateurs système. J’essaye
notamment de mettre en pratique les recommandations de la méthodologie
« [<span lang="en">The Twelve-Factor App</span>](https://12factor.net/) ».
Ainsi, lorsque je développe une fonctionnalité, je pense à ce qu’elle deviendra
une fois en production. Dans cette optique, j’essaye aussi de faire en sorte
que ce que je développe arrive le plus rapidement possible en production afin
d’éviter un décalage trop important entre mon code et la base initiale. En
terme de langages, cela a relativement peu d’importance pour moi vu que je suis
capable d’apprendre si la situation le demande. J’ai toutefois plus
d’expérience en Ruby (très orienté Ruby on Rails), Python, JavaScript et PHP.
Il s’agit là de compétences que **je maîtrise suffisamment pour être autonome
sur un projet sans aucun soucis**. Bien entendu, mon code s’accompagne de tests
et je fonctionne souvent en <abbr>TDD</abbr> (pour [<span lang="en">_Test-Driven
Development_</span>](https://fr.wikipedia.org/wiki/Test_driven_development)).
Par ailleurs, j’ai un attachement particulier aux logiciels simples, qui ne
cherchent pas à faire trop de choses et dont on peut fouiller le code pour y
trouver rapidement ce que l’on cherche.

Enfin, la dernière compétence fondamentale que j’aimerais mettre en application
au sein des projets où j’aurai à intervenir porte sur **l’accompagnement à la
mise en place d’une méthodologie documentaire**. J’en parlais lorsque je
détaillais mes valeurs, la transmission des connaissances est primordiale pour
qu’un groupe ne dépende pas de quelques personnes clés. Je crois qu’il y a
aujourd’hui un manque énorme de culture écrite dans nos modes de fonctionnement
(je l’ai retrouvé à différents niveaux professionnel et associatif) ou alors
cette culture n’est pas orientée vers le partage et la conservation des
connaissances. C’est un sujet que j’ai commencé à toucher du doigt et sur
lequel j’aimerais véritablement progresser. Et pour progresser, rien de tel que
de pratiquer. Les wikis et les blogs sont pour moi deux outils très puissants
dans cette recherche du partage mais qui ont le défaut de figer la connaissance
à un instant T. J’aimerais étudier **des modes de fonctionnement qui font vivre
cette connaissance**, qui lui permettent véritablement d’évoluer dans le temps
et qui replacent la documentation au centre des préoccupations.

## Des compétences transverses, au bénéfice des projets

En plus des trois compétences fondamentales listées au-dessus et qui forment un
tout dans la vie d’un projet, j’ai voulu ajouter quatre compétences que je
peux appliquer de façon générale sur tout type de projet.

Tout d’abord, **une communication que je décrirais de transparente**. Je
parlais plus haut de confiance à établir avec les client·es, cela passe avant
tout par la communication (écrite comme orale). Il est notamment important de
ne pas cacher des éléments dont on pourrait avoir « honte ». Par exemple, la
production est tombée, on l’a réparée en catimini sans que la cliente n’en
sache rien mais on la tient quand même informée de ce qu’il s’est passé, par
transparence. Savoir communiquer peut notamment servir à désamorcer des sujets
qui pourraient être tabous et donc dangereux dans la vie d’un projet. Afin de
communiquer de façon efficace, j’essaye de me placer tout d’abord dans une
position d’écoute. Cette position me sert à comprendre mon interloteur ou
interlocutrice, comprendre comment il ou elle intéragit avec moi, quel degré de
familiarité je peux me permettre, si elle est sensible à mon argumentaire ou
s’il est plutôt fermé à des approches différentes. Ce n’est pas toujours chose
aisée, mais **en exprimant aussi ce que l’on ressent, en partageant les doutes
qui peuvent surgir en nous**, on arrive à poser un terrain propice à la
collaboration.

La deuxième compétence dont je pense pouvoir tirer partie est ma capacité à
**rendre compte de façon claire et argumentée** un travail que j’ai produit.
Que ce soit à l’oral ou à l’écrit, j’essaye toujours de me rendre
compréhensible et je n’hésite pas à reformuler si je sens que je ne suis pas
clair. Cela peut toutefois me demander un certain temps de préparation pour me
laisser le temps de répéter ou de me relire au minimum deux à trois fois. Je
suis par ailleurs très attaché à la pratique de la langue française et j’aime
jouer sur les nuances que peuvent porter des mots ou la construction d’une
phrase. Cela peut paraître anecdotique vis-à-vis d’autres compétences, mais
j’ai réalisé il y a peu que le fait de **savoir exprimer correctement ses
idées** joue un rôle très important dans la façon dont on est perçu par les
autres. Cela affecte notre capacité à convaincre et à être suivi.

Ensuite, **la gestion de projet** intervient à tout les instants d’un projet.
Afin d’être efficaces, il me semble que [les méthodes agiles](https://fr.wikipedia.org/wiki/M%C3%A9thode_agile)
sont idéales. Je n’ai commencé à pratiquer qu’en arrivant au sein de Sogilis où
j’ai pratiqué un mélange de [<span lang="en">Scrum</span>](https://fr.wikipedia.org/wiki/Scrum_(d%C3%A9veloppement))
et d’[<span lang="en">Extreme programming</span>](https://fr.wikipedia.org/wiki/Extreme_programming),
mais j’ai très rapidement saisi un certain nombre d’avantages que ces pratiques
offraient. Ce que j’en retire avant tout, c’est le fait de partir du constat
que nos méthodes de travail ne fonctionneront pas parfaitement au démarrage
d’un projet, et donc d’intégrer dans le processus de travail les outils qui
permettront d’améliorer et corriger le processus lui-même, en cours de route.
De plus, et cela rejoint mon premier point, la communication est placée au
centre des préoccupations ce qui permet une bonne compréhension des enjeux par
tout le monde et facilite la détection des problèmes qui ne manquent pas
d’arriver au fil de l’eau. **Les méthodes agiles offrent donc un cadre sain et
malléable** pour gérer et faire vivre un projet, elles seront donc au cœur de
ma façon de travailler.

Enfin, le dernier point que je souhaitais aborder est **l’intégration aux
équipes**. Il est pour moi important de faire partie d’une équipe et de ne pas
travailler dans mon coin. Cela aide à mieux communiquer (encore !), à partager
les connaissances pour ne pas devenir indispensable et à améliorer la qualité
du produit final. La confrontation des points de vue sur un sujet donné, un
bout de code par exemple, aide à **penser un problème sous différents angles et
prendre du recul sur ce que l’on fait**. Je n’ai quasiment jamais travaillé ces
trois dernières années tout seul sur un projet, et même sur [Lessy](https://lessy.yuzu.ovh)
qui reste un projet très personnel, j’ai eu à cœur de demander conseil autour
de moi. Je pense être capable de m’intégrer facilement à une équipe, mais cela
dépend aussi beaucoup des personnalités au sein de l’équipe ; je ne saurais
donc en faire une règle absolue.

## Un objectif au service de ma raison d’être

Arrive donc le moment où je dois résumer les quelques 3 200 mots de mes deux
articles abordant mon introspection professionnelle ainsi qu’un an de
réflexions plus ou moins poussées, en une phrase d’accroche qui définira la
façon dont je veux aborder mon travail durant les prochains mois et/ou
prochaines années.

Après plusieurs reformulations, je suis arrivé à déterminer un objectif qui me
convient plutôt bien :

> J’accompagne les humain·es de **notre société de demain** dans une démarche
> de résilience en participant à **la conception de leurs outils numériques**
> et en les aidant à **améliorer leurs méthodes de travail**, le tout dans **un
> esprit de sobriété**.

J’ai essayé d’y insuffler un maximum d’éléments issus des étapes précédentes
sans pour autant en abuser pour que le message reste clair. Tout d’abord, la
notion de « société de demain » fait directement référence à un monde
post-effondrement que j’évoquais dans mon article précédent. J’y fais aussi
référence en tentant de dessiner une première esquisse d’une société que je
souhaite **résiliente et sobre**. La sobriété fait aussi référence à ma volonté
de développer des logiciels simples et « bidouillables ». Mes compétences sont
lisibles au creux de mon objectif : la conception d’outils numériques et
l’amélioration des méthodes de travail qui incluent les méthodes agiles et une
démarche de documentation. Aussi, je m’inscris dans **une position de
complémentarité** (à l’inverse d’être indispensable) des équipes déjà en place en
« accompagnant », « participant » et en « aidant ».

Cet objectif pourra être amené à évoluer dans le futur, mais j’espère qu’il
saura m’accompagner dans mes premières missions et qu’il permettra de poser sur
la table des sujets de discussion que l’on aborde trop peu dans le monde de la
technologie. J’espère qu’il véhicule correctement le message politique que je
lui ai imaginé.

J’entrevois d’ailleurs déjà des limites à cet objectif très orienté vers un
métier que je pense voué à être marginalisé à long terme. Métier par ailleurs
pratiqué par et pour une population privilégiée dans le sens où elle possède et
maitrise des outils informatiques. Je crois que ces outils doivent
s’accompagner d’une démarche complémentaire **au risque d’abandonner sur le
bord de la route les personnes les moins à l’aise avec le numérique**.
L’informatique reste un outil à notre service, en aucun cas il ne s’agit d’une
fin en soi. Je souhaiterai à terme évoluer vers un métier plus ancré dans le
réel. Aujourd’hui je vais toutefois profiter des compétences acquises durant
ces dernières années pour orienter le domaine au sein duquel j’agis (le
développement logiciel, principalement) vers un horizon qui me semble plus
enviable. Ce sera l’occasion aussi de rencontrer des personnes qui pourront me
guider vers des voies que je n’imagine pas encore. Mon objectif, tout imparfait
soit-il, est une image à cet instant précis de mes envies et une piste à
explorer pour pouvoir imaginer et construire « autre chose » par la suite.

Avec cet objectif défini, je considère que le gros du travail introspectif est
terminé. Il reste toutefois quelques étapes supplémentaires avant d’attaquer la
refonte du site à proprement parler. J’aborderai dans le prochain article les
deux derniers points de ma démarche, à savoir de quelle manière je souhaite
communiquer et, enfin, la conception de l’architecture du site.
