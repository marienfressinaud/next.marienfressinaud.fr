---
title: Alléger
date: 2021-02-07 13:00
---

J’ai parlé dans mon précédent article, [2021](2021.html), de ma volonté de
retrouver une part d’anonymat sur Internet. À l’époque je n’étais pas sûr de
savoir ce que cela signifierait pour ce site. J’envisageais notamment de
simplifier la page d’accueil et de virer le blog. J’ai trouvé une solution qui
me convenait mieux.

**[La page d’accueil](index.html) a effectivement subi un ravalement de façade
conséquent :** mon nom, 4 phrases et quelques liens. C’est largement suffisant
pour me présenter.

**J’ai conservé [le blog](blog.html). En revanche, j’ai décidé de garder dans
la liste principale uniquement les articles récents (6 mois et moins).** J’y ai
adjoint une seconde liste d’articles sélectionnés (parce qu’ils me parlent et
qu’ils racontent quelque chose sur moi). Celle-ci évoluera sans doute ;
j’aimerais n’en lister qu’une dizaine maximum. Enfin, j’ai conservé les séries
parce qu’elles recontextualisent certains articles. Finalement, peu d’anciens
articles ne sont plus accessibles directement, mais j’aime l’idée que certains
tombent dans une forme d’oubli. Ils existent toutefois toujours afin de ne pas
casser les liens pointant vers eux.

De manière plus globale, j’ai allégé visuellement quelques éléments : l’en-tête
est plus simple, le pied de page ne contient plus qu’une poignée de liens. J’ai
aussi réorganisé quelques éléments pour mettre l’emphase sur le contenu
« utile ».

Enfin, il n’y a plus de liens vers mes comptes de réseaux sociaux, seulement
mon adresse courriel sur la page d’accueil. Inversement, j’ai retiré les liens
vers mon site depuis mes comptes de réseaux sociaux. **C’est une manière de
cloisonner mes identités en ligne.** Je continuerai sans doute de partager des
articles là-bas, mais ça ne sera pas automatique. Pour suivre le blog,
[privilégiez les flux de syndication](abonnement.html).

Je suis heureux de ces changements qui simplifient beaucoup de choses. Le site
est maintenant plus aligné avec ce que je veux partager en ligne. Reste plus
qu’à reprendre l’écriture d’articles 😏
