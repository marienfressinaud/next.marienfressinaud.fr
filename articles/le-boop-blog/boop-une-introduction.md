---
title: Boop! Une introduction
date: 2018-10-29 20:00
---

Cette fin d’année 2018 marque pour moi un tournant professionnel. Après avoir
travaillé trois années chez [Sogilis](http://sogilis.com/), j’ai décidé, pour
diverses raisons, de sortir du sentier qui se dessinait devant moi pour me
mettre à mon compte. Ce changement s’accompagne pour moi d’une nécessaire
remise en question de mes motivations et engagements : à quoi souhaité-je
consacrer mon temps ?

Pour accompagner ce cheminement, j’ai souhaité créer un nouveau site à partir
de zéro. Si je ne sais pas encore exactement ce qu’il contiendra, je sais que
j’ai envie de renouer avec mes premiers pas sur Internet : c’est-à-dire
l’apprentissage et le partage de connaissances.

L’une des choses que j’ai tout de suite aimé en informatique, c’est cette
capacité que l’on a de pouvoir nous forger facilement nos propres outils. Mon
site Internet en est un que j’ai longtemps laissé de côté mais que je
souhaitais ramener dans ma stratégie pour contrôler mon image numérique. Pour
cela, j’aurais pu continuer d’utiliser [Pelican](https://getpelican.com/) que
j’utilisais jusque-là, mais quelque chose me titillais : et si j’en profitais
pour comprendre comment fonctionne un générateur de sites statiques ? La
mécanique interne ne me semble pas si compliquée…

## Un générateur de sites statiques, pour quoi faire ?

L’une des premières choses à laquelle j’ai pensé a été de me faire un site
uniquement à base de HTML et CSS. Quitte à repartir de zéro, autant utiliser
les technologies les plus simples du web ; aucun élément généré dynamiquement,
uniquement du code moulé au clavier. Mais je me suis vite rendu compte que cela
deviendrait très pénible à maintenir : un changement dans la structure HTML ?
C’est toutes les pages qu’il faudra modifier. Un nouvel article ? Il faudra
écrire le flux RSS à la main. Beurk.

L’avantage du générateur de sites statiques est qu’il peut rester léger tout en
automatisant certaines tâches et en facilitant la maintenance. Certaines
fonctionnalités deviennent toutefois plus compliquées à gérer, notamment la
gestion des commentaires et la recherche, mais le compromis me semble
acceptable.

## Les premiers pas de <span lang="en">Boop!</span>

Je sous-entendais plus haut que je n’y connais rien en générateur de sites
statiques. En vérité, <span lang="en">Boop!</span> a déjà quatre mois ! En
effet, j’ai développé début juillet une première version destinée à générer [ma
galerie de photos](https://photos.marienfressinaud.fr) (le code est disponible
sur [Framagit](https://framagit.org/marienfressinaud/photos.marienfressinaud.fr)).

Son but n’était alors pas tout à fait le même, et comme il s’agissait surtout
d’une preuve de concept, ça ne me dérange pas plus que ça de repartir de zéro.
À terme, j’aimerais que les fonctionnalités de la première version soient
intégrées dans ce que je vais désormais développer.

## Implémentation d’une version zéro

Bien, maintenant que je sais que je veux me faire un site qui sera généré par
un outil du doux nom de <span lang="en">Boop!</span>, il est temps de rentrer
dans le vif du sujet : par quoi je commence pour obtenir une version zéro ?

Le principe de base d’un générateur de sites statiques est qu’il lit le contenu
depuis un répertoire source, qui est passé dans une moulinette qui va
transformer ce contenu en fichiers HTML pour les déposer ensuite dans un
répertoire destination.

Pour cette première version de <span lang="en">Boop!</span>, j’ai décidé de
rester extrêmement simple : les fichiers sources seront des fichiers HTML et
CSS. Il n’y a donc pas besoin de les transformer et on peut se contenter de les
copier dans le répertoire destination. Oui OK, ça ne sert à rien pour le
moment, mais c’était histoire de me mettre dans le (petit) bain.

Le code écrit en Python est disponible sur [Framagit](https://framagit.org/marienfressinaud/boop/blob/0.0.0/boop.py)
avec le tag `0.0.0`.

Les esprits taquins (et techniques) noteront peut-être que cette première
version, aussi simple soit-elle, n’est pas exempt de bugs : on corrigera cela
dans la prochaine version !

## La mise en ligne du site

Une fois la version zéro de <span lang="en">Boop!</span> prête, il est temps de
réaliser la première version du site. Une [page HTML](https://framagit.org/marienfressinaud/marienfressinaud.fr/blob/8f1ea7653b1c6856afa53effda32e33d7288641a/content/index.html)
et un [fichier CSS](https://framagit.org/marienfressinaud/marienfressinaud.fr/blob/8f1ea7653b1c6856afa53effda32e33d7288641a/content/style/herisson.css)
plus tard, le site est prêt pour être mis en ligne. C’est minimaliste mais ça
fait bien le boulot tout en étant lisible.

Pour la mise en ligne, je dispose déjà d’un serveur web capable de servir du
contenu statique (un serveur [nginx](http://nginx.org/)), je n’ai qu’à ajouter
une entrée DNS de `next.marienfressinaud.fr` vers mon serveur et ajouter le
fichier de configuration qui va bien pour nginx :

```nginx
# version simplifiée
server {
  listen 80;
  listen [::]:80;

  server_name next.marienfressinaud.fr;

  root /var/www/next.marienfressinaud.fr;
  index index.html;
}
```

Il ne me reste plus qu’à faciliter le téléversement des fichiers générés par
<span lang="en">Boop!</span> sur le serveur. Pour cela j’ai créé un fichier
[`Makefile`](https://framagit.org/marienfressinaud/marienfressinaud.fr/blob/8f1ea7653b1c6856afa53effda32e33d7288641a/Makefile)
contenant quatre règles :

- `build` qui appelle <span lang="en">Boop!</span> pour générer le contenu ;
- `clean` qui supprime les fichiers générés dans `./output` ;
- `publish` qui téléverse sur mon serveur avec `rsync` les fichiers générés ;
- `help` qui affiche une aide générée automatiquement pour la commande `make`
  (si je ne dis pas de bêtise, cette commande est issue à la base [de chez
  Marmelab](https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html)).

Avec ça, j’ai donc désormais un site disponible à l’adresse [next.marienfressinaud.fr](http://next.marienfressinaud.fr/).
Rien de bien compliqué, mais on a déjà les bases pour la suite.

## Prochaines étapes

La suite d’ailleurs, quelle est-elle ? J’ai déjà noté quelques besoins liés à
l’écriture de cet article :

- j’ai dû dupliquer la structure du HTML depuis le fichier `index.html`, si
  cela reste gérable avec deux fichiers, je vais vite avoir envie de factoriser
  mon code ;
- pour l’instant j’ai ajouté le lien vers cet article de blog à l’accueil
  manuellement, ce serait bien si je pouvais automatiser cela ;
- je n’ai pas de flux RSS, ce qui va se révéler nécessaire pour que des
  personnes extérieures s’abonnent au blog ;
- lié à l’ajout d’un flux RSS, je vais devoir dissocier le contenu des articles
  de la structure HTML.

Lorsque cela sera prêt, je pourrai alors aborder des sujets moins techniques
pour expliquer où je veux emmener ce nouveau site.
