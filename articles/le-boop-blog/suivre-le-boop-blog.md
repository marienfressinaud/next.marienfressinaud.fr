---
title: Suivre le Boop! blog
date: 2018-12-13 11:10
---

J’ai décidé de faire vite pour ajouter le flux d’actualités à <span lang="en">Boop!</span>,
mon générateur de sites statiques, et c’est donc chose faite. Vous pouvez
désormais **vous abonner à ce site** en ajoutant l’adresse dans votre
[agrégateur d’actualités préféré](https://freshrss.org) (qui devrait être
capable de trouver l’adresse du flux tout seul, si si, mais si ça ne marche pas
le flux est accessible [ici](http://marienfressinaud.fr/feeds/all.atom.xml)).

Petit retour sur comment je m’y suis pris.

## Distinguer les pages des articles

La première étape à laquelle je me suis trouvé confronté a été de distinguer
les pages (en l’occurence, la page d’accueil) des articles. Jusqu’à maintenant,
je n’avais pas eu besoin de faire une telle distinction, mais je ne veux pas me
trouver avec la page d’accueil dans mon flux <abbr>RSS</abbr>. Pour cela,
**j’ai réorganisé toute la structure des répertoires**, en m’inspirant cette
fois-ci de ce qui pouvait se faire dans [Jekyll](https://jekyllrb.com/docs/structure/)
(au lieu de [Pelican](https://blog.getpelican.com/) qui me servait de base
jusqu’à maintenant).

Auparavant, tout le contenu du site était déposé dans un répertoire `./content`
puis copié dans `./output` (via une phase de transformation dans le cas des
fichiers <span lang="en">Markdown</span>), désormais la structure est la
suivante :

- un fichier `index.html` à la racine ;
- les articles, nécessairement au format <span lang="en">Markdown</span>, sont
  dans un répertoire `./articles` (c’est original) ;
- le <span lang="en">*template*</span> des articles se trouve dans
  un répertoire `./templates` (toujours très original) ;
- les fichiers à copier tels quels, comme les fichiers <abbr>CSS</abbr>, sont
  placés dans `./static` ;
- et le tout est copié/généré dans un répertoire `./site`.

Je trouve personnellement cette structure un poil plus compliquée que la
précédente, mais beaucoup plus explicite sur **les intentions de chaque
répertoire**. Vous pouvez voir à quoi cela ressemble [en suivant ce lien](https://framagit.org/marienfressinaud/marienfressinaud.fr/tree/f3200d89523d4a4ed618310170bcb5c946ceca25).

Vous pouvez aussi voir le [<span lang="en">commit</span> introduisant le
changement](https://framagit.org/marienfressinaud/boop/commit/c37fe01d8da8a71cb16c1de649e342dcbb92c4d0#b8c037ff8ab16c431251e16173af8e9ec6b1cbcf)
sachant qu’il en existe quelques autres mais que je n’ai pas envie de faire un
lien vers chacun d’entre eux.

Dorénavant, les articles sont donc identifiables aisément car déposés dans un
répertoire spécifique. J’ai décidé, en plus de cela, de les abstraire dans une
classe particulière (`Article`) pour qu’ils soient plus faciles à manipuler par
la suite ([voir le <span lang="en">commit</span>](https://framagit.org/marienfressinaud/boop/commit/303d11926116109ac8929a7bfb6d254839dcaa6d)).

## Récupérer les informations manquantes via la configuration

Les flux <abbr>RSS</abbr> et Atom demandent **des informations supplémentaires**
que je ne pouvais pas encore récupérer, notamment l’<abbr>URL</abbr> et le
titre du site. Je devais aussi **figer certains attributs présents dans les
métadonnées** des articles <span lang="en">Markdown</span> comme le titre,
l’auteur et la date de publication. Par ailleurs, j’ai voulu **correctement
gérer les fuseaux horaires** sachant la complexité que cela pouvait être par la
suite. Il me fallait donc aussi récupérer le fuseau de l’auteur (pour ma part,
`Europe/Paris`).

Je suis tout d’abord parti sur un simple fichier de configuration au format
<abbr lang="en">YAML</abbr> pour indiquer les informations générales du site et
alimenter une variable `configuration` ([voir le <span lang="en">commit</span>](https://framagit.org/marienfressinaud/boop/commit/39c7a809ad6f39e0f9c0295c3c58345ae30a135c)).

J’ai ensuite créé quelques méthodes sur ma classe `Article` pour accéder
facilement aux informations stockées dans les métadonnées. Ça leur donne ainsi
un rôle particulier et me permet de faire des manipulations dessus à la volée
([voir le <span lang="en">commit</span>](https://framagit.org/marienfressinaud/boop/commit/e0e669e186047d2d94043b34061238600d20f3ee)).

Enfin, les fuseaux horaires sont gérés grâce au paquet externe [pytz](https://pypi.org/project/pytz/).
Connaissant la complexité du sujet, je ne me suis pas amusé à développer mon
propre module donc, là encore, petite entorse à ma règle de ne pas dépendre de
code externe.

Les informations sont toutes facultatives, des valeurs par défaut étant
générées à chaque fois, mais il est tout de même recommandé de les renseigner.

## Générer le flux Atom, enfin !

Avec toutes ces informations en main, je pouvais enfin générer le flux
<abbr>RSS</abbr> tant attendu. Mais encore un petit choix à faire avant : il
existe deux formats « concurrents » pour générer des flux d’actualités,
[<abbr>RSS</abbr>](https://fr.wikipedia.org/wiki/RSS) et [Atom](https://fr.wikipedia.org/wiki/Atom_Syndication_Format).
D’un point de vue simple utilisateur, les deux se valent et les deux sont très
bien supportés par tous les agrégateurs d’actualités. **Mon choix s’est porté
sur Atom** qui se voulait être pendant un temps une amélioration de
<abbr>RSS</abbr>, mais surtout parce que mon site précédent utilisait déjà ce
format. Et pourquoi pas les deux ? Parce que ça ne servirait à rien et que j’ai
mieux à faire de mon temps, tout simplement.

Il ne me restait donc plus qu’à générer un document <abbr>XML</abbr> suivant le
format Atom et l’écrire dans un fichier dans le répertoire `./site` ([voir le
<span lang="en">commit</span>](https://framagit.org/marienfressinaud/boop/commit/edfcf8220f725c1b4f3d5099b191d17ea807a5cd)
suivi de près [par un autre](https://framagit.org/marienfressinaud/boop/commit/c9eff5a1c0df05b97c6162505e1bf7d5a8ddaee4)
destiné à abstraire la génération du <abbr>XML</abbr> dans un module nommé
<span lang="en">Boopfeed</span>).

Et voilà, plus qu’à [incorporer un lien](https://framagit.org/marienfressinaud/marienfressinaud.fr/commit/8a43fca999a3b0a237e7cf7377f5718a4e5dfe19)
dans l’en-tête de mes fichiers <abbr>HTML</abbr> vers ce fichier Atom, et c’est
terminé !

## Dans la tête d’un développeur

Bien entendu, je n’ai pas développé tout cela d’une traite. Entre poser à plat
les problèmes que posaient l’implémentation initiale, décider d’une nouvelle
structure après plusieurs essais, réfléchir à la meilleure manière de récupérer
les informations manquantes, etc., ce travail m’a pris une journée entière
durant laquelle je n’ai pas chômé.

Si [mon historique <span lang="en">Git</span>](https://framagit.org/marienfressinaud/boop/commits/master)
paraît relativement linéaire, c’est que j’ai pris le soin de le réécrire avant
de le publier, cela pour deux raisons :

- **un rôle pédagogique**, si de futurs développeuses ou développeurs veulent
  comprendre ce que j’ai fait, l’enchaînement des <span lang="en">commits</span>
  leur paraîtront plus logiques ;
- **un rôle de débogage**, si jamais j’ai introduit des <span lang="en">bugs</span>
  à un moment donné, il sera plus simple de retrouver à quel moment et pour
  quelle raison.

J’ai d’ailleurs **fonctionné de manière totalement inverse** lors du
développement. En effet, j’ai commencé à écrire le code générant le flux Atom,
en mettant les valeurs manquantes en dur dans le code, puis en le retravaillant
au fur et à mesure.

Je fais cette petite précision pour les développeurs et développeuses moins
expérimenté·es qui pourraient se dire qu’il leur est impossible d’arriver à ce
résultat de façon aussi directe. Ça l’est effectivement, mais pour les autres
aussi !

## La technique, c’est fini

Je suis donc arrivé à un point du développement de <span lang="en">Boop!</span>
qui me satisfait. Il manque bien évidemment plein de choses, et notamment il
n’est pas possible de créer des pages qui ne soient pas des articles ; cela
viendra plus tard.

Je souhaite désormais me consacrer au contenu du site car il ne sert à rien
d’avoir une belle vitrine si ce qu’il transmet est creux et vide de sens. Les
prochains articles devraient donc porter sur **des réflexions plus
personnelles** : quel but je souhaite donner à ma vie, quelles sont mes
valeurs, quelles pratiques je compte appliquer, etc. ; tout cela pour concevoir
une vitrine cohérente avec mes aspirations à la fois personnelles et
professionnelles.
