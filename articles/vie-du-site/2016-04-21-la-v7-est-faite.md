Date: 2016-04-21 00:00
Title: La v7 est faite !
Slug: la-v7-est-faite
Tags: màj, python
Summary: Mon site existe depuis 2010. Il a vécu avec moi quasiment toute ma scolarité dans le supérieur. Cette 7e version du site vient encore une fois tout remettre à plat en adoptant une philosophie plus minimaliste. Fini Shaarli, au revoir galerie photos et [bonjour Pelican](http://getpelican.com)&nbsp;!

La précédente version du site qui tournait sous PluXML m’aura survécu plus de deux ans et demi, un record pour moi. Malheureusement, force est de constater que je ne publiais plus grand chose. L’envie me manquait certes un peu, mais j’avoue que devoir me connecter à une interface administateur m’a toujours beaucoup ennuyé alors que je pouvais rédiger mes articles tranquillement sur mon PC.

Écrire en Markdown, le transformer en HTML, vérifier que tout fonctionne, penser à *téléverser* les images sur le serveur&nbsp;: non seulement c’était casse-pieds, mais en plus c’était idiot&nbsp;! En effet cela fait un an que j’ai mis en place un blog pour FreshRSS (certes pas très actif) tournant [sous Pelican](http://getpelican.com) et j’avais déjà réalisé un thème auparavant pour ce moteur de sites statiques, pourquoi ne pas en profiter&nbsp;?

Prenant mon courage à deux mains, j’ai donc réalisé un nouveau thème à partir de zéro. Objectif&nbsp;: faire quelque chose de simple et joli. Le résultat se trouve sous vos yeux. Pour l’anecdote, je bosse sur cette nouvelle version depuis janvier et j’ai d’ailleurs failli me convaincre moi-même de faire appel à un professionnel pour le design&nbsp;! Ça aurait été tout de même dommage ;).

Profitant de ce changement je me suis dit que, tant qu’à faire, je pouvais bien faire le tri dans les articles. Ce sont donc 89 articles qui viennent vous dire au revoir. Les 33 articles qui ont survécu correspondaient tous à l’une des trois catégories du blog (informatique, nouvelles et vie du site) et possédaient un intérêt dans mon parcours personnel. Pour chacun d’eux j’ai pris le temps d’écrire un très court résumé (ok, ils sont pour beaucoup identiques), de réparer les liens cassés, de virer des parties plus du tout d’actualité et de corriger quelques fautes d’orthographe. C’était long, c’était fastidieux et j’espère bien que c’est la dernière fois que je fais ça&nbsp;!

Aussi, la partie blog se trouve reléguée en seconde partie du site. La raison est que si je n’ai pas envie de publier pendant plusieurs mois / années, cela se verra un peu moins que lorsque la liste des articles stagnait en première page :).


Pour terminer, je vous propose trois captures d’écran. Il s’agit respectivement des versions 3 (22 novembre 2011, merci [Internet Archive](https://archive.org/web/)), 6 (17 avril 2016) et 7 (21 avril 2016) de ce site.

![capture d’écran du site marienfressinaud.fr le 22 novembre 2011](images/site/v3.png)

![capture d’écran du site marienfressinaud.fr le 17 avril 2016](images/site/v6.png)

![capture d’écran du site marienfressinaud.fr le 21 avril 2016](images/site/v7.png)

Notez que le lien de l’ancien flux RSS est redirigé vers [le nouveau](http://marienfressinaud.fr/feeds/all.atom.xml).
