Date: 2013-08-29 14:22
Title: Une v6 qui carbure
Slug: une-v6-qui-carbure
Tags: màj
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

6 mois. 6 mois seulement que j'avais fait évoluer mon site vers sa cinquième version. Faut dire que j'étais parti dans un petit délire qui rendait le tout pas très pratique à manipuler. Mais c'est de l'histoire ancienne ! Aujourd'hui j'officialise cette v6 de mon cher petit site.

Qu'est ce qui se cache donc sous le capot cette fois-ci ?

## Un blog PluXML

Ça y est, j'ai enfin décidé qu'il était temps d'arrêter de vouloir tout redévelopper alors qu'il existe des CMS et des logiciels qui font les choses très bien et même mieux. Depuis le début de ce site je me suis refusé à utiliser un moteur de blog tel que [PluXML](http://www.pluxml.org/), Dotclear ou Wordpress parce que je voulais faire les choses moi-même. Force est de constater qu'il est beaucoup plus simple d'utiliser le produit d'un autre quand il est maintenu correctement.

PluXML a tout pour lui : léger, sans base de données, facile à adapter à ses besoins, système de plugins, (vraiment) bien codé. Je peux enfin respirer après des années à utiliser une interface d'administration pour mon site pas terrible.

J'ai quand même pu récupérer l'ensemble de mes anciens articles pour les gérer via PluXML. Bien sûr, les URL pour y accéder ont changé et certains liens dans les articles ne fonctionneront plus. Et vu que cette fois j'ai la flemme de tout vérifier, ça restera ainsi (sauf si la motivation revient).

## Un Shaarli

[C'est un grand retour.](marienfressinaud.fr/shaarli/) Alors que je l'avais abandonné durant la v5 pour un outil fait maison, j'ai assez vite déchanté. Les bugs et l'interface (là encore assez peu glamour) m'ont forcé à repasser à [Shaarli](http://sebsauvage.net/wiki/doku.php?id=php:shaarli). Mêmes explications que plus haut, ne pas avoir à se charger de la maintenance d'un produit est une épine enlevée du pied.

Je vais donc pouvoir recommencer à partager mes liens comme avant ! :)

Les plus attentifs remarqueront que j'ai abandonné le flux unifié de mes articles de blog / liens partagés. À vrai dire, le fait que mes liens arrivent en première page, de la même manière qu'un article, me gênait. En effet, un lien partagé est censé être moins important qu'un article et devrait rester en seconde page.

## Un design fait avec amour

Je parlais de design glamour, et ceux qui connaissent l'interface standard d'un Shaarli me diront que s'en est tout l'inverse. Le design par défaut de PluXML n'est pas non plus des plus enthousiasmant. Et le design précédent de ce site était sans commune mesure plus laid que les autres (alors qu'étrangement j'avais réussi à faire quelque chose de pas mal avec la v4).

Heureux que vous devez être alors d'apprendre que je vous propose au téléchargement le design tout beau (enfin j'espère) que j'ai développé pour ce site. Une partie pour PluXML et une autre pour Shaarli. Attention tout de même pour le Shaarli, j'ai fait des modifications dans le `index.php` et j'ai remanié entièrement les fichiers TPL (la création d'un thème à part entière pour Shaarli n'est pas encore au point).

Pour les couleurs, le fantastique site [Flat UI Colors](http://flatuicolors.com/) m'a énormément aidé. Je me suis aussi inspiré de ce que j'ai fait pour FreshRSS pour compléter. Bref, j'espère que ça vous plaira !

## Un nouveau départ

Je n'écrit jamais beaucoup sur ce site / blog (à peu près un article par mois depuis le début de l'année !). J'espère que cette nouvelle version avec un vrai moteur de blog va me motiver un peu plus. De toute façon, ce site reste mon site et ma vitrine, aucune chance que je l'abandonne lâchement ! :D

Si toutefois vous souhaitez (me) lire, j'essaye d'être plus régulier (et plus compréhensible) sur [mon blog dédié à mon voyage en Norvège](http://marienfressinaud.fr/norge/).
