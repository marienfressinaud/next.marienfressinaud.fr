Date: 2012-05-19 15:48
Title: Le changement, c’est maintenant !
Slug: le-changement-c-est-maintenant
Tags: màj
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

Au moins pour le design de mon site. Comme je le disais dans un article précédent, je suis un éternel insatisfait du design de mon site. Mais bon, cette fois-ci c’est la bonne ! (jusqu’à la prochaine) Au menu, une simple épuration du haut du site, l’utilisation d’une police d’écriture plus agréable à l’œil, et une révision de l’affichage des articles du blog.

Sans oublier quelques corrections de bugs et d’une grosse (grosse) faille (je considère que l’affichage de la configuration pour accéder comme on veut à la base de données est une faille ☺)
