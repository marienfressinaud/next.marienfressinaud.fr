---
Date: 2018-05-01 16:00
Title: Ce qui nous pousse au Libre
Slug: ce-qui-nous-pousse-au-Libre
Tags: logiciel libre, réflexions, featured
Summary: Suite à la lecture d’un article de Maiwann sur comment réconcilier *designers* et logiciels libres, j’ai eu envie d’y répondre à travers une suite de plusieurs articles. Ce premier article revient sur mes motivations à faire du Libre en tant que développeur.
---

Il y a quelques jours, Maiwann proposait dans un article, de [réconcilier
*designers* et logiciels libres](http://maiwann.net/blog/designers-&-logiciels-libres-si-on-collaborait/).
L’article ne manque pas d’intérêt, ne serait-ce que par ses suggestions
d’actions. Bien que je partage bon nombre des constats, je souhaitais le
« compléter » d’un point de vue de développeur. Je vous propose donc d’entamer
une petite mise en perspective à travers mes expériences personnelles, que ce
soit celles sur [FreshRSS](https://freshrss.org), [Lessy](https://lessy.yuzu.ovh),
les actions menées au nom de [Framasoft](https://framasoft.org) ou encore à
travers les écrits que j’ai pu lire à droite à gauche.

J’ai décidé de découper ces réflexions en une suite de plusieurs articles (sans
me forcer à tenir sur la durée). Et comme l’idée me trottait depuis un moment,
j’en profite pour inaugurer une nouvelle catégorie sur ce blog intitulée
« Libres ? ».

Le premier sujet que je souhaite aborder en est un que Maiwann n’aborde
quasiment pas : **pourquoi faire du logiciel libre ?** J’aurais en effet aimé
mieux comprendre ce qui motive des *designers* à vouloir contribuer au Libre.
J’essaye donc dans mon article de faire le tour de ce qui peut pousser un
développeur à en faire, sans prétendre être exhaustif.

## Apprentissage

De l’article de Maiwann, la seule référence à une potentielle motivation se
trouve au détour d’un paragraphe :

> Lors de nos études, […] alors que nous cherchons à nous entraîner, sur notre
> temps libre ou pour des projets de fin d’année, nous nous plaignons de ne
> connaître aucun développeur avec qui co-créer des sites ou logiciels.

Voilà une raison qui devrait parler à bon nombre d’étudiants et d’étudiantes !
Appliquer ce que l’on a pu apprendre en cours et donc, par extension,
**apprendre par la pratique** est souvent moteur chez les développeurs. J’ai
moi-même développé un certain nombre de programmes avec cette simple
motivation. Par exemple, [Minz](https://github.com/marienfressinaud/MINZ) fut
ma tentative de comprendre le fonctionnement interne des frameworks web.
FreshRSS a été l’occasion de travailler véritablement en communauté, et donc en
équipe travaillant à distance et asynchrone. *Petit aparté* : paraît-il que ce
mode de travail est compliqué à mettre en place dans les boîtes, mais
cela se fait très naturellement sur les projets communautaires ; peut-être y
a-t-il des choses à en tirer ? Sur Lessy, j’ai pu consolider tout un paquet de
connaissances que j’ai ensuite pu proposer et appliquer au boulot. Le logiciel
libre est une formidable source d’apprentissage que je recommande fortement à
toutes et tous.

Cela étant dit, considérer l’apprentissage comme seul moteur dans le
développement d’un logiciel libre est bien entendu extrêmement réducteur et
j’aurais tendance à dire que ce n’est pas la raison principale (bien qu’il
s’agisse probablement de la porte d’entrée principale pour bon nombre d’entre
nous). Cherchons donc ailleurs d’autres raisons qui nous poussent, nous
développeurs et développeuses, à produire du logiciel libre.

## Plaisir

Dans le prologue du bouquin [*L’Éthique hacker*](https://fr.wikipedia.org/wiki/L%27%C3%89thique_hacker),
Linus Torvalds explique les motivations des hackers derrière le système
d’exploitation Linux comme ceci :

> La raison pour laquelle les hackers derrière Linux se lancent dans quelque
> chose, c’est qu’ils trouvent ça très intéressant et qu’ils veulent le
> partager avec d’autres. Tout d’un coup, vous avez le plaisir parce que vous
> faites quelque chose d’intéressant et vous avez aussi le pendant social.

Il nous dit plusieurs choses ici. Tout d’abord, le développement d’un tel
système relève avant tout **du plaisir**. Et il est vrai qu’on peut se demander
ce qui pousse des milliers de développeurs à partager leurs savoirs et leur
temps, généralement de façon gratuite, si ce n’est le plaisir de le faire ?
D’ailleurs Pekka Himanen (l’auteur du bouquin) cite un peu plus loin Éric
Raymond, à l’origine de la popularisation du terme « open source » (j’aurai
l’occasion de revenir sur ce terme plus tard) :

> La conception de logiciel et sa mise en œuvre devraient être un art
> jubilatoire, et une sorte de jeu haut de gamme. Si cette attitude te paraît
> absurde ou quelque peu embarrassante, arrête et réfléchis un peu. Demande-toi
> ce que tu as pu oublier. Pourquoi développes-tu un logiciel au lieu de faire
> autre chose pour gagner de l’argent ou passer le temps ?

On y retrouve la notion de plaisir à travers le « jeu haut de gamme ». Je
prends souvent l’exemple du Sudoku ou de la grille de mots-croisés : il n’y a,
à priori, aucune raison de remplir ces cases de chiffres ou de lettres, si ce
n’est le plaisir de résoudre un problème, parfois complexe. Je trouve
personnellement que le développement de logiciel peut amener à un état de
satisfaction similaire lorsqu’on se trouve face à un problème et qu’on arrive
finalement à le résoudre après plusieurs <del>heures</del> <del>jours</del>
semaines de recherche.

D’un point de vue personnel, j’ai toujours été attiré par les domaines de
« création ». J’ai immédiatement accroché au développement lorsque j’ai
découvert que créer un site web était aussi simple que créer un fichier texte
avec quelques mots dedans. Les balises HTML ? un simple jeu de légo. Le CSS ?
quelques directives de base à connaître et on arrive rapidement à quelque chose
de totalement différent. Un serveur web ? un ordinateur avec un logiciel
spécifique qui tourne dessus. Un bug ? une « chasse » durant laquelle on
déroule le programme qui nous semblait *si* logique au moment de l’écrire (mais
qui l’est maintenant beaucoup moins !). Pour moi, la beauté de l’informatique
réside dans sa simplicité et sa logique : il y a un véritable plaisir à
comprendre comment toutes ces petites boîtes s’agencent entre elles et que tout
devient plus clair.

## Partage

Si l’on se tient aux notions d’apprentissage et de plaisir, il n’y a rien qui
distingue le logiciel libre du logiciel propriétaire. Vous pouvez très bien
apprendre et éprouver du plaisir en développant du code fermé. Il nous faut
revenir à la citation de Torvalds pour commencer à percevoir ce qui les
différencie :

> […] ils veulent le partager avec d’autres.

Le **partage** : on a là une valeur fondamentale du logiciel libre qui ne
trouve pas véritablement son pendant du côté du logiciel propriétaire. Bien que
j’ai plus de mal à identifier clairement ce qui peut motiver l’être humain à
partager ses savoirs, c’est quelque chose que je ressens effectivement. Cet
aspect coopératif — Torvalds parle d’un « pendant social » — peut créer ou
renforcer des liens avec d’autres personnes ce qui rend cette activité
profondément humaine.

Partager, c’est donc transmettre. Transmettre à une communauté, donner les clés
pour que celle-ci soit indépendante. Partager ses savoirs qui permettront
peut-être à d’autres de bâtir autre chose par-dessus. Cela permet aussi de
créer du lien humain, rencontrer des personnes et ouvrir ses perspectives en
créant son propre réseau. C’est aussi s’offrir un coin de canapé quand on
voyage (coucou [Alex](https://f.a80.fr/profile/alkarex) 👋). Je me suis rendu
compte assez récemment de ce que m’offrait aujourd’hui cette décision en IUT de
partager les petits programmes que je pouvais développer sur mon temps libre.
La liberté n’est pas que celle du code.

Il y a certainement une forme de fierté à avoir exploré un domaine le premier,
ou développé une application que d’autres vont utiliser (« Quoi ? Ce que j’ai
fabriqué de mes propres mains t’est *aussi* utile ? »). Si cette fierté est par
essence un peu narcissique (je suis toujours un peu pénible lorsque je suis
cité [chez NextInpact](https://www.nextinpact.com/news/106286-intelligence-social-freshrss-veut-redonner-coup-fouet-aux-agregateurs-flux.htm)
ou chez [Korben](https://korben.info/lessy-gestionnaire-de-temps-ethique-respectueux.html) 😇),
elle est aussi bénéfique car elle encourage à rendre son travail public et
donc… partager encore.

## Éthique

On retrouve aussi cette notion de partage dans les écrits de Richard Stallman
lorsqu’[il nous parle des quatre libertés](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html)
du logiciel :

> Elles sont essentielles, pas uniquement pour les enjeux individuels des
> utilisateurs, mais parce qu’elles favorisent le partage et la coopération qui
> fondent la solidarité sociale.

Ces mots, pris du point de vue de Stallman, sont bien évidemment à interpréter
sous **la dimension éthique** (et donc **politique**) du logiciel libre, ce qui
n’est pas forcément le cas de Torvalds (je ne saurais néanmoins l’affirmer).
Puisque Stallman est à l’origine du mouvement du logiciel libre, on ne peut
évidemment pas enlever l’éthique de son équation ou alors vous obtenez de
l’open source (comme il l’explique dans l’article cité plus haut).
On peut toutefois raisonnablement penser que les partisans du logiciel libre
sont moins nombreux que ceux de l’open source, ce que j’explique par une peur
ou un désintérêt envers cet objet politisé.

Je trouve toutefois dommage de ne pas plus s’y intéresser. En effet, la
dimension éthique aide à répondre à une question que beaucoup de personnes
peuvent se poser : « **ce que je fais au quotidien a-t-il du sens ?** ».
Stallman y répond par la défense et le respect des utilisateurs et
utilisatrices :

> Le mouvement du logiciel libre fait campagne pour la liberté des utilisateurs
> de l’informatique depuis 1983.

Ou encore :

> Pour qu’on puisse dire d’un logiciel qu’il sert ses utilisateurs, il doit
> respecter leur liberté. Que dire s’il est conçu pour les enchaîner ?

Si je souhaitais conclure par cet argument, c’est parce qu’il aide à boucler la
boucle avec l’article de Maiwann. En effet, en tant qu’*UX designer*, elle va
avoir à cœur de répondre aux besoins de ses utilisateur·trices et donc
d’imaginer des mécanismes pour rendre l’outil le plus **utilisable** et
**accessible** possible. Aujourd’hui il me semble percevoir dans cette
communauté un mouvement de prise de conscience que ces mécanismes doivent
**respecter** (on y revient !) les personnes utilisant le logiciel. Cela est
superbement bien illustré par la vidéo « [Temps de cerveau disponible](https://www.arte.tv/fr/videos/071494-003-A/tr-oppresse-3-10/) »
(de la série « (Tr)oppressé » que je recommande vivement) dans laquelle un
ancien employé de Google, expert en éthique, témoigne :

> Le but est de capter et d’exploiter au maximum l’attention.

Il l’illustre ensuite par le lancement automatique de l’épisode suivant sur
Netflix et par le défilement infini sur Facebook ou Twitter (incitant de ce
fait à parcourir son fil d’actualité dans son ensemble) ; ces petits riens
qui font que nous revenons sans cesse à ces applications et nous en rendent
dépendant alors qu’elles n’ont d’intérêt que de nous divertir.

L’un des problèmes que j’identifie aujourd’hui est que le logiciel libre
copie beaucoup (trop) ce qui se fait dans le propriétaire, et en particulier
chez GAFAM et consorts… jusque dans leurs mécanismes nocifs. On peut ici
reprendre l’exemple du mécanisme de défilement infini que l’on retrouve chez
[Mastodon](https://joinmastodon.org/) ou [Diaspora](https://diasporafoundation.org/)
(et même sur FreshRSS !). Une certaine forme de dépendance peut donc
s’installer au sein même de logiciels libres.

## Convergence des buts ?

Les *designers* peuvent aujourd’hui nous aider, développeurs et développeuses,
à repenser l’éthique de nos logiciels **en replaçant les usages au centre de
nos préoccupations** et en imaginant et proposant des mécanismes permettant
« d’endiguer » ce flux permanent d’informations qu’il nous faut ingurgiter.

Elles et ils peuvent aussi nous aider à atteindre véritablement nos
utilisateurs en rendant nos outils utilisables et… utilisés. Car **un logiciel
non utilisable peut-il véritablement être considéré comme Libre ?** Je ne peux
m’empêcher de faire ici le parallèle avec l’association [Liberté 0](http://liberte0.org/)
qui a pour objet de « sensibiliser et de promouvoir le numérique libre et
accessible à toutes et tous ». Dans [leur charte](http://liberte0.org/charte.html),
il est explicité :

> Les membres du groupe « Liberté 0 » considèrent que la liberté d’exécuter un
> programme n’a de sens que si celui-ci est utilisable effectivement.

L’association est donc dans cette même démarche de promouvoir l’utilisabilité
des logiciels, au même titre que les *UX designers* (mais sous le prisme de
l’accessibilité).

N’y aurait-il pas ici une convergence des buts ? N’existe-t-il pas un lieu où
nous pourrions nous regrouper tou·tes ensemble pour imaginer des outils autres que
ceux issus du « [capitalisme de surveillance](https://framablog.org/2016/07/04/les-nouveaux-leviathans-i-histoire-dune-conversion-capitaliste-a/) » ?

---

Merci à [Maiwann](http://maiwann.net/) pour sa relecture attentive !
