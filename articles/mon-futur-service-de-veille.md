---
title: Mon (futur) service de veille
date: 2019-07-12 11:30
---

J’ai [partagé](./questionnaire-agregateurs-dactualite.html) il y a quelque temps
un questionnaire destiné à étudier le marché des agrégateurs d’actualité dans
le but d’une potentielle création d’entreprise. Mon objectif était de mieux
connaître le marché actuel, d’évaluer un début de tarification et de mieux
connaître les attentes de potentiel·les client·es. J’en profite d’ailleurs pour
remercier les 281 personnes qui ont pris le temps de répondre, ça m’a bien aidé
à y voir plus clair dans ce que j’allais faire !

Je reviens dans cet article sur les raisons qui m’encouragent à mettre en place
un tel service et j’appuie certains éléments par les questions posées par le
questionnaire.

## La raison de faire

Si vous suivez ce blog, vous savez sans doute que je suis au chômage depuis
décembre dernier. Loin d’être une situation subie, j’en ai profité pour poser à
plat mes envies à travers un [travail d’introspection](./serie/conception-du-site.html).
Je comptais alors, sans le dire explicitement, suivre une voie « par défaut »
qui était celle de me mettre en auto-entrepreneur pour faire du service auprès
d’entreprises. J’ai mis un moment avant de prendre réellement conscience que
**ce métier ne m’intéressait absolument pas**.

Mon chômage s’écoule doucement mais sûrement depuis maintenant 7 mois,
finançant d’une certaine manière mes activités bénévoles (en témoigne
l’évolution de ma page « [En ce moment](./now.html) » ces derniers mois). Il
est toutefois temps que je commence à travailler concrètement sur mon futur
professionnel afin de me dégager un salaire. L’idée n’étant pas de générer un
salaire mirobolant, mais juste assez pour pouvoir vivre tranquillement (par
transparence, je vise entre 1 500 et 2 000 € par mois).

Je n’envisage pas mon avenir professionnel en tant que salarié d’une entreprise
« traditionnelle » où l’on décide à ma place ce sur quoi je dois travailler.
J’ai toujours pris soin de ma liberté de choix et je compte bien continuer. En
avant donc pour créer ma propre structure. Aussi, l’envie de créer un service
en ligne de manière professionnelle me trotte dans la tête depuis plusieurs
années et cette remise en question m’a permis de la remettre au goût du jour.

Pour terminer sur ma raison de « faire », je souhaite revenir sur la limitation
volontaire de mon salaire. J’ai à cœur de pouvoir travailler et être d’une
quelconque aide au monde associatif, en particulier celles œuvrant dans les
luttes pour le climat ou les luttes sociales. Le monde économique dans lequel
nous vivons ne valorisant pas (encore ?) le travail qui peut être fait dans ces
milieux-là, il s’agit bien souvent de bénévolat. Mon but en limitant mon
salaire est donc de générer du temps libre que je pourrai mettre à dispositon
d’associations : « gagner moins pour travailler plus utile » en quelque sorte.

## Les raisons d’être

Se pose maintenant la question de la raison d’être du service en ligne.
Quelle raison me fait dire que le service aura une quelconque utilité ? À quel
besoin je réponds ?

L’une des choses que j’observe depuis longtemps, en partie à travers mon action
au sein de [Framasoft](https://framasoft.org), c’est le cloisonnement de nos
données et notre assentiment à déléguer le choix de nos sources d’information à
des plateformes privées telles que Twitter ou Facebook. On l’a vu notamment
lors du [scandale de Cambridge Analytica](https://www.nextinpact.com/news/106349-retour-sur-scandale-cambridge-analytica-et-molle-reponse-facebook.htm) :
cela peut avoir des conséquences désastreuses sans réelle réflexion politique
ni mesure de contre-pouvoir.

Pourtant, il est important de pouvoir se tenir au courant de l’actualité. C’est
le cas pour toutes les personnes qui veulent s’informer de ce qu’il se passe
dans le monde, mais aussi à des échelles plus locales. Il peut s’agir de
politique, d’évènements festifs, de sport, etc. D’un point de vue
professionnel, la veille spécialisée permet de se tenir à jour des évolutions
de son domaine (on peut penser à l’informatique mais aussi au domaine
législatif par exemple).

À travers notre veille, nous nous faisons influencer par les points de vue des
personnes qui publient l’information, voire par la pub qu’elles y insèrent.
Aucune information n’est totalement objective, mais nous décidons de suivre
tel ou tel site de manière encore à peu près consciente. Cela devient plus
pernicieux lorsque **cette veille est orientée sans nous le dire par les
plateformes sur lesquelles nous l’effectuons**. Il me paraît par conséquent
important d’avoir confiance dans ces plateformes (et donc les outils techniques
sous-jacents).

Dans ce que j’explique ici, j’identifie deux raisons d’être distinctes :

- celle de l’outil technique qui serait de **proposer un outil de veille
  connecté, simple et respectueux de ses utilisateur·ices** ;
- et celle du service en ligne qui serait de **permettre la promotion et le
  développement de cet outil en proposant un service transparent et de
  confiance**.

Cela implique tout un tas de choses que je détaille dans la partie suivante en
me basant sur les chiffres qui ressortent de mon questionnaire.

## La philosophie du service

### Connexion

J’ai eu beaucoup de réponses à mon questionnaire de personnes m’expliquant
qu’elles préféraient utiliser un logiciel sur leur propre PC, voire même une
personne m’a affirmé que les services en ligne étaient inutiles. Si je
comprends très bien que l’on puisse préférer un logiciel sur sa machine, il me
semble néanmoins que les outils de veille en ligne ont aussi leur utilité, ne
serait-ce que pour faire face aux usages liés à la mobilité (c’est-à-dire lire
sur différents terminaux en fonction de la journée tout en gardant synchronisé
ce que j’ai lu ou marqué en favori). La connexion est aussi intéressante dans
les interactions qu’elle peut apporter. Si notre suivi de l’actualité a migré
sur des plateformes sociales, ce n’est pas parce qu’elles sont meilleures pour
cet usage spécifique mais parce qu’elles permettent **de réagir, compléter et
confronter l’information à d’autres avis**. Aujourd’hui les agrégateurs
d’actualité ne le permettent effectivement pas, mais il s’agit pour moi d’une
évolution qui aurait tout son sens.

### Simplicité

Ensuite, la simplicité d’usage est importante si je veux toucher un public
autre que celui qui est prêt à creuser les fonctionnalités ou qui comprend
déjà le fonctionnement d’un agrégateur de flux <abbr>RSS</abbr>. C’est une
composante essentielle si je veux poser mon service comme une véritable
alternative face à d’autres plateformes. Deux questions dans mon sondage
devaient d’ailleurs me permettre de valider cet aspect-là. À l’argument « Les
fonctionnalités du service sont très ciblées et restent volontairement
simples », 58% des personnes ont répondu qu’il les inciterait à utiliser le
service, et 32% non. L’argument corollaire « Le service dispose de nombreuses
fonctionnalités (au risque de devenir complexe) » a reçu 38% d’avis positifs et
54% négatifs. J’admets toutefois que ce deuxième argument était orienté,
j’aurais sans doute dû éviter. On voit néanmoins que les rapports s’inversent
de façon très claire. Et j’ajoute que le questionnaire a touché un public
plutôt geek, plus enclein à fouiller un logiciel, alors qu’il ne s’agira pas
forcément de ma cible finale (j’y reviens plus loin).

### Transparence

La transparence du service m’apparaît comme une composante essentielle dans le
respect des utilisateur·ices car cela peut amener à comprendre certains choix
qui sont faits. Par exemple, une majorité de personnes sont prêtes à payer 1 ou
2 € par mois pour utiliser le service. Néanmoins, plus le prix sera bas, et
plus je devrais avoir de client·es pour générer mon chiffre d’affaire. Qui dit
plus de monde dit aussi plus de support à faire (donc moins de temps par
personne), un serveur plus puissant avec plus de capacités de stockage, etc.
Être transparent c’est aussi expliquer que le prix se calcule en fonction de
nombreux paramètres et que s’il peut paraître cher, il y a une raison derrière.
Cet argument sur la transparence des prix a d’ailleurs emporté l’adhésion de
70% des répondants à mon questionnaire, il y a donc une véritable demande.

La transparence induit également **l’utilisation de code libre**. Une telle
condition n’est pas une fin en soi mais permet à qui le souhaite et le peut de
vérifier que le code respecte ses utilisateur·ices et qu’il est effectivement à
leur service. Sans surprise vis-à-vis du public touché, le logiciel libre
est un argument pour utiliser le service pour 81% des répondants.

### Confiance

La question de la confiance était posée sous différentes formes (et est
d’ailleurs déjà traitée à travers la transparence). La confiance, c’est ce qui
va convaincre les gens de venir, de rester (donc de payer) et de parler autour
d’eux de mon service. C’est aussi peut-être ce qu’il y a de plus compliqué à
assurer sur le long terme. Cela passe à la fois par le dialogue et par les
actes ; les critiques ne seront pas tendres en cas (d’accumulation) de
faux-pas.

Les deux composantes essentielles du dialogue que je vois sont la communication
sur les médias sociaux et le support offert aux client·es. Une mauvaise
communication ou un mauvais support, c’est ce qui peut plomber une image de
marque ; à l’inverse d’un bon support, dont vous n’entendrez jamais parler.
Quant à l’excellent support, c’est les anciens de Captain Train / Trainline
qui [en parle le mieux](https://medium.com/@djo/obsession-service-client-captain-train-cb0b91467fd9).
À l’argument du support par email, j’ai d’abord été surpris de n’avoir que 51%
de réponses favorables… mais cela ne veut pas dire qu’il ne faut pas en faire,
juste que cela ne correspond pas à un argument marketing qui justifierait de
changer de service.

Ensuite, je compte aligner mes actes avec mes paroles à travers au moins trois
actions :

- faciliter la portabilité des données (que ce soit pour arriver ou pour partir) ;
- contribuer au code du logiciel utilisé ;
- participer au collectif des [<abbr title="Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr>](https://chatons.org/).

D’un point de vue marketing, les deux premiers arguments sont sans appel : 82
et 85% d’avis favorables. Le troisième est intéressant car c’est celui qui a
recueilli le plus de personnes ne se prononçant pas (26% alors que les autres
arguments tournent plutôt entre 7 et 15%). Je l’analyse du fait que les
<abbr>CHATONS</abbr> n’est pas (encore) un collectif très connu et que les
personnes ne savent donc pas quoi en penser. En revanche, 78% des avis
exprimés sont favorables, ce qui est plutôt positif dans l’ensemble !

### Qualité de service

Pour terminer, la qualité du service (performances et maintenance du service en
ligne) devra être au cœur de mes préoccupations si je veux rester crédible. Sur
ce point néanmoins je dispose d’un peu moins de billes aujourd’hui, même si je
maintiens déjà [Lessy](https://lessy.yuzu.ovh) sans trop de soucis.

L’une de mes certitudes toutefois est qu’il ne faut pas lésiner sur les moyens
et notamment sur les capacités du serveur et la sécurité que ce soit
maintenance, <em lang="en">monitoring</em> ou <em lang="en">backups</em>.

Pour avoir vécu quelques applications en production douloureuses, j’ai
également conscience qu’il y a véritablement un changement d’esprit à opérer
entre un service offert (type [lessy.yuzu.ovh](https://lessy.yuzu.ovh)) et un service
payant professionnel.

## Marché et tarification envisagées

La définition d’un marché est un exercice compliqué car, d’une part, je ne l’ai
jamais fait et, d’autre part, c’est un peu naviguer à l’aveugle. Dans le cas
d’un agrégateur d’actualités, c’est encore plus casse-gueule car **les
technologies comme <abbr>RSS</abbr> semblent à la fois sur le déclin et déjà
fortement investies par l’auto-hébergement**. On me l’a d’ailleurs
(sous-)entendu de (très) nombreuses fois dans les commentaires de mon
questionnaire. 70% des répondants utilisent déjà un service hébergé ou sur leur
propre PC et seulement 37% se disent prêt·es à payer pour un tel service ; ce
qui ne signifie pas non plus qu’ils et elles viendront chez moi ! Qu’à cela ne
tienne, je vais tout de même tenter une réponse.

Je vois très clairement deux périodes pour mon service. La première consistera
à proposer un service basé sur un logiciel existant (très certainement [FreshRSS](https://freshrss.org)).
Je viserai alors un public déjà sensibilisé à ce genre d’outils, qui n’a pas
l’envie ni le temps de maintenir un service en ligne et qui est prêt à payer
pour déléguer ces tâches à quelqu’un d’autre. J’ai bon espoir d’arriver à
générer un peu de chiffre d’affaire, de quoi commencer à me sortir un salaire
(probablement pas énorme). En parallèle de cette période, je compte améliorer
le logiciel pour l’orienter vers quelque chose de plus grand public et moins
dépendant de technologies précises. Tout du moins, je pense qu’il sera
nécessaire de repenser la façon de s’abonner à des sites. Je dis « améliorer »
mais comme FreshRSS est déjà soutenu par une communauté, cela se fera
nécessairement en accord avec celle-ci. Ce qui m’amènera donc à la deuxième
période où je pourrai toucher un public élargi et qui ne se soucie pas des
technologies sous-jacentes. En résumé, **plutôt que de lutter pour m’insérer
sur un marché hyper-concurrentiel, je compte en changer pour me permettre de
respirer**.

Un autre point, qui rajoute peut-être une couche de complexité, c’est que je
compte m’adresser à un public exclusivement francophone. On peut trouver cette
décision un peu étrange à l’heure de la connexion mondiale, mais je fais ce
choix de manière pragmatique pour me faciliter la communication. Je ne serai
en effet jamais aussi efficace qu’en français pour véhiculer une idée ou pour
aider une personne sur le support. Je crois que ce choix me permettra de
m’économiser de l’énergie pour d’autres tâches plus importantes (et j’aurai
déjà beaucoup à faire !) Bien entendu, si le service se base sur FreshRSS, il
existe déjà de nombreuses traductions donc je ne m’amuserai pas à les enlever.
Je parle ici bien uniquement de la communication.

Concernant la politique tarifaire, cela reste encore à valider mais je
partirais sur :

- une période d’essai d’un mois ;
- un abonnement mensuel (le prix reste à déterminer) ;
- un abonnement annuel avec réduction (idem) ;
- aucune limite sur le nombre de flux suivis.

Concernant le dernier point, plus de 90% des répondants se contentent de moins
de 500 flux ce qui me semble tout à fait gérable. Reste les 10% restants et les
abus éventuels qui pourraient faire exploser les coûts de stockage et de
traitement notamment. J’envisageais d’abord de mettre une limite à 500
abonnements, déblocable sur simple demande au support, mais d’un point de vue
marketing je trouve qu’il est plus efficace de ne pas mentionner de limite et
de gérer les abus au cas par cas. L’avenir me le dira !

Aussi, j’ai décidé de ne pas appliquer de tarifs évolutifs en fonction du
nombre d’abonnements. Les avis étant très partagés sur la question, il me
semble qu’il est plus simple d’avoir un tarif unique.

## Avenir envisagé sur le long terme

La grande question qui se pose lorsque l’on s’abonne à un service en ligne,
c’est celle de sa pérennité. Est-ce que le service sera toujours là dans un
an ou deux ? Vous aurez peut-être compris que je ne souhaite surtout pas me
lancer sur un modèle « startup » qui consiste pour beaucoup à cramer de
l’argent dans l’espoir de trouver un <em lang="en">business model</em>… bien au
contraire. Comme déjà évoqué plus haut, mon premier objectif sera de me générer
un revenu correct pour vivre. Cet objectif, j’aimerais l’atteindre globalement
seul afin de valider un certain niveau d’autonomie. Mais maintenir un service
seul sur le long terme ne me paraît absolument pas viable et extrêmement
fragile, c’est pourquoi j’envisage le long terme au sein d’une structure avec
plusieurs personnes. Il est bien sûr trop tôt pour m’avancer sur quoi que ce
soit, en particulier sur la forme que prendrait cette structure. J’ai toutefois
une ambition, directement inspirée par la société Basecamp : celle de
**maintenir le service [jusqu’à la fin d’Internet](https://basecamp.com/about/policies/until-the-end-of-the-internet)**.

Cela ne peut rester qu’une ambition tant que je ne génère pas de revenu et que
la stabilité de l’entreprise n’est pas prouvée, mais je trouve que c’est une
vision juste et encourageante que ce soit pour moi ou pour mes (futur·es)
client·es. Là où beaucoup trop de services sont aujourd’hui éphémères, je
trouve qu’il est rassurant de savoir qu’un tel endroit est prévu pour durer.
J’espère également que cela me poussera à faire les choses « correctement » et
inspirera d’autres personnes à faire de même.

## Prochaines étapes

Tout ce que je viens de raconter reste très théorique tant que je n’ai pas
commencé à avancer dessus. Je n’ai d’ailleurs pas encore de date à donner quant
à la sortie officielle du service. J’ai commencé un plan pour avoir une vue
d’ensemble de tout ce que j’ai à faire d’ici là. Si je suis assez confiant sur
le fait de pouvoir avancer relativement vite, **le service ne devrait pas
sortir pour autant avant la fin de l’année**. J’ai notamment, avant ça, un
voyage à faire en Bretagne ; voyage que je repousse depuis plusieurs années…
mais cela risque bien d’être le dernier moment si je veux le réaliser sous la
forme que j’envisage.

En parallèle de ce travail de définition de mon service, j’ai également réalisé
des documents prévisionnels pour connaître les coûts de mon projet et
comprendre mon chiffre d’affaire. Il me reste encore à affiner ces chiffres, en
particulier les objectifs en terme de nombre de client·es. La prochaine étape
que je vois est celle du choix du statut légal de l’entreprise. J’envisage
aujourd’hui deux possibilités : le statut de micro-entreprise ou rejoindre une
[coopérative d’activité et d’emploi](https://fr.wikipedia.org/wiki/Coop%C3%A9rative_d%27activit%C3%A9s_et_d%27emploi).
Je reste néanmoins toujours à l’écoute d’autres solutions.

Quant au nom du service… je le garde pour plus tard 😉
