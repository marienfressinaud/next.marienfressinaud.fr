---
title: Gagner ma vie : FreshRSS ou Lessy ?
date: 2019-09-19 18:30
---

Lorsque j’ai décidé de monter un service pour essayer de [gagner ma vie](mon-futur-service-de-veille.html),
j’étais parti dans l’idée de le faire avec [Lessy](https://lessy.yuzu.ovh) (un
gestionnaire pour aider à gérer son temps). Les avantages étaient nombreux :

- une demande qui me semble plus importante ;
- des technologies que je connais mieux ;
- une base de code plus récente avec moins de dette technique ;
- un projet dont la gouvernance est super simple vu que je suis le seul à
  travailler (ahem…) dessus ;
- et j’en oublie sans doute.

Toutefois, en y réfléchissant un peu, je me suis dit que le but de Lessy était
d’avoir à l’utiliser le moins possible, voire d’être capable de s’en
passer totalement dans une démarche de réappropriation de son temps. Il me
semblait du coup malhonnête de générer de l’argent avec puisque cela impliquait
nécessairement de tenter de capter l’attention des gens (à minima pour les
faire venir sur le service, idéalement pour m’assurer qu’ils l’utilisent
effectivement).

À l’inverse il me semble que [FreshRSS](https://freshrss.org), étant un outil
de veille, s’intègre plutôt bien dans une activité quotidienne. Je suis moins
gêné par le fait que les personnes utilisant le service payent pour l’utiliser
quotidiennement puisque c’est dans sa nature. Je crois également qu’un outil de
veille s’attaque à un problème plus ambitieux : l’information nous est
aujourd’hui pré-digérée principalement à travers des plateformes centralisées
et propriétaires, donc de manière plus ou moins uniforme, en tout cas opaque.
Il est pour moi plus que temps que l’on redonne leurs lettres de noblesse à des
technologies comme <abbr>RSS</abbr> et Atom ; cela implique d’inventer de
nouveaux usages autour d’elles, et donc de développer des outils. En gagnant de
l’argent avec FreshRSS, je serai en mesure de l’améliorer en lui accordant du
temps. D’ailleurs, j’entérine ça dans [les conditions générales de vente de
Flus](https://flus.io/i/?a=cgv) (le service que je mets en place pour fournir
un FreshRSS hébergé) :

> Le service est amené à évoluer régulièrement à travers des mises à jour du
> logiciel, développées à la fois par une communauté de développeurs et
> développeuses, et **par le vendeur du service qui s’engage de ce fait à
> contribuer au développement d’un bien commun**. Cet engagement sera tenu à
> partir de et aussi longtemps que le vendeur pourra vivre du service.

Je n’ai plus qu’à espérer que les flux <abbr>RSS</abbr> ne soient pas tout à
fait morts ;)
