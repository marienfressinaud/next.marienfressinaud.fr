---
title: Mes jeux 2024
date: 2025-02-09 11:35
description: Ma rétrospective de jeux 2024 : No Man’s Sky, Story Teller, Sea of Stars, Return of the Obra Dinn et Celeste (encore, oui mais !)
---

Comme en 2023, j’ai décidé de faire une rétrospective des jeux auxquels j’ai joué l’année passée (voir [la rétrospective de mes jeux 2023](mes-jeux-2023.html)).

Il faut savoir que je me fais en début d’année une petite liste des jeux auxquels j’aimerais jouer sur l’année.
Ça me permet d’être moins tenté par les nouveautés « à la mode », mais aussi de mieux profiter des jeux auxquels je joue.
Ça a été une très bonne pioche en 2024.

## No Man’s Sky

J’ai commencé l’année en jouant à No Man’s Sky.
Il s’agit d’un jeu d’exploration et de survie spatiale dont l’univers (infini ?) est créé de manière procédurale[^1].

[^1]: De façon unique et aléatoire si vous voulez.

J’étais resté sur le souvenir d’un jeu hyper attendu mais qui avait énormément déçu à sa sortie.
J’avais néanmoins entendu dire qu’il s’était bonifié avec le temps et ses nombreuses mises à jour.

Ça a failli être une déception car je n’ai pas été immédiatement happé par le jeu.
Comme tout jeu de survie, je me suis assez vite lassé et je l’ai mis de côté.
On discerne en effet assez rapidement certains des artefacts qui permettent au jeu d’être « infini » ; par exemple les dialogues avec les autres personnages scriptés à l’extrême, ou les planètes générées selon un même schéma répétitif.
De plus, j’y ai joué sur la Switch qui galère pas mal à faire tourner le jeu (quoique j’estime que le niveau de fluidité global soit un petit exploit pour ce jeu !)

J’ai toutefois décidé de relancer le jeu en décembre, un peu par hasard.
C’était l’ambiance parfaite qu’il me fallait pour finir l’année : bien que les artefacts du jeu soient toujours aussi visibles, il y a aussi une ambiance très douce, presque mélancolique, qui se dégage de No Man’s Sky.
La musique transporte le joueur à travers l’immensité de l’univers du jeu et j’ai finalement pris plaisir à simplement suivre la quête principale, tout en construisant une base sur ma planète « paradisiaque » (elles ne le sont pas toutes !)

J’ai finalement passé une cinquantaine d’heures supplémentaires sur le jeu 😅
Une très bonne surprise tout compte fait !

[Lien vers le site de No Man’s Sky.](https://www.nomanssky.com/)

## Story Teller

Story Teller est un petit jeu d’énigmes.
Chaque énigme consiste à narrer une histoire en quelques cases.
On agrémente chacune des cases avec une scène (= un lieu) et des personnages qui vont interagir entre eux.
Seulement, les interactions changent et évoluent en fonction des cases précédentes.
Par exemple, si on a fait se marier deux personnages à la première case, si la seconde case représente le décès de l’un des deux, l’épous·e sera triste.

C’est un bon jeu pour se creuser les méninges, sans perdre trop de temps (quelques minutes pour chaque énigme).
Il a l’avantage d’être très simple à prendre en main.
La difficulté des énigmes est toutefois assez inégale et le jeu plutôt court (je l’ai terminé en environ 4h).
Cela n’empêche pas Story Teller d’être un jeu malin, bien pensé et agréable à jouer.

[Lien vers le site de Story Teller.](https://annapurnainteractive.com/en/games/storyteller)

## Sea of Stars

Sea of Stars est un RPG « à l’ancienne » mais avec des mécaniques de gameplay remises au goût du jour.
On y incarne deux enfants du Solstice, élevé·es en guerriers pour botter le train des monstres d’un alchimiste malfaisant : le Fleshmancer.

Le scénario est bien mené, parfois un peu simpliste sans que ce soit trop gênant puisque porté par des personnages très attachants[^2].
Quelques twists dans l’histoire permettent de rester accrochés.
J’ai particulièrement aimé les petites énigmes qui parsèment le jeu, jamais bien compliquées, mais bien dosées.
Les quêtes secondaires rajoutent du contenu agréable et pas rébarbatif, ce qui permet d’en profiter quelques heures de plus.
Enfin, pour ne rien gâcher, le jeu est très beau avec son look rétro et sa musique soignée.

[^2]: Mention spéciale au personnage de Garl, évidemment ♥️

Je rangerais Sea of Stars dans la catégorie de mes jeux un peu « doudous ».
Peut-être pas mon jeu de l’année, mais j’ai pris du très bon temps dessus, au point que je veuille terminer l’ensemble des quêtes secondaires pour ne rien manquer.
Je conseille vivement !

[Lien vers le site de Sea of Stars.](https://seaofstarsgame.co/)

## Return of the Obra Dinn

Il s’agit là encore d’un jeu d’énigme.
On débarque sur un bateau, l’Obra Dinn, pour enquêter sur la disparition mystérieuse de son équipage.
On est pourvu pour cela d’une montre à gousset un peu spéciale puisqu’elle permet de revivre les scènes qui ont mené à la mort des membres de l’équipage.
On dispose également d’un livre de bord que l’on renseigne au fil de nos déductions.
Le principe est de déterminer la cause du décès de chacun·e des membres du bateau.

Au premier abord, les graphismes du jeu sont assez déstabilisants et la boucle de gameplay se révèle plutôt répétitive.
Il faut toutefois passer les premières appréhensions pour apprécier le jeu à sa juste valeur.
Il y a notamment un côté très addictif à découvrir ce qui est arrivé à l’ensemble de l’équipage du bateau.

Ce n’est clairement pas un jeu facile — les déductions tiennent parfois à pas grand-chose —, mais il sait bien rétribuer le ou la joueuse quand elle finit par trouver la solution.
De plus, il n’y a pas de mécanique punitive (pas de mort ou de chronomètre pour mettre la pression par exemple), ce qui permet d’apprécier tranquillement le jeu.
J’ai fini par adorer les quelques heures passées dessus, et je pense que je retournerai sur l’Obra Dinn une fois que je l’aurai un peu oublié.

[Lien vers le site de Return of the Obra Dinn.](https://obradinn.com/)

## Celeste

C’est un retour puisque Celeste faisait déjà partie de ma liste de jeux 2023 !

Il s’agit d’un jeu de plateforme en deux dimensions particulièrement exigeant.
On y incarne Madeline, une jeune fille qui veut gravir une montagne : le Mont Celeste.
C’est finalement sa dépression et son anxiété qu’on finit par affronter au fil de l’ascension.

Je concluais dans mon précédent article ainsi :

> Mon avis est (un peu) mitigé vis-à-vis de ce jeu.
> Je pense que j’en attendais beaucoup, mais que le fait d’avoir atteint la « première » fin du jeu relativement rapidement ne m’a pas laissé le temps de l’apprécier à sa juste valeur.
> Pour autant, j’ai beaucoup aimé y jouer.
> J’ai de toute façon encore beaucoup de choses à faire, notamment les « faces B » des chapitres (des versions beaucoup plus ardues des cartes).
> […] Je pense que je l’apprécierai de plus en plus avec le temps.

Vous vous en doutez : ça n’a pas raté 😁

Je ne pensais pas passer autant d’heures supplémentaires dessus (une centaine d’heures, oui oui…)
Pour autant, je me suis pris au jeu des défis supplémentaires qui m’attendaient après avoir gravi le Mont Celeste une première fois.

Le défi qu’a représenté la « vraie » fin du jeu a mis mes nerfs à l’épreuve, mais m’a aussi apporté un plaisir que je n’avais encore jamais vécu en jouant.
Il faut vraiment prendre les défis que proposent Celeste comme des marches que l’on grimpe une par une.
Dès qu’on butte un peu, alors c’est qu’il est temps de revenir un peu en arrière pour assurer toujours un peu mieux ses pas.
C’est vraiment cette progression (et sa difficulté) qui porte tout au long du jeu.

Une mention spéciale pour le DLC qui m’a énormément touché.
Il est non seulement absurdement dur, mais aussi très touchant et extrêmement généreux dans sa récompense finale.
Terminer le DLC a probablement été l’une de mes meilleures expériences vidéo-ludiques de ces dernières années.
J’ai rarement vu un gameplay aussi bien porter le message qu’il veut transmettre.
Rien qu’à me le remémorer, j’ai envie de relancer le jeu !

J’ai poussé le bouchon encore un peu plus loin en visant de terminer la première fin du jeu en moins d’une heure.
Défi relevé, mais je ne crois pas avoir envie de m’y replonger toutefois 😅

Bref, vous comprendrez que je recommande Celeste les yeux fermés.
Si la difficulté assumée du jeu vous fait peur, sachez qu’il est possible d’ajuster la difficulté afin de profiter du jeu en fonction de son niveau.

[Lien vers le site de Celeste.](https://www.celestegame.com/)
