---
title: Notes #4
date: 2020-02-17 22:45
blog: false
---

[David](https://larlet.fr/david/) a posé la question sur Mastodon de savoir
pourquoi [les gens n’utilisent pas de lecteur de flux RSS](https://mastodon.social/@dav/103524352995568684).
La principale raison semble être le fait de se trouver noyé sous la quantité
d’informations. Ça me conforte dans la direction que j’ai prise pour le futur
de [Flus](https://flus.fr).

---

J’ai continué la semaine dernière à faire des maquettes pour Flus. J’y reviens
régulièrement pour simplifier ou compléter des choses. Il y a un réel plaisir à
voir les choses prendre forme de la sorte et tout devient de plus en plus clair
dans ma tête. Je ne risque pas de m’ennuyer ces prochains mois, et je vais sans
doute devoir revoir mon organisation globale.

---

J’ai annoncé proposer [des cafés-formation](https://flus.fr/carnet/cafes-formation.html)
gratuits pour aider à la prise en main de Flus. J’ai l’impression qu’il s’agit
peut-être d’un chainon manquant dans l’adoption de certains logiciels et je suis
très content de proposer cet accompagnement.

Pour l’instant j’ai eu des retours positifs, mais cependant pas encore de prise
de rendez-vous. Timides ? 😁

---

Mon nouveau système de monitoring fonctionne bien… peut-être un peu trop. Je me
fais régulièrement spammer de SMS et de courriels. J’ai encore un peu de boulot
de configuration en face de moi, mais ça avance.

---

Je continue encore et toujours à faire du pain, et j’en ai fait pour la
première fois pour des voisins. Je crois qu’il a été apprécié :). J’ai encore
le sentiment de progresser, notamment sur la phase de pétrissage. Mon plus gros
souci désormais est de ne pas trop dégueulasser mon plan de travail, ou bien
de trouver un moyen pour le nettoyer plus facilement.

---

J’ai acheté un vélo la semaine dernière chez [Cycle & go](https://www.cycles-go.com/home).
Ça faisait deux mois que ça trainait. Première balade hier le long du
[Drac](https://fr.wikipedia.org/wiki/Drac), avec un beau soleil 😍

J’ai également acheté un PC de bureau en reconditionné chez [afb](https://www.afbshop.fr/).
Ce n’est pas une bête de course, mais grandement suffisant pour mes besoins. Les
personnes autour de moi semblent étonnées que je n’eusse pas pris un ordinateur
plus puissant, j’avoue ne pas comprendre cet étonnement et y vois
principalement un décalage entre leurs propres envies et les miennes.

Dans les deux cas, je n’ai pas ressenti une pression de la part des vendeurs
pour acheter, j’avais même l’impression qu’ils m’incitaient à prendre mon
temps. Agréable.

---

Je prends plus de plaisir à écrire ces notes ici que je n’en avais à le faire
quand je les gardais pour moi. Est-ce dû au fait qu’elles sont publiées (et
donc potentiellement lues) ? Ce qui me gêne c’est que je ne publie plus rien
sur le blog alors que j’aurais des choses à y raconter. Je n’ai juste pas de
temps à y accorder.

Ça me fait penser que j’ai eu plusieurs retours récemment où des personnes
m’ont dit lire mes articles de blog. J’avoue que je suis toujours un peu
surpris : comme je n’ai pas d’idée précise sur l’audience du blog (pas d’outil
de tracking, pas de commentaires), j’ai l’impression que personne ne me lit.

Du coup, si vous êtes arrivé·e jusqu’ici, je serais curieux de savoir « si » et
« pourquoi » vous me lisez. Vous pouvez me contacter [par-là](contact.html).
