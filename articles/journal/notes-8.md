---
title: Notes #8
date: 2020-04-13 11:00
blog: false
---

Je n’ai pas rédigé de notes depuis le 23 mars, mais difficile d’accorder du
temps à tout ce que je veux faire. Bizarrement depuis le confinement, je fais
plus de choses, plus variées.

---

**Dessin.** J’ai continué depuis la dernière fois tant et si bien que je me suis
offert, sur un coup de tête, un cours en ligne [sur Dessindigo](https://www.dessindigo.com/).
Pourquoi ce site plutôt qu’un autre ? Je suis tombé sur [l’une de leurs
vidéos](https://www.youtube.com/watch?v=o2a-_2HKZhg) sur Youtube et j’ai trouvé
la personne qui présente sympathique. Ce n’est pas plus rationnel que ça. Je
sens que les échanges sur le forum vont également me permettre de progresser
plus vite.

---

**Pain.** C’est rigolo comme à chaque fois que je me dis que je « maîtrise » un
truc, je me rends compte la fois d’après que je peux encore varier ma façon de
faire. Je ne crois pas, par exemple, que j’ai pétri mon pain deux fois de la
même manière depuis le début du confinement et pourtant j’en fais deux fois par
semaine. Il y a toujours à apprendre et ça me fait vraiment plaisir de sentir
la progression en 1 an. Je crois que c’est ce qui m’a motivé à réessayer le
dessin d’ailleurs.

---

**Lecture.** J’ai longtemps cherché à savoir quand dédier du temps à la lecture.
Pendant un temps, sur le conseil de mon frère, je tentais de lire un chapitre
chaque soir avant de me coucher… sauf que je finissais toujours par m’endormir
dessus et je n’arrivais pas à assimiler ce que je lisais. J’ai finalement
trouvé le moment idéal pour moi : le matin, pendant mon petit-déjeuner. Comme
je lis « mieux » le matin, je comprends mieux l’histoire, et je finis
mécaniquement par me dénicher d’autres moments de lecture. Depuis le début du
confinement j’ai lu les 4 premiers tomes du [_cycle des Robots_](https://fr.wikipedia.org/wiki/Cycle_des_robots),
trois dialogues de Platon ([_Théétète_](https://fr.wikipedia.org/wiki/Th%C3%A9%C3%A9t%C3%A8te_(Platon)),
[_Le Sophiste_](https://fr.wikipedia.org/wiki/Le_Sophiste) et [_Le
Politique_](https://fr.wikipedia.org/wiki/Le_Politique)… mais il a fallu
s’accrocher !) et enfin, [_Le Droit à la paresse_](https://fr.wikipedia.org/wiki/Le_Droit_%C3%A0_la_paresse)
de Paul Lafargue.

---

**Exercice.** Ça aura pris un peu de temps pour que je le ressente, mais les
effets du confinement sur mon corps sont bien là. Étant déjà relativement
fragile du dos, j’ai commencé par faire quelques exercices et étirements à ce
niveau-là, mais j’ai débuté trop récemment pour jauger de l’efficacité.
J’essaye de privilégier les vidéos produites par des personnes professionnelles
pour éviter de faire n’importe quoi. Pour l’instant je me base essentiellement
sur les conseils de la chaîne [Major Mouvement](https://www.youtube.com/channel/UCsnRkRtbTyISkfGTGwM7VfQ).

---

**Jeu vidéo.** J’ai acheté [_Animal Crossing_](https://fr.wikipedia.org/wiki/Animal_Crossing:_New_Horizons)
sur la Switch quand il est sorti. Je n’avais pas du tout envisagé de l’acheter
à la base, mais l’engouement pour ce jeu a fini par avoir raison de moi.
Finalement ce n’est pas plus mal, ça permet de se changer les idées de temps en
temps dans un univers certes légèrement cucul, mais plein de bienveillance.
C’est un jeu qui se joue par petites tranches horaires, permettant donc de ne
pas y perdre ses journées entières.

---

**Flus.** Et oui, je bosse également. J’ai bien avancé la semaine dernière sur
le futur site. Je me suis ajouté au moins 2 semaines de boulot par rapport à ce
que j’avais imaginé initialement, mais j’estime que ça en valait la peine ;
vous verrez. L’occasion de féliciter mon moi du passé pour n’avoir jamais
annoncé de date de lancement, sauf avec d’énormes pincettes et de conditions.
En tout cas, ça approche.
