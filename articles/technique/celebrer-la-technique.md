---
title: Célébrer la technique
date: 2021-04-24 21:50
tags: featured
---

Je parle très peu de technique. Souvent, ce genre de conversations m’ennuient.
Mais à force de m’en tenir éloigné, j’ai fini par croire que la technique ne
m’intéressait pas tant que ça. J’ai fini par me dire que ce qui comptait
uniquement, c’était que le résultat final soit utile et que seule l’approche
utilisateur m’intéressait. Mais c’est faux : les deux approches m’intéressent
et je crois que je me serais reconverti depuis longtemps sans l’une ou l’autre.

Aujourd’hui je vais donc parler technique en célébrant les choix que j’ai pris
depuis un an pour [flusio](https://github.com/flusio/flusio), le logiciel
derrière mon service [Flus](https://flus.fr).

**Je suis heureux d’avoir fait le choix du <abbr>PHP</abbr>.** Certains choix
de conception sont rageants, mais je suis très à l’aise avec ce langage au
quotidien. Il me permet également une certaine créativité que je ne trouve pas
avec d’autres langages.

**Je suis heureux d’avoir fait le choix de (re)développer mon propre framework,
[Minz](https://github.com/flusio/Minz).** Il s’adapte à ma manière de penser en
fonction des problèmes que je rencontre, pas l’inverse.

**Je suis heureux de son système d’abstraction des requêtes utilisateur et des
réponses serveur.** Cela me permet de tester une large portion du logiciel
facilement. Un effet de bord positif a été de me fournir un mécanisme de [ligne
de commande](https://github.com/flusio/flusio/blob/main/cli) à peu de frais.

**Je suis heureux de ne pas utiliser d’<abbr>ORM</abbr> pour l’accès à la base de
données.** Le <abbr>SQL</abbr> m’est souvent facile à lire et je me torture moins
l’esprit pour les requêtes compliquées.

**Je suis heureux de mon [système de migrations](https://github.com/flusio/Minz/blob/main/src/Migrator.php)
qui tient en moins de 300 lignes, commentaires inclus.** J’ai appris que ce
n’était pas si compliqué que ça à faire.

**Je suis heureux du choix d’abstraire la création des dates derrière une classe
[`\Minz\Time`](https://github.com/flusio/Minz/blob/main/src/Time.php).** Durant
les tests, cela me permet de figer le temps à une date précise et faciliter
ainsi le travail sur les dates relatives.

**Je suis heureux d’avoir développé un système de jobs asynchrones au sein de
flusio plutôt qu’utiliser un logiciel dédié.** Ça me fait un logiciel de moins
à maîtriser et installer. Le cœur du système tient [en quelques lignes](https://github.com/flusio/flusio/blob/5beb930e887f7d14e0a3b774fe04c3fc9ceba8a1/src/cli/JobsWorker.php#L62-L94).

**Je suis heureux de ma mini-bibliothèque [SpiderBits](https://github.com/flusio/flusio/tree/main/lib/SpiderBits/src)
pour parser le Web.** Elle tient en 1 300 lignes de code relativement simples
et permet d’aller du [nettoyage d’<abbr>URL</abbr>](https://github.com/flusio/flusio/blob/main/lib/SpiderBits/src/Url.php)
à [l’extraction d’informations OpenGraph](https://github.com/flusio/flusio/blob/main/lib/SpiderBits/src/DomExtractor.php)
en passant par [l’analyse de flux <abbr>RSS</abbr>](https://github.com/flusio/flusio/blob/main/lib/SpiderBits/src/feeds/RssParser.php).

**Je suis heureux du nombre de tests écrits.** Ça prend du temps à écrire, mais
ils m’ont sauvé plus d’une fois. Le résultat, c’est une application stable
depuis sa première version.

**Je suis heureux du nombre réduit de dépendances à des bibliothèques externes
([<abbr>PHP</abbr>](https://github.com/flusio/flusio/blob/main/composer.json)
et [JavaScript](https://github.com/flusio/flusio/blob/main/package.json)).**
Le code externe, c’est du code que je ne maîtrise pas ; j’y fais appel avec
parcimonie.

**Je suis heureux de prendre mon temps pour abstraire les concepts.** Je
préfère répéter du code de nombreuses fois tant que je n’ai pas trouvé
l’abstraction qui me satisfait, souvent pour mon plus grand bien.

**Je suis heureux de traiter la dette technique comme l’évolution naturelle
de mon code répondant à de nouveaux problèmes,** pas comme de mauvais choix de
conception passés.

Tout n’est pas parfait dans flusio, mais je suis heureux de chacun des choix
que j’ai pu faire, même ceux sur lesquels je souhaite revenir. Ils ont tous été
pris en connaissance de cause, en fonction de ce que j’avais comme information
à ma disposition à un instant T. Pour prendre mes décisions, je ne suis pressé
ni par une personne externe, ni par le temps. Il en résulte un logiciel
maintenable dont j’ai plaisir à parcourir le code. Même seulement au bout d’un
an de développement, ce n’est pas souvent le cas.

Je crois que flusio est une bonne vitrine sur tout ce que j’ai appris en 10
ans, c’est un logiciel qui me ressemble.
