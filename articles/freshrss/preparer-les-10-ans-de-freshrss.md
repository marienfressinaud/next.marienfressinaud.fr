---
title: Préparer les 10 ans de FreshRSS
date: 2022-03-28 11:53
---

[FreshRSS](https://freshrss.org) va avoir 10 ans en octobre. J’aimerais célébrer ça d’une manière ou d’une autre, mais j’ai peur de me lancer dans quelque chose de trop grand pour moi. Je pensais simplement revoir le site pour le rendre plus attractif et ajouter des informations manquantes.

J’ai ouvert une discussion [sur GitHub](https://github.com/FreshRSS/FreshRSS/discussions/4294) pour que d’autres personnes puissent s’exprimer et éventuellement faire des choses ensemble.
