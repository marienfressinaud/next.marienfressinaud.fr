Date: 2014-01-29 21:31
Title: FreshRSS 0.7, ça casse et ça passe !
Slug: freshrss-0-7-ca-casse-et-ca-passe
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

FreshRSS, l’agrégateur de flux RSS qui déboite tout, sort aujourd’hui en version 0.7. Au menu, des tas de bonnes choses : un mode multi-utilisateurs, des statistiques, un script de mise à jour et des performances qui s’améliorent encore et encore, parmi tant d’autres (j’y reviens dans le reste de cet article). Comme je sais que vous ne tenez déjà plus en place, je vous invite à vous rendre immédiatement sur [la démo](http://demo.freshrss.org) (demo / demodemo).

## Un nouveau site fait maison

Le précédent site était géré via le système de GitHub. Dans un soucis d’auto-hébergement, j’ai créé un site (en anglais) fait maison avec [des saucisses bien de chez nous à l’intérieur](http://knacss.com/). Je pense que celui-ci est plus clair que le précédent et présente mieux les choses. Le lien du site : [freshrss.org](http://freshrss.org).

## Sauvez votre serveur, lâchez TTRSS !

FreshRSS était déjà du genre plutôt rapide par rapport à certaines alternatives (*keuf*TTRSS*keuf*) mais souffrait quand même sur le long terme de ralentissements. Avec les améliorations au niveau de la base de données et des requêtes SQL, FreshRSS semble maintenant pouvoir gérer plus de 100 000 articles sans broncher.

Des améliorations au niveau du code PHP sont aussi au rendez-vous mais je ne rentrerai pas dans les détails (le [changelog](https://github.com/FreshRSS/FreshRSS/blob/master/CHANGELOG) est là pour ça).

TTRSS n’est pas connu pour ses performances, mais grâce à l’ami [@konfiot](https://github.com/konfiot), on peut se faire une idée des gains qu’apportent FreshRSS. Voici quelques graphiques représentant l’activité de son serveur. Faut-il vous indiquer explicitement le moment de sa migration ?

![activité des entrées-sorties](images/freshrss/ttrss_freshrss_io_activity.png)

Activité des entrées-sorties sur le disque dur

![requêtes MySQL](images/freshrss/ttrss_freshrss_mysql_queries.png)

Activité des requêtes MySQL

![Charge sur serveur](images/freshrss/ttrss_freshrss_load.png)

Charge du serveur (c’est un peu moins marqué… mais quand même !)

On me souffle dans [le README](https://github.com/FreshRSS/FreshRSS/blob/master/README.md) qu’un Raspberry Pi permet de faire tourner une instance de FreshRSS avec 150 flux (~22k articles) en moins d’une seconde.

## Partagez votre instance de FreshRSS

Parmi les demandes récurrentes, on trouvait le mode multi-utilisateurs. Et bien ce mode est désormais disponible ! :) Vous pourrez désormais inviter vos amis, votre famille ou n’importe qui vous rejoindre sur votre instance FreshRSS.

Petit point technique tout de même pour ne pas que vous soyez surpris. FreshRSS n’a pas été pensé à la base pour du multi-utilisateurs. Aussi, nous n’avons pas touché à la structure de la base de données car ça aurait demandé beaucoup de changements. La solution choisie est de créer de nouvelles tables en BDD, préfixées du nom d’utilisateur. Aussi la taille de votre base de données risque d’exploser si vous invitez beaucoup de monde. Vous êtes prévenus ;).

Une autre petite nouveauté : un nouveau moyen d’authentification, plus traditionnel (formulaire à remplir). C’est donc aussi une demande de longue date qui a été comblée là. Petit bonus sécurité : si vous ne possédez pas une instance sécurisée de FreshRSS (protocole HTTPS), le mot de passe ne transite pas pour autant en clair sur le réseau. Celui-ci est d’abord hashé côté client en JavaScript avant d’être envoyé. C’est cadeau, mais ça force l’utilisation du JavaScript (m’enfin, vous me direz, ça fait un moment que JavaScript est requis pour l’utilisation de FreshRSS).

## Lisez l’actualité, même la nuit !

La nuit, tous les chats sont gris, mais votre écran d’ordinateur continue d’être blanc comme neige quand vous êtes sur FreshRSS. Dans un soucis de santé publique, un nouveau thème sombre a été développé en association avec les meilleurs contributeurs du moment (merci à [@aledeg](https://github.com/aledeg) !). Vos yeux vous remercieront.

## Partagez les articles qui vous intéressent

Facebook, Google+, Twitter ? Vous ne connaissez pas et vous préférez [Shaarli](http://sebsauvage.net/wiki/doku.php?id=php:shaarli), [Diaspora*](https://diasporafoundation.org/) ou encore [wallabag](http://www.wallabag.org/) ? Ces six moyens de partage sont désormais disponibles. Nous avons aussi pensé à ceux qui n’en ont rien à faire : vous pouvez les faire disparaître de la liste si vous n’en voulez pas.

## Migrez de la 0.6 à la 0.7 sans tout casser

Il y aurait bien encore des tas de choses à ajouter sur cette version qui est sans doute la plus conséquente de toutes (nous avons dépassé la barre des 1000 commits !), mais je vais finir en évoquant la mise à jour. Comme évoqué plus haut, il y a eu de gros changements tant au niveau base de données qu’architecture des fichiers. J'avais prévenu lors de la sortie de la 0.6 : tout a été cassé.

Attention : VOUS NE POURREZ PAS FAIRE CETTE MISE À JOUR SIMPLEMENT À LA MAIN.

Mais on ne vous laisse pas tout seul pour autant et un script de mise à jour est fourni. Suivez bien ces étapes si vous ne voulez pas tout casser :

1. Vérifiez que vous êtes bien en version 0.6. Un moyen simple de le savoir est de regarder dans la configuration si vous pouvez gérer les icônes de lecture. Si oui, alors vous êtes en 0.6 (ou alors déjà en 0.7 mais alors vous saurez très bien vous débrouiller tout seul :)).
2. Faites une copie de `./app/configuration/application.ini` et de `./public/data/Configuration.array.php`.
3. Supprimez le reste
4. Déployez la version v0.7 (depuis [l’archive ZIP](https://github.com/FreshRSS/FreshRSS/archive/master.zip) ou par [git](https://github.com/FreshRSS/FreshRSS))
5. Placez `application.ini` et `Configuration.array.php` dans `./data/`
6. Si vous avez peur, faites une sauvegarde de votre base de données via phpMyAdmin ou grâce à la commande MySQL : `mysqldump -u utilisateur -p --databases freshrss > freshrss.sql`
7. Lancez l’installation de la version 0.7 qui fera la mise à jour automatiquement (cette fois donc, il NE FAUT PAS supprimer le fichier d’installation)

Il n’y a eu aucun soucis lors de nos tests, il n’y a pas de raison que vous en ayez :) Et sinon, ce sont les risques de ne pas utiliser les versions 1.0 :p

## Ces contributeurs sans qui rien n’aurait été possible

Alors que je révisais, passais mes partiels, rentrais en France, passais les fêtes en famille, déménageais et vivais dans le froid à la recherche d’un appartement, le projet a été maintenu à bout de bras, une fois de plus, par [@alkarex](https://github.com/Alkarex). Je veux dire, VRAIMENT. Une (très) grosse partie (quasiment la totalité) des améliorations que vous verrez dans cette version est de son propre crû.

Cette version 0.7 aura aussi vu l’arrivée des contributions d’@aledeg, évoqué plus haut pour son thème "Dark". Ce n’est pas sa seule contribution vu qu’il a aussi ajouté un raccourci pour refermer l’article en cours (la navigation est plus sympa comme ça), créé de nouveaux filtres pour la vue et surtout commencé un travail que j’avais prévu de commencer il y a un petit moment : la page de statistiques ! (j’en parle dans la partie suivante). [@Bubbendorf](https://github.com/Bubbendorf) n’est pas à oublier non plus puisqu’il a aidé au formatage des nombres et participé à de nombreuses reprises aux tickets de bug.

Enfin je n’oublie pas, comme toujours, toutes les personnes qui remontent des petits bugs par-ci par-là et/ou qui encouragent et qui font avancer mine de rien FreshRSS. Même si ce n’est pas toujours grand chose, ce sont ces petits trucs qui permettent de parfaire le logiciel. La version 1.0 n’en sera que meilleure ! Puis quand je vois, à chaque nouvelle version, le nombre toujours grandissant d’utilisateurs, on ne peut qu’être confiant dans la qualité de FreshRSS :]

## Bonus : tout savoir sur vos abonnements en un clin d’œil

C’est tout chaud, ça sort du four et c’est la dernière "killer feature" de FreshRSS : la page des statistiques. Vous pourrez dorénavant savoir d’un coup d’œil tout un tas de choses sur vos abonnements. Pour l’instant ça reste limité à la répartition des articles (globalement ou par catégorie), à la répartition des flux par catégorie, au nombre d’articles parus par jour sur les 30 derniers jours et aux flux les plus productifs.

D’autres graphiques devraient apparaître dans les prochaines versions, mais si vous avez des idées, n’hésitez pas à nous les faire savoir !

## 0.8 en approche

Sortir une nouvelle version est synonyme de développement d’une autre. Si vous avez aimé la 0.7, la 0.8 devrait le confirmer. La grosse nouveauté sera [l’arrivée d’une API](https://github.com/FreshRSS/FreshRSS/issues/13) qui permettra donc de connecter d’autres applications sur FreshRSS. Cela sera utile surtout sur mobile où tout le monde ne veut pas utiliser la version mobile de FreshRSS (qui est parfaitement responsive design, je le rappelle).

Je ne m’étend pas sur les autres nouveautés qui pourraient éventuellement arriver (catégories arborescentes, gestion de SQLite, import/export des favoris, etc.) car c’est le genre de choses que j’ai tendance à toujours renvoyer à la version d’après :p

En tout cas, la version 1.0 n’a jamais été aussi proche et j’aimerais la voir arriver dans les six prochains mois ! Y a plus qu’à coder !
