Date: 2015-01-31 15:35
Title: FreshRSS un point zéro
Slug: freshrss-un-point-zero
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Elle est là, enfin ! La version 1.0 de FreshRSS, meilleur agrégateur à auto-héberger au monde (du moins selon moi), est sortie.

Ceux qui me suivent un peu savent que je tenais tout particulièrement à cette version. En effet, je ne pouvais considérer mon logiciel terminé sans cette version qui venait clôre un cycle particulièrement important de développement (deux ans et trois mois tout de même !). Cette nouvelle version vient changer un petit paquet de choses. J’expliquerai néanmoins dans un prochain article plus personnel pourquoi j’ai (un peu) précipité la sortie de cette version.

Les liens habituels :

* Le site officiel : [freshrss.org](http://freshrss.org)
* La démo en version 1.0 : [demo.freshrss.org](http://demo.freshrss.org)
* Le dépôt de code [sur GitHub](https://github.com/FreshRSS/FreshRSS)
* Le bugtracker aussi [sur GitHub](https://github.com/FreshRSS/FreshRSS/issues)

## Les différentes versions bêta

Depuis fin septembre, quatre versions différentes sont sorties. La dernière fut la 0.9.4 sortie à la mi-janvier. Voici un petit tour des ajouts et modifications de chaque version.

### FreshRSS 0.9.1

Sortie en même temps que la discrète 0.8.1, il s’agissait en fait d’une version de stabilisation. Pas grand chose de neuf si ce n’est des corrections de bugs et quelques ajouts au système de statistiques.

### FreshRSS 0.9.2

Une version qui a demandé pas mal de boulot puisqu’il s’agissait de réécrire toute la partie de gestion des abonnements avec, notamment, la possibilité de réorganiser ses flux par « drag and drop ».

J’en profitai aussi pour revoir la colonne de gauche listant les catégories et flux lors de la consultation des articles. Plus jolie, plus pratique.

Les changements sous le capot n’étaient pas en reste car c’était l’occasion de réécrire des parties importantes comme l’authentification et la récupération des articles. Toute la partie coding style était aussi améliorée.

### FreshRSS 0.9.3

Une version un peu moins conséquente que la précédente et pour cause, je participais au NaNoWriMo durant le mois de novembre. Je n’ai pas pu être aussi présent pour cette version. Néanmoins on a vu arriver un nouveau sélecteur de thème (made by [@aledeg](https://github.com/aledeg)) et le nouveau thème BlueLagoon (made by [@Mister aiR](https://github.com/misterair)). Une fonctionnalité assez démandée arrivait aussi : la disparition des articles une fois lus.

Sous le capot, j’ai tout de même profité de la première quinzaine de décembre pour réécrire toute la partie qui gérait l’internationalisation de FreshRSS. Un travail long et pénible mais qui facilite pas mal la gestion des langues.

### FreshRSS 0.9.4

Une version bien sympathique puisqu’elle apportait avec elle le système d’extensions. Celui-ci n’était pas aussi complet que je l’avais voulu mais ça m’aurait demandé tellement de travail que j’ai préféré le sortir un peu limité dès la mi-janvier. Quelques extensions [sont disponibles sur Github](https://github.com/FreshRSS/Extensions).

J’en ai profité pour faire quelques gros changements sous le capot comme la réécriture du système de configuration et du système de partage (qui devrait encore évoluer dans le futur) et une réorganisation des fichiers utilisateurs.

## FreshRSS 1.0.0

Pour ceux qui tournent en 0.9.4, vous ne verrez rien de très neuf. En effet, je n’ai pas voulu intégrer trop de changements pour éviter une version majeure toute cassée. La version 1.0 sera donc uniquement une version de stabilisation de la 0.9.4 avec quelques jolis et moins jolis bugs en moins (et notamment [celui qui m’aura valu la plus belle prise de tête](https://github.com/FreshRSS/FreshRSS/issues/700) depuis que je fais du PHP).

Une nouveauté tout de même intéressante, c’est l’arrivée de l’Allemand comme langue supportée par FreshRSS ! (j’en reparle plus loin)

### Mise à jour

Depuis la version 0.8.0, FreshRSS dispose d’un système de mise à jour automatique (et ça c’est la grande classe !). Aussi vous n’avez qu’à vous rendre dans l’interface d’administration pour lancer la mise à jour. **Note importante** : je déconseille fortement d’utiliser une autre méthode au risque de casser votre instance ! Seul le système de mise à jour automatique permet de mettre à jour facilement les fichiers situés dans votre répertoire `./data`.

Précision : deux versions sont sorties en même temps, la 1.0.0 (stable) et la 1.1.0 (la même mais dans la branche bêta). Cela permet d’avoir une vraie distinction entre les branches et surtout de faciliter le processus de mise à jour.

## La suite : un projet plus communautaire

J’en parlerai plus en détail dans l’article qui suivra mais je compte m’effacer un peu du projet FreshRSS afin de bosser sur d’autres choses et voyager. Je sais que ça sonne un peu comme un Mozilla annonçant l’arrêt des améliorations de Thunderbird ou les développeurs de Diaspora* annonçant leur retrait du projet qu’ils avaient initié, mais cela a plutôt bien réussi pour ce dernier. Comme il serait idiot que FreshRSS coule à cause de mon absence, j’ai déjà entreprit quelques taches pour rendre le projet plus ouvert aux contributions.

Tout d’abord, j’ai déplacé le dépôt de l’adresse [https://github.com/marienfressinaud/FreshRSS](https://github.com/marienfressinaud/FreshRSS) à l’adresse [https://github.com/FreshRSS/FreshRSS](https://github.com/FreshRSS/FreshRSS). Ça peut paraître assez peu mais cela m’a permis de donner les « clés » officiellement à @Alkarex et @aledeg qui peuvent gérer tous les dépôts associés [à l’organisation FreshRSS](https://github.com/FreshRSS). De plus cela a impliqué de détacher FreshRSS de mon nom : ça peut paraître assez égoïste mais je suis assez fier d’avoir eu ce succès avec cet agrégateur ; le lier à un compte « global » signifie que je ne suis plus qu’un contributeur comme les autres. Au final je suis content de ce choix qui permet de ne pas avoir une tête qui dépasse mais seulement un projet global avec des contributeurs, c’est la continuation logique d’un projet Libre selon moi.

La prochaine étape va être d’ouvrir un blog spécifique à l’actualité de FreshRSS plutôt que de publier ici.

Une tâche importante que je dois terminer va être de documenter conséquemment le code ainsi que les fonctionnalités pour favoriser les contributions externes. J’ai déjà commencé mais il faut finir maintenant.

Le fait que je me retire un peu du projet ne signifie pas non plus que je ne vais plus y participer : je continuerai de donner mes idées, mes avis et surement même quelques lignes de code. Je me propose aussi pour continuer de gérer les scripts de mise à jour et les sorties de version (mais j’écrirai des tutoriels pour faire ça).

Par contre très concrètement, j’aimerais désormais éviter que l’on me contacte directement par mail par rapport à FreshRSS (même si ça fait toujours plaisir ;)).

Pour conclure, j’ai une véritable volonté de faire de FreshRSS un projet plus communautaire. Je crois qu’il n’existe pas aujourd’hui d’agrégateur libre véritablement bati autour d’une communauté : [Kriss Feed](http://tontof.net/kriss/feed/) et [Leed](http://leed.idleman.fr/) n’ont pas donné signe de vie depuis plusieurs mois, [TinyTinyRSS](http://tt-rss.org/redmine/projects/tt-rss/wiki) est un gros projet mais son développeur principal est « assez peu ouvert » de ce que j’en ai compris et par contre je ne sais pas ce qu’il en est vraiment de [selfoss](http://selfoss.aditu.de/). Dans tous les cas ces projets restent batis autour d’une personnalité et non pas du logiciel en lui-même.

## Les remerciements

Comme à chaque fois, je tiens à remercier les différents contributeurs. En plus d’[@Alkarex](https://github.com/Alkarex) et d’[@aledeg](https://github.com/aledeg), [@Alwaysin](https://github.com/Alwaysin) et [@Draky50110](https://github.com/Draky50110) continuent toujours de beaucoup commenter, tester et donner leur avis. Leur présence permet de tester différents cas et trouver des bugs oubliés.

Nous avons eu aussi dernièrement une forte présence du côté de l’Allemagne avec deux articles [coup](http://www.robbenradio.de/dc/index.php?post/53) sur [coup](http://linuxundich.de/gnu-linux/tiny-alternative-freshrss-installieren-news-plus-handy/) et l’arrivée d’un contributeur ([@ealdraed](https://github.com/ealdraed)) qui a traduit l’ensemble du projet en très peu de temps. Une très bonne surprise !

Autre énorme surprise, celle d’avoir été cité en octobre sur le site [opensource.com](http://opensource.com) dans [un article de blog présentant cinq logiciels](http://opensource.com/life/14/10/five-open-source-alternatives-popular-web-apps) (dont [wallabag](https://www.wallabag.org/) et [shaarli](http://sebsauvage.net/wiki/doku.php?id=php:shaarli)). Trois jours plus tard [c’était un tweet](https://twitter.com/nixcraft/status/523303778041077761) de [@nixCraft](https://twitter.com/nixcraft) (+40 000 abonnés) qui ramenait du monde.

Suite à la série d’articles qu’il y a pu avoir autour de FreshRSS, j’ai ouvert [un ticket spécial](https://github.com/FreshRSS/freshrss.org/issues/9) listant ces articles. Vous êtes libres de pointer les votres ou ceux que vous croisez :).

Il y a encore des tas de personnes à remercier : [dada](http://www.dadall.info/blog/) pour ses articles, [@plopoyop](https://github.com/plopoyop) pour l’intégration dans [Yunohost](http://yunohost.org/), toutes les personnes qui remontent des bugs, qui envoient des mails pour remercier et féliciter, [Cyrille Borne](http://cyrille-borne.com/) qui a écrit quelques mots sur son (ses ?) blog, [Mister aiR](http://remitaines.com/) pour ses thèmes et ses idées de continuité du projet (avec [@dhoko](https://github.com/dhoko)), [Luc Didry](https://fiat-tux.fr/) qui a apporté des pistes pour améliorer l’aspect multi-utilisateurs… et tous ceux que j’oublie !
