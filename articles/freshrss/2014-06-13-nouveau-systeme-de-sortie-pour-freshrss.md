Date: 2014-06-13 19:52
Title: Nouveau système de sortie pour FreshRSS
Slug: nouveau-systeme-de-sortie-pour-freshrss
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Il y a environ quatre mois et demi sortait FreshRSS 0.7 et une vingtaine de jours plus tard le patch 0.7.1. Depuis ? Rien de visible à priori puisque la page principale de Github n’affichait plus aucune activité. Néanmoins ne vous méprenez pas : même si je ne suis pas tout le temps très actif, le projet continue de vivre activement grâce à de fantastiques contributeurs ! Tout se passe dans les tréfonds de [la branche de développement](https://github.com/FreshRSS/FreshRSS/tree/dev)…

Partant du constat que l’activité n’était pas suffisamment visible, nous avons décidé de créer une branche bêta qui sera mise à jour tous les mois avec les nouveautés de la branche de développement. La branche bêta est désormais celle par défaut, celle qui est visible en atterrissant sur [https://github.com/marienfressinaud/FreshRSS](https://github.com/FreshRSS/FreshRSS). J’espère que cela montrera que FreshRSS est bel et bien vivant !

Voici le nouveau fonctionnement de sortie des versions (inspiré du système mis en place par Gnome) :

* Les numéros impairs (0.7.x, 0.9.x) correspondent aux versions bêta et les numéros pairs (0.8.x, 0.10.x) aux versions "stables"
* Nous travaillons sur la branche `dev` avec une feuille de route menant vers une version stable
* Chaque mois, la branche dev est stabilisée **un minimum** et est mergée dans la branche `beta` avec un nouveau numéro de version (0.7.2 par exemple)
* Une fois que toutes les nouveautés prévues pour la version stable sont présentes, on stabilise un maximum le tout et on merge dans la branche `master` avec un nouveau numéro de version (0.8.0 par exemple)

En résumé : vous préférez la stabilité ? Utilisez la branche `master`. Vous aimez connaître les nouveautés avant les autres ? Préférez la branche `beta` qui est celle par défaut. Vous êtes un fou dans votre tête et vous aimez quand tout est cassé ? Alors essayez la branche `dev` !

Note : je ne préviendrai pas des sorties des versions bêta donc si vous voulez vous tenir au courant, abonnez-vous [au flux RSS dédié sur Github](https://github.com/FreshRSS/FreshRSS/commits/beta.atom).

## Les dernières nouveautés

Pour me démentir je vous annonce quand même la sortie de la version bêta 0.7.2. Petite exception car il y a quelques nouveautés sympathiques :

* Une API compatible Google Reader pour brancher des applications Android dessus ([ça reste peut-être un peu compliqué à utiliser la première fois](https://github.com/FreshRSS/FreshRSS/issues/13#issuecomment-36046833) mais ça fonctionne)
* Des boutons pour filtrer plus facilement (le menu se situant au-dessus des articles a été entièrement remanié)
* Un système de recherche plus poussé ([faites-vous plaisir en filtrant au niveau de la date](https://github.com/FreshRSS/FreshRSS/issues/511#issuecomment-44776991))
* Un nouveau système d’import/export qui permet de sauvegarder aussi les articles ! (basé sur le format Google Reader, normalement compatible)
* Le thème Origine a été remanié (entièrement réécrit mais ça ne devrait pas trop se voir ;))
* Une option pour ajuster la largeur du contenu des articles (4 options : fine, moyenne, large et pas de limite)

Malgré tout ça, il reste encore pas mal de boulot avant la 0.8 et quelques bugs à corriger. Je reviens avec un nouvel article dans les prochains jours pour prévenir de la suite des événements car il se pourrait que la tant attendue version 1.0 arrive plus tôt que prévu… Wait and see!
