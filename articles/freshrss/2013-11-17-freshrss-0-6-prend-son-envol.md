Date: 2013-11-17 13:28
Title: FreshRSS 0.6 prend son envol
Slug: freshrss-0-6-prend-son-envol
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Un mois après la sortie de la version 0.5, je vous présente FreshRSS 0.6. Toujours plus stable, toujours plus performant, toujours plus utile, FreshRSS deviendra très vite votre plus fidèle compagnon au quotidien. Vous ne pourrez bientôt plus vous en passer !

Pour vous en faire une première idée, [la démo a été mise à jour](http://demo.freshrss.org).

## Des performances accrues

Le gros du travail a porté cette fois-ci sur les performances coté client. On saluera le travail fantastique réalisé par [Alexandre Alapetite](https://github.com/Alkarex) (à vrai dire, c'est lui qui a quasiment tout fait), désormais contributeur régulier. Les performances ont été améliorées sur deux axes :

1. Le code JavaScript : celui-ci a été remanié en grande partie et son chargement grandement amélioré. Ce n’est pas encore parfait, mais une grande étape a été franchie
2. La mise en cache : l’utilisation d’un fichier .htaccess permet de garder en cache de manière plus fine les différents fichiers pour les utilisateurs d’Apache. De plus, pour limiter les requêtes inutiles coté serveur, les pages de FreshRSS sont désormais mises en cache de "manière globale" si aucun changement n'a été effectué depuis le dernier chargement.

## Du sucre fonctionnel

En plus des performances, on notera en vrac quelques améliorations sympathiques (issues du changelog) :

* Mise à jour automatique du nombre d’articles non lus
* Amélioration des retours utilisateur lorsque la configuration n’est pas bonne (vous savez, cette fameuse page blanche...)
* Meilleure prise en charge des flux RSS invalides
* Amélioration de la vue globale (toujours un peu plus à chaque version)
* Possibilité de personnaliser les icônes de lecture
* Suppression de deux champs "inutiles" lors de l'installation (base_url et sel)

## Toujours pas de version 1.0 !

L’arrivée d’Alexandre en contributeur régulier a amené aussi pas mal de conseils pour améliorer les performances. Ceci a donc encore repoussé en partie certaines fonctionnalités et, par conséquent, l’arrivée de la version 1.0. Je ne m’en plains absolument pas car FreshRSS est de plus en plus agréable à utiliser et cela signifie que lors de la sortie de la version finale, il devrait être particulièrement stable et performant.

## Mise à jour

Pas de modification au niveau de la base de données cette fois-ci, tout va bien. Du coup comme d’habitude :

1. [Téléchargez la nouvelle version](https://github.com/FreshRSS/FreshRSS/archive/0.6.0.zip)
2. Écrasez les fichiers des répertoires "app", "lib" et "public". Faites attention à garder vos anciens `app/configuration/application.ini` et `public/data/Configuration.array.php`
3. Supprimez le fichier `public/install.php` car vous n’en avez pas besoin dans le cas d’une mise à jour

Ou un simple `git pull` si vous préférez utiliser Git ;)

**Attention :** je vous préviens tout de suite, mais ce ne sera pas aussi facile pour mettre à jour vers la version 0.7. Il y aura de **très** gros changements au niveau de la base de données et de l’architecture des fichiers. Si vous suivez la version de dev, faites attention !

## La version 0.7, celle qui va tout casser

Contrairement à ce que je fais d’habitude, je ne vais pas faire de liste des fonctionnalités pour la version 0.7. Je vous redirige plutôt vers le [gestionnaire de bugs](https://github.com/FreshRSS/FreshRSS/issues?milestone=6) sur Github qui est toujours à jour.

Néanmoins, comme je le disais juste au-dessus, cette version devrait casser pas mal de choses, donc prenez garde, notamment sur la version de développement.

## Pour finir

Merci encore une fois pour tous les retours que vous m’avez fait, c’est très encourageant et je le répète, c’est grâce à ça que FreshRSS est ce qu’il est aujourd’hui. Je profite aussi de cet article pour vous renvoyer sur [un journal que j’ai écrit sur LinuxFR](http://linuxfr.org/users/fargo/journaux/freshrss-1-an-et-quelques-dents) pendant que mon site était mort. Je parle surtout de l’année passée à développer mon agrégateur de flux RSS et sur ce que m’a apporté ce travail. C’est un peu long, mais ça peut valoir le coup de le lire si vous souhaitez vous lancer dans un projet personnel.
