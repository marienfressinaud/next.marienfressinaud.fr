Date: 2014-09-23 23:46
Title: FreshRSS 0.8 : retour sur sept mois de travail
Slug: freshrss-0-8-retour-sur-sept-mois-de-travail
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Nous y voilà, la nouvelle version de la branche master de FreshRSS sort aujourd’hui, sept mois après la version 0.7.1 !

Comme les grosses nouveautés de cette version ont déjà été annoncées lors des sorties des versions bêta (0.7.2, 0.7.3 et 0.7.4), je ne vais faire qu’un rappel des grosses nouveautés.

Avant de débuter, quelques liens utiles :

* Le site officiel : [freshrss.org](http://freshrss.org/)
* La démo en version 0.8 : [demo.freshrss.org](http://demo.freshrss.org/)
* Le dépôt de code [sur GitHub](https://github.com/FreshRSS/FreshRSS)
* Le bugtracker aussi [sur GitHub](https://github.com/FreshRSS/FreshRSS/issues)

## Retour sur les versions bêta

La version 0.7.2 (première bêta) voyait l’arrivée de l’API compatible avec les applications Google Reader. Un gros travail a été fait par Alkarex ici et qui a mené à la possibilité d’utiliser deux applications mobiles différentes sur Android ([EasyRSS](https://github.com/Alkarex/EasyRSS) et [News+](https://play.google.com/store/apps/details?id=com.noinnion.android.newsplus.extension.google_reader)). Je profitai aussi de cette version pour introduire le nouveau système d’import / export des flux et des articles. Ce nouveau système permet notamment d’exporter ses favoris pour les réimporter ailleurs. Le format utilisé est celui de feu Google Reader (encore lui !) et il est donc possible d’importer les articles que vous avez potentiellement gardé dans un coin de votre <del>disque dur</del> SSD.

La version 0.7.3 ajoutait le support de SQLite pour le système de stockage en plus de MySQL/MariaDB. C’était une **très** forte demande de la part de nombreuses personnes et j’avoue que c’est agréable de ne pas avoir à taper toutes les informations de base de données à chaque fois (SQLite génère un simple fichier par utilisateur).

La grande nouveauté de la version 0.7.4 était le magnifique thème Screwdriver de Mister aiR. Beaucoup de compliments ont été fait à son sujet et je trouve qu’il permet bien de mettre en valeur FreshRSS.

## La version 0.8

Aujourd’hui sort donc la version 0.8, une grande étape pour la suite puisqu’elle signe le début d’une forte activité de ma part dans le développement de FreshRSS. En effet, comme je l’expliquais en juin dernier, j’ai débuté neuf mois sabbatiques (dont trois de voyage) pour me consacrer à mes projets personnels. Une priorité que je me suis donné et de sortir un version 1.0 d’ici la fin de l’année. Cette version 0.8 marque donc la fin de mes études (avec deux semaines de retard, certes). J’ai pas mal travaillé dessus pour corriger des petits détails qui avaient leur importance (que ce soit en termes de sécurité, de design ou de fonctionnalités).

La grosse nouveautés de cette version, c’est le nouveau système de mise à jour. Si auparavant il était nécessaire de faire pas mal de manipulations à la main, désormais il vous suffira de cliquer sur un bouton, éventuellement préciser certaines informations et confirmer la mise à jour. Le reste, c’est FreshRSS qui s’en chargera.

Une plus petite amélioration qui me tenait à cœur, c’est l’affichage des statistiques (que l’on doit par ailleurs à aledeg qui a fait du très bon travail à ce niveau-là !). J’ai donc réorganisé les différents blocs pour une meilleure utilisation de l’écran ce qui, j’espère, rend les choses plus agréables à regarder.

Pour terminer sur les nouveautés de cette version 0.8, j’ai intégré ce matin même un nouveau thème : [Pafat](https://github.com/plopoyop/FreshRSS/tree/dev/p/themes/Pafat) par [plopoyop](https://github.com/plopoyop). Merci à lui !

## Passer d’une 0.7.x en 0.8

Comme le nouveau système de mise à jour n’est présent qu’à partir de la 0.8, il va vous falloir faire une dernière mise à jour à la main ! Avant cela je vous conseille, si vous ne l’avez pas fait régulièrement, de vous assurer d’être en version 0.7.4 (visible dans "À propos"). Changer de version se fait facilement :

* Téléchargez FreshRSS : [0.7.4](https://github.com/FreshRSS/FreshRSS/archive/0.7.4.zip) ou [0.8.0 (stable)](https://github.com/FreshRSS/FreshRSS/archive/0.8.0.zip) ou [0.9.0 (bêta)](https://github.com/FreshRSS/FreshRSS/archive/0.9.0.zip).
* Décompressez l’archive.
* Supprimez les fichiers et répertoires de votre installation précédente **à l'exception du répertoire `./data`**.
* Remplacez ce que vous venez de supprimer par les fichiers extraits de l'archive à l'étape 2.

## Une communauté qui s’agrandit

Si je pouvais déjà compter sur le fort soutien d’Alkarex et d’aledeg pour le code et d’Alwaysin pour remonter les bugs et les demandes de fonctionnalités, je remarque que les contributions ponctuelles sont de plus en plus nombreuses. Il y a notamment eu, fin août, en l’espace d’une semaine un Anglais, un Pakistanais et un Allemand qui m’ont chacun envoyé un mail. Fou !

On notera aussi une certaine activité du tag [#FreshRSS sur Diaspora*](https://diaspora-fr.org/tags/freshrss), initié par moi-même évidemment mais avec pas mal de personnes qui semblent en être satisfait :)

En tout cas, je me répète à chaque fois, si vous avez une question, une proposition, un soucis, envie de remercier ou que sais-je d’autre, GitHub et mon adresse mail vous sont ouverts !

## Une démarche qualité

Comme je sais que mon maître d’apprentissage de ces trois dernières années a toutes les chances de passer par là, j’en profite pour aborder un sujet qui lui est cher :).

Depuis quelques temps, nous commençons à travailler Alkarex, aledeg et moi à améliorer la qualité globale de FreshRSS. J’avais débuté mon agrégateur comme quelque chose de personnel qui n’intéresserait probablement pas grand monde et cela se ressent. C’est pourquoi :

* Les règles de "coding style" [ont bien été définies](http://doc.freshrss.org/fr/developers/01_First_steps/) et je m’applique à rendre peu à peu tout le code conforme à celui-ci.
* J’ai commencé à m’attaquer plus sérieusement à [la documentation sur doc.freshrss.org](http://doc.freshrss.org) (en français pour le moment).
* Nous allons prochainement [mettre en place des tests](https://github.com/FreshRSS/FreshRSS/issues/577) pour éviter les régressions.
* aledeg a initié un mouvement "[Commentons le code !](https://github.com/FreshRSS/FreshRSS/pull/630/files)".

J’espère que ces différentes initiatives permettront d’y voir plus clair et de faciliter les modifications du code source… C’est le but en tout cas !

## La suite

Je l’ai dit : la version 1.0 est prévue d'ici la fin de l'année. Pour être plus précis, voici ce que je prévois pour les mois qui approchent :

* Aujourd'hui : sortie des versions 0.8 et 0.9 (son pendant dans la branche bêta).
* 10 octobre : sortie des versions 0.8.1 et 0.9.1, les versions de maintenance (je ne me fais pas trop d’illusion, il reste toujours quelques bugs).
* 31 octobre : sortie de la version 0.9.2, uniquement une version bêta donc.
* 30 novembre : sortie de la version 0.9.3. À noter que durant cette période je participerai aussi au Nanowrimo qui me prendra énormément de temps.
* 19 décembre : sortie de la version 1.0. J'évite une sortie le 25 décembre parce que je risque d’avoir moins de retours même si la date m'aurait bien plu.

Au programme, beaucoup de choses :

* Un système de plugins
* Refonte de la gestion des catégories
* Un système de web-cron
* Une meilleure gestion du mode multi-utilisateurs (qui reste basique actuellement)
* Support de PostgreSQL
* Le support de l’allemand (initialement prévu pour la 0.8 mais finalement reporté)
* Une nouvelle API (basée sur celle de TTRSS) ?
* [Et plein d’autres choses !](https://github.com/FreshRSS/FreshRSS/milestones/0.9.0)

Beaucoup de travail donc, mais contre-balancé par une très forte motivation de ma part. Je vais bientôt fêter les deux ans du projet et je vois enfin le bout du tunnel ! Le plus motivant est que j’ai réussi à amener FreshRSS à un niveau de maturité que je n’imaginais même pas au départ et que mon application connait apparemment un certain succès dans le milieu francophone.

Comme je suis motivé, je prévois quelques suprises pour la sortie de la 1.0 (voire avant) qui devrait permettre à FreshRSS de s’imposer comme une solution incontournable dans le monde des agrégateurs de flux RSS. Ouais, rien que ça. *Trust me, I’m an engineer*.
