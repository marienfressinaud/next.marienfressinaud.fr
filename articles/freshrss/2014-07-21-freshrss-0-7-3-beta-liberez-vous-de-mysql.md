Date: 2014-07-21 18:47
Title: FreshRSS 0.7.3 (bêta) : libérez-vous de MySQL
Slug: freshrss-0-7-3-beta-liberez-vous-de-mysql
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Comme prévu dans l’un de mes derniers articles, voici, avec (à peine) plus de cinq semaines de développement, une nouvelle version bêta de FreshRSS !

Au programme, des petites nouveautés :

* Réécriture des thèmes Flat et Dark pour mieux correspondre aux "standards" définis
* Ajout d’une limite configurable pour la fréquence de synchronisation des flux. Je vois souvent des gens qui rafraîchissent leur flux toutes les une ou deux minutes : ça ne sert à rien et vous bouffez les ressources des sites ! Avec cette fonctionnalité, nous vous limitons cette possibilité en vous imposant une fréquence d’au moins 20 minutes entre deux actualisations (une heure par défaut).
* Ajout d’un système de filtres (queries en anglais). Ce sont, en gros, des raccourcis que vous créez à partir de vos recherches. Imaginons que vous recherchiez le thème "FreshRSS" apparu dans les articles de la semaine dernière (`date:P1W FreshRSS`) dans la catégorie "Les trucs que j’aime". Au lieu de devoir taper ça à chaque fois, vous n’avez qu’à créer un filtre à partir de la page courante et le raccourci se glisse dans la barre des filtres.

Ça c’était pour les petites news. Il en reste une qui devrait faire plaisir à pas mal de monde : **[le support de SQLite ! \o/](https://github.com/FreshRSS/FreshRSS/issues/100)** Il s’agissait d’une demande de longue date mais qui nécessitait plus de travail que prévu (le langage SQL, bien que standardisé, se trouve géré bien différemment selon les moteurs SQL). Notez bien que SQLite n’est pas la solution que je recommande si vous avez de grosses quantités de données : nos premiers tests sont plutôt concluant mais les requêtes ne sont pas particulièrement optimisées. Si par contre vous ne suivez que quelques flux et / ou que vous ne stockez pas les articles sur de longues durées, ça peut sans doute vous convenir.

Avec le support de SQLite, à priori FreshRSS devrait attirer plus de monde. Par conséquent j’espère avoir encore plus de retours :)

Avec tout ça, j’ai aussi débuté en parallèle [la documentation](http://doc.freshrss.org/) un peu plus sérieusement. Pas grand chose de plus, mais ça va prendre forme au fur et à mesure. Si certains sont motivés pour donner un coup de main, [un ticket est ouvert pour en parler](https://github.com/FreshRSS/FreshRSS/issues/123).

Niveau communauté, je remercie une fois de plus les contributeurs les plus actifs, [Alexandre](https://github.com/Alkarex) et [Alexis](https://github.com/aledeg), pour leur super travail ! Il y a aussi tous ceux qui remontent les bugs : je ne les nomme pas individuellement parce que, mine de rien, il commence à y en avoir pas mal. Ce serait d’ailleurs sympa que GitHub propose un système pour voir les plus gros contributeurs en nombre de commentaires dans les tickets. En attendant vous pouvez toujours [tous les retrouver sur GitHub](https://github.com/FreshRSS/FreshRSS/issues)… Enfin presque : n’oublions pas tous ceux qui m’envoient des mails que ce soit pour encourager, remercier, remonter des problèmes ou pour suggérer des améliorations.

En guise de conclusion, j’en profite pour diriger les âmes aventureuses vers [le projet Freeder](https://github.com/FreederTeam/Freeder) que j’ai découvert suite à une discussion (par mail) avec [Phyks](http://phyks.me/) et qui cherche à "réinventer" le concept d’agrégateur de flux RSS. Ça me semble un projet intéressant et ça pourrait être l’occasion de mettre en application des idées que je ne pourrai pas mettre en place dans FreshRSS.
