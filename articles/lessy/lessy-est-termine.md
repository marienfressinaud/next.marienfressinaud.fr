---
title: Lessy est terminé
date: 2022-04-12 10:18
---

Lessy <del>est</del> était un gestionnaire de temps mettant à disposition une liste de tâches et un gestionnaire de projets.

Il y a un an, [j’écrivais](des-nouvelles-de-lessy.html) :

> En l’état, Lessy m’est en fait relativement peu utile. […] Ma manière de gérer mes tâches a évolué et j’arrive la plupart du temps à me contenter d’un bout de papier […].
> 
> Lessy, dans sa version actuelle, correspond en fait pas mal à ce que je souhaitais avoir quand j’ai commencé son développement. […]
> 
> **Tout bien pensé, je crois que Lessy est terminé.**

Depuis, rien n’a vraiment changé. Je gère toujours mes tâches sur un bout de papier, aidé en plus par un mécanisme de routines qui ne nécessite que d’une feuille de tableur (je vous en reparle prochainement). Je continue également de penser que Lessy, dans sa forme actuelle, correspond à ce que j’imaginais initialement et qu’il n’a plus besoin de changer.

Le problème, c’est que je ne savais pas trop quoi faire de lui. **En octobre dernier, j’ai décidé de fermer les inscriptions sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh).** C’était déjà un petit soulagement.

Mais que faire alors du code existant ? Initialement, je pensais [terminer](terminer-ses-projets.html) Lessy proprement en n’ajoutant plus de fonctionnalités, mais en continuant de mettre à jour les dépendances logicielles afin d’éviter des failles de sécurité. Mais je dois vous avouer que je n’en suis plus capable. Les dépendances JavaScript sont une véritable plaie à maintenir avec des trucs qui pètent dans tous les sens à chaque mise à jour. C’est une charge que je n’ai pas envie de gérer.

Finalement, j’ai décidé de clore et archiver [le dépôt de code](https://github.com/lessy-community/lessy) un peu plus sauvagement. Le fichier README a été mis à jour pour expliquer que le projet n’est plus maintenu. **Je continuerai de maintenir le service [lessy.yuzu.ovh](https://lessy.yuzu.ovh) ouvert tant que des gens l’utiliseront** (et il y en a !) ; les inscriptions, elles, resteront fermées.

Je suis très heureux d’avoir bossé sur Lessy avec lequel j’aurai appris plein de choses. Mais je suis encore plus content de pouvoir annoncer qu’il est terminé et ainsi de ne plus avoir à m’en soucier 🙂
