Date: 2017-10-08 21:00
Title: Project Zero devient Lessy
Slug: project-zero-devient-lessy
Tags:
Summary: Petit projet devient grand et muri, l'occasion de remettre les choses à plat et de faire le tour de ce sur quoi je travaille en ce moment.

Il y a quelques jours, j’ai annoncé sur Mastodon que Project Zero devenait
officiellement Lessy, il était temps je l’officialise aussi sur ce blog. La
raison est multiple et je vais essayer de la détailler ici.

Pour rappel, Lessy (donc) est un gestionnaire de temps à base de gestion de
projets et de liste de tâches.

Tout d’abord, cela fait maintenant neuf mois (clairement pas à plein
temps&nbsp;!) que je travaille sur ce projet. Cela me prouve que je suis
motivé par son développement, d’autant plus que j’ai tout juste gratté les
fonctionnalités dont j’ai envie/besoin. Project Zero m’est devenu au fil des
mois **un outil indispensable**. J’en parlerai probablement dans un autre
article, mais en très bref, j’ai réussi grâce à lui à démarrer (et mener à
bien) plusieurs projets que je laissais trainer depuis des années. Aussi,
depuis mai, ma liste de tâches n’en contient plus datant de plus de deux
semaines, ce qui est une grande victoire pour moi&nbsp;!

De l’usage de cet outil, j’en ai tiré un tas d’idées un peu en vrac et j’avais
besoin de remettre un peu d’ordre dans tout cela. C’est pourquoi j’ai voulu
faire appel à une personne sensible à ma démarche et qui pourrait m’apporter un
regard extérieur. J’ai eu la chance le mois dernier d’avoir les retours et
idées de la part de [Marie-Cécile Paccard](http://www.mcpaccard.com/) qui est,
en <del>deux</del> quatre mots, UX designer à Lyon. Cela a eu pour effet de
rediriger mes efforts vers une toute autre direction… pour le mieux&nbsp;! En
effet, nous avons commencé à identifer ensemble une liste de valeurs et de
«&nbsp;missions&nbsp;» que pouvait se donner Project Zero. J’ai ensuite
continué de mon côté pour essayer d’identifier un objectif qui n’est pas encore
totalement défini (le travail est encore en cours). En revanche, j’ai déjà une
bonne vision de l’ensemble et l’un des éléments qui est revenu plusieurs fois
est **la notion de réduction de la «&nbsp;charge cognitive&nbsp;»**. Ce sera en
effet l’un des points fondamentaux qui dirigeront les développements futurs du
projet. J’y reviendrai en détails dans un futur article.

En parallèle de ce travail, je me suis rendu compte qu’il était essentiel que
j’obtienne plus de retours, plus variés, afin de faire émerger d’autres besoins
essentiels. Il est important pour moi que Project Zero soit utile à une portion
importante de la population non-geek, j’ai donc pour objectif d’orienter mes
prochaines actions pour rendre le projet **plus accueillant, plus
communautaire**. C’est pourquoi les deux prochaines «&nbsp;itérations&nbsp;»
(j’y reviendrai aussi) seront orientées vers une documentation plus aboutie
pour les nouveaux venus et une interface plus accueillante. Dans un tel
contexte, le nom devenait donc important pour ne pas rebuter les gens.

**Project Zero devient donc Lessy.** Le nom ne me plaisait pas depuis le
début&nbsp;: je l’avais plutôt utilisé comme nom de code, signifiant qu’il
faudrait que je commence par développer celui-ci avant d’attaquer un autre
projet. Il était clair depuis le début qu’il ne serait pas définitif. En
devenant Lessy, je souhaite faire passer la notion de «&nbsp;moins&nbsp;» (i.e.
*less* en anglais)&nbsp;: moins de stress, moins de charge cognitive. Je
souhaite que ce nom soit apaisant en quelque sorte. De plus, Lessy est un nom
particulièrement court, qui se retient et qui n’est quasiment pas utilisé
aujourd’hui ce qui permettra d’en faciliter le référencement sur Internet.

Le nouveau dépôt de code est toujours sur GitHub ([github.com/marienfressinaud/lessy](https://github.com/marienfressinaud/lessy))
mais le service que je mets (pour le moment, gracieusement) à disposition
**s'est déplacé vers l’adresse [lessy.yuzu.ovh](https://lessy.yuzu.ovh)**. Ceci dit, ne
vous jetez pas dessus, c’est toujours aussi moche.
