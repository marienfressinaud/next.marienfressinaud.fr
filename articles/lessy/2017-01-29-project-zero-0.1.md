Date: 2017-01-29 19:00
Title: Project Zero 0.1
Slug: project-zero-0-1
Tags:
Summary: Après un peu plus d’un mois de boulot, je sors Project Zero en version 0.1, petite présentation.

J’en parlais lors de [ma rétrospective 2016](https://marienfressinaud.fr/projet-zero-2016.html)
 sur ma méthodologie de travail, j’ai commencé à bosser sur une application qui
devrait me servir de cadre méthodologique. Jusqu’à maintenant j’utilisais le
système de fichiers de l’ordinateur pour organiser mes projets, mais c’était
trop contraignant et je n’arrivais pas à assurer un suivi suffisant.

Après un mois de travail acharné, j’ai enfin terminé la version 0.1 de
«&nbsp;Project Zero&nbsp;». Le code source est disponible [sur
GitHub](https://github.com/marienfressinaud/project-zero) sous licence MIT et
vous pouvez même vous créer un compte sur le service que je mets à
disposition&nbsp;: [zero.marienfressinaud.fr](http://zero.marienfressinaud.fr).
Attention, l’application n’est pas encore disponible en HTTPS&nbsp;!

## Pourquoi Project Zero&nbsp;?

Cela fait longtemps maintenant que j’imagine un outil de gestion de mes
projets. Bien sûr, je suis un éternel insatisfait des outils que j’utilise et
il fallait absolument que je développe mon propre logiciel&nbsp;:). Comme il
devait s’agir du projet à la base de tous mes autres projets, je l’appelais
«&nbsp;Projet Zéro&nbsp;». Fin 2015 je donnais un coup de frein à ma
contribution à [FreshRSS](https://freshrss.org/) pour commencer à bosser sur ce
fameux outil… chose que je n’ai jamais concrétisée car mes idées à l’époque
n’étaient absolument pas claires sur ce que je voulais faire.

C’est en août, pris dans la tourmente de mes nombreux projets commencés mais
dont aucun n’avançait, que je mettais en place quelques actions pour me
reconcentrer sur ce que je faisais. Cela a donné [une pré-version](https://marienfressinaud.fr/projet-zero-readme-md.html)
de ce que je voulais obtenir.

Fin décembre, voyant que ma méthodologie avait globalement bien fonctionné mais
que j’avais toujours des soucis à assurer le suivi, j’ai dessiné les premiers
écrans d’une application que j’ai donc nommé «&nbsp;Project Zero&nbsp;» (notez
le passage à l’anglais).

## Les (quelques) fonctionnalités

Il y a aujourd’hui très peu de fonctionnalités car je veux d’abord voir comment
évoluent mes besoins. Je préfère avoir très peu de fonctionnalités qui me
servent toutes que beaucoup qui ne me servent pas. Ce qu’il est possible de
faire&nbsp;:

- créer un compte (et s’authentifier, évidemment)&nbsp;;
- gérer ses projets (qui ne consistent qu’en un nom, une description en
  MarkDown et quelques dates)&nbsp;;
- démarrer jusqu’à trois projets en même temps en précisant une date de
  fin&nbsp;;
- stopper les projets qui stagnent&nbsp;;
- terminer les projets… terminés.

![Dashboard de Project Zero 0.1](images/projectzero/2017-01-29-0.1-dashboard.png)
![Vue d’un project dans Project Zero 0.1](images/projectzero/2017-01-29-0.1-project-show.png)

Il s’agit de la version 0.1 bien sûr, tout reste encore à construire… et j’ai
pas mal d’idées pour ça&nbsp;:).

## Sous le capôt

J’ai pas mal tergiversé avant de me décider sur les technologies à utiliser
pour développer Project Zero. Voulant au départ découvrir de nouvelles choses,
je partais sur un backend [Django](http://djangoproject.com/), le frontend en
[Elm](http://elm-lang.org/) et une API à base de [GraphQL](http://graphql.org/).

Mais plus j’hésitais plus je me convainquais que j’allais accumuler les
difficultés et que je n’avancerais pas. Au final j’ai décidé de partir sur des
bases que je connais et avec lesquelles je suis à l’aise&nbsp;:

- [Ruby on Rails](http://rubyonrails.org/) pour le backend comme cela fait
  presque 1 an et demi que j’en fais quotidiennement, pas de difficulté à ce
  niveau&nbsp;;
- une API classique en Json, je savais que j’avais encore besoin de progresser
  là-dessus mais je n‘exclue pas d’inclure du GraphQL à terme&nbsp;;
- [VueJS](https://vuejs.org/) pour le frontend, pour le coup que je ne
  connaissais pas mais qui n’est pas trop éloigné conceptuellement de React.

Pour ce qui est de VueJS, la lecture de la documentation m’a convaincu que tous
les problèmes que j’avais avec React allaient disparaître comme par magie… et
ce n’est pas tout à fait faux&nbsp;:). Bien sûr d’autres questions se sont
posées au fur et à mesure que j’avançais, mais je suis tout de même
content du résultat&nbsp;! L’époque où j’écrivais du JS imbitable sur FreshRSS
est révolue&nbsp;:D.

Comme je le disais plus haut, le code est hébergé [sur GitHub](https://github.com/marienfressinaud/project-zero)
mais il n’est pas exclu que je le déplace sur [Framagit](http://framagit.org/).
Ce qui me retient de le faire actuellement est de deux ordres&nbsp;:

- j’ai dans l’espoir de trouver une troisième solution. Le but de Framasoft
  n’étant pas de tout centraliser sur ses serveurs, j’aimerais aller voir
  ailleurs et de préférence chez un [chaton](https://chatons.org/) local&nbsp;;
- GitHub reste à des années lumières de Gitlab au niveau expérience
  utilisateur. Gitlab a beau avoir des fonctionnalités (très) intéressantes,
  son interface reste foutraque et j’ai du mal à m’y faire.

## Le service Project Zero

J’expliquais plus haut que j’ai mis à disposition un service à l’adresse
[zero.marienfressinaud.fr](http://zero.marienfressinaud.fr). Si l’adresse est
temporaire, j’ai dans l’espoir que le service ne le soit pas. Je ne me fais pas
trop d’illusions, actuellement Project Zero est beaucoup trop limité
fonctionnellement pour être utile à qui que ce soit, mais je pense qu’à terme
il pourrait plaire à quelques-uns.

Attention, le service est aujourd’hui ouvert à n’importe qui et gratuit, mais
je n’exclu pas à terme de **le rendre payant**. On ne pourra pas dire que je
n’ai pas prévenu&nbsp;! Bien sûr le logiciel restera sous licence libre et je
ne prévois absolument pas de version alternative «&nbsp;closed-source&nbsp;».
L’idée serait plutôt de rembourser les coûts du serveur et de mettre un petit
quelque chose de côté pour me donner un peu plus de temps libre, absolument pas
de faire fortune.

## Mes idées pour la suite

Voici une liste non-exhaustive de ce que j’ai en tête pour le moment&nbsp;:

- gestion de listes de tâches à faire (communément appelées «&nbsp;todo
  lists&nbsp;»)&nbsp;;
- gestion d’environnements pour distinguer les projets personnels des projets
  professionels&nbsp;;
- envoi de mails de rappels&nbsp;;
- gestion de sous-projets&nbsp;;
- client en ligne de commande pour notamment synchroniser des dossiers en
  local, ça permettrait d’avoir par exemple un dépôt Git en local associé à un
  projet&nbsp;;
- gestion du profil utilisateur et projets publics.

Ma priorité numéro 1 va être la gestion des tâches. J’utilise actuellement
[Todoist](https://todoist.com) et j’en suis vraiment très satisfait. Ceci dit
il y a parfois des petits bugs et je n’aime pas la façon dont il gère le
système de karma qui pousse parfois à bacler des tâches seulement pour garder
de bonnes statistiques. Et puis c’est toujours rigolo de redévelopper des
choses qui existent déjà, non&nbsp;?&nbsp;:). Je cogite encore pour réfléchir à
comment m’y prendre et ne pas faire quelque chose de trop bâteau.
