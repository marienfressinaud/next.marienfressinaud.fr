Date: 2016-09-14 20:00
Title: Projet Zéro/README.md
Slug: projet-zero-readme-md
Tags:
Summary: J’ai toujours eu beaucoup de mal à canaliser mes idées de projets et à les terminer. Je présente ici ce qui m’aide aujourd’hui à me reconcentrer&nbsp;: un carton.

Cela fait bien longtemps que je n’ai rien publié sur ce site. En toute logique
et parce que je suis fait de plein de contradictions, j’ouvre aujourd’hui une
nouvelle catégorie sur celui-ci&nbsp;: **Projet Zéro**.

Avant de présenter cette catégorie et de m’auto-persuader que je vais l’alimenter, un peu de contexte s’impose.

En septembre de l’année dernière, après un joli petit tour d’Europe, j’ai
rejoint la société de services Sogilis (attendez, [ce n’est pas ce que vous
croyez&nbsp;!](http://sogilis.com/blog/sogilis-vs-ssii/)). Rejoindre cette boîte a
été l’occasion pour moi d’apprendre beaucoup de choses&nbsp;: Ruby et Rails, JavaScript
via React, approfondir mes connaissances de Git, me mettre à Vim sous
l’impulsion des [TupperVims](http://tuppervim.org/), gratouiller du Go, apprendre et mettre en place pas
mal de bonnes pratiques dans différents domaines, etc. Forcément, engranger
toutes ces choses m’a donné des idées de projets et des envies d’améliorer
ceux existants (si si, j’envisage toujours de revenir sur FreshRSS pour le
faire évoluer&nbsp;:)). À côté de ça j’ai eu envie d’apprendre à dessiner, j’ai
toujours des projets de nouvelles que je souhaite publier sur ce site et j’ai
réussi à me motiver à aller courir régulièrement pour me remettre en forme.
Si on n’oublie pas le fait que j’essaye toujours de contribuer aux actions de
Framasoft en tant que bénévole, vous comprendrez qu’à un moment je ne savais
plus où donner de la tête.

Il y a donc un peu moins d’un mois, après m’être dispersé une nouvelle fois
dans un énième projet qui n’aboutira jamais, je me suis amusé à compter les
différents projets que j’avais en tête. Après un court brainstorming pour
coucher sur papier l’ensemble des choses que j’avais en tête, je finissais avec
une grosse vingtaine de projets&nbsp;: des voyages, des nouvelles, des
contributions à des logiciels opensource, d’autres plus personnels, etc. Un
joyeux fourre-tout que j’ai entièrement rangé… dans un carton. À ce carton j’ai
assorti quatre règles simples&nbsp;:

- je n’ai pas le droit de toucher ou d’avancer les projets qui sont dedans&nbsp;;
- j’ai le droit d’en sortir trois maximum à la fois&nbsp;;
- un projet sorti du carton ne peut pas y retourner avant une date limite que
  j’ai fixée au préalable (ou alors je dois l’avoir terminé)&nbsp;;
- tout nouveau projet "méga-top-génial" doit le rejoindre.

Ce carton, c’est ma manière de m’empêcher de me disperser dans tous les sens.
Ça fait maintenant un mois que j’ai mis cette méthodologie en place, je manque
évidemment encore de recul dessus (j’ignore si cela va durer ou si cela est
même efficace) mais j’en suis pour le moment entièrement satisfait.

Le plus intéressant dans la mise en place de ce carton c’est que je me suis rendu
compte que j’avais en fait réalisé une première version minimale d’un projet
qui se trouvait au sein même du carton&nbsp;! Son petit nom, c’est
«&nbsp;Projet Zéro&nbsp;».

Projet Zéro part du constat que j’ai toujours trop de projets en parallèle&nbsp;: il
me fallait donc un projet pour les gérer tous <strike>et dans les ténèbres les
lier</strike>. Projet Zéro c’était le premier projet que je me devais de terminer…
Devant la tâche immense qui m’attendait, je n’ai néanmoins jamais trouvé le
courage de m’y atteler. Son développement a été repoussé de semaines en
semaines et les mois se sont transformés en années.

Alors cette première version de Projet Zéro n’est pas vraiment un logiciel que
j’ai développé, mais il ne s’agit pas non plus d’un carton physique. Projet
Zéro pour le moment, c’est un banal système de fichiers&nbsp;:

```bash
marien@pizza ~$ tree -L 1 ProjetZéro/
ProjetZéro/
├── Blog
├── Carton
├── Dessin
├── SogiMood
└── Sport

5 directories, 0 files
```

On y trouve 5 répertoires&nbsp;:

- `Carton/` est mon carton numérique contenant tous les autres projets (de simples
  fichiers texte)&nbsp;;
- `Dessin/`, `SogiMood/` et `Sport/` sont mes trois projets en cours. Chacun contient un
  fichier `README.md` qui décrit le projet, donne des liens utiles et fixe une
  date limite de fin ainsi qu’un objectif (celui qui me fera dire «&nbsp;j’ai fini
  ce projet&nbsp;»)&nbsp;;
- `Blog/` est un raccourci vers les articles du blog. Je me suis dit que ce serait
  une bonne idée, à chaque fois que j’atteindrai un objectif (i.e. à chaque fin
  d’un projet), de partager mon expérience sur le blog. Il ne s’agit donc pas
  vraiment d’un projet mais d’une tâche transversale me servant à poser une
  vraie conclusion à mes projets.

Projet Zéro c’est donc pour moi le début d’une expérimentation qui se base sur
trois points&nbsp;:

- un carton contenant l’ensemble de mes projets et restreint par les quatre règles
  citées plus haut&nbsp;;
- trois projets maximum en dehors du carton avec chacun un objectif ainsi
  qu’une date limite de fin&nbsp;;
- un article de blog à chaque fin de projet.

Le prochain article ne devrait d’ailleurs pas trop tarder&nbsp;!
