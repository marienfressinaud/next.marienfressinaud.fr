---
title: Une histoire de pizzas : la grille
date: 2019-10-24 13:30
---

En début d’année, j’ai passé du temps à développer un mini-jeu pour les
[Ergogames](https://www.ergogames.fr). Le principe des Ergogames est simple :
faire découvrir des principes d’ergonomie à travers des jeux qui illustrent
différents concepts. Ceux-ci sont conçus par deux anciennes collègues
ergonomes, [Marion](https://twitter.com/mgeorges_) et [Margaux](https://twitter.com/Margauxlergo),
via l’agence de design [uxShadow](https://twitter.com/UxShadow) de [Sogilis](https://sogilis.com)
(mon ancienne boite).

Elles m’ont demandé si je pouvais leur donner un coup de main et j’ai accepté
à condition que le code soit ouvert ([ça se passe ici](https://gitlab.com/sogilis/ergogames.fr) 😊).

J’ai donc développé le mini-jeu « [À vous les pizzas !](https://www.ergogames.fr/game/pizzas) »
en précisant bien que je n’avais jamais conçu de jeux mais que c’est pas grave,
« je vais apprendre ». Du coup j’ai effectivement appris, et c’est ce
cheminement que j’aimerais partager maintenant à travers une série d’articles.
Je préviens tout de suite : je n’ai aucune idée de si je finirai cette série,
mais enfin, je l’aurai au moins commencée. Si vous cherchez le code du jeu en
lui-même, [il se trouve ici](https://gitlab.com/sogilis/ergogames.fr/tree/master/src/views/games/pizzas).

Le but de la série va être de redévelopper le jeu pas à pas. Je n’irai pas
aussi loin que celui des Ergogames pour me simplifier la vie, mais nous verrons
quand même les principaux mécanismes. Le principe du jeu est de déplacer un
personnage (Meiko) dans une cuisine en cliquant de case en case pour lui
faire fabriquer une pizza. Je vous laisse jouer au jeu sur le site des
[Ergogames](https://ergogames.fr) pour découvrir le principe d’ergonomie qui
est illustré ici. Le résultat ressemble à cela :

![Capture d’écran du jeu montrant Meiko au milieu d’une cuisine](images/ergogames/jeu.png)

Le jeu sera développé en HTML, CSS et JavaScript avec la librairie [VueJS](https://vuejs.org)
afin de rester le plus proche possible de ce qui a été fait initialement.
J’éviterai tout de même toute étape de <em lang="en">build</em> qui
consisterait à transformer le code <abbr>JS</abbr> en un seul fichier
optimisé : c’est ce qui est fait pour les Ergogames mais ça complexifierait la
compréhension sans rien apporter de plus. Je vais également m’éviter quelques
bonnes pratiques en mixant allègrement tout le code (HTML, JS et CSS) au sein
d’un même fichier (ne faites pas ça chez vous).

Dans ce premier article qui servait essentiellement à introduire la série, on
va se contenter d’afficher un cadre de 6 par 6 cases. Comme je l’ai dit plus
haut, je n’avais encore jamais développé de jeu, donc j’ai avancé de manière
très naïve. Il me semblait évident que la première phase consisterait à
dessiner le tableau de jeu.

Cela permet en plus d’introduire à la fois un peu de HTML, de CSS et de JS et
donc de poser les bases. Pour représenter le cadre, on va se contenter de 6
balises `<div>` pour les lignes, découpées elles-mêmes en 6 autres balises
`<div>` pour les cellules. On va s’éviter tout de suite de copier-coller plein
de code HTML et on va donc ajouter VueJS immédiatement et répéter ces blocs
avec des boucles `v-for`. Ça donne quelque chose que j’espère simple :

```html
<div id="app">
    <div class="board">
        <!--
            la variable size est passée par VueJS et a pour valeur 6. On répète
            donc 6 fois la div "board-line", au sein desquelles on répète 6
            fois la div "board-cell".
        -->
        <div v-for="y in size" :key="y" class="board-line">
            <div v-for="x in size" :key="x" class="board-cell">
            </div>
        </div>
    </div>
</div>

<!--
    J’ajoute la librairie VueJS via un CDN, mais dans la vraie vie préférez
    héberger le code vous-même, c’est meilleur pour la vie privée de vos
    visiteurs et visiteuses.
-->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<script>
    const BOARD_SIZE = 6;

    // Un tout petit peu de JS pour dire à VueJS de "monter" l'application sur
    // la balise possédant l'id #app et passer la variable size. Je ne rentre
    // pas dans le détail ici, ce n’est pas très important.
    const app = new Vue({
        el: '#app',
        data: {
            size: BOARD_SIZE,
        },
    });
</script>
```

Avec ça, on a la base de notre code HTML, par contre ça ne ressemble à rien. On
va donc ajouter un zeste de CSS pour rendre le tout visuel :

```css
/*
    Ici on définit quelques variables CSS, c'est pas forcé mais ça aidera à
    la maintenance du code.
*/
#app {
    --cell-size: 128px;

    --color-floor: #fff;
    --color-floor-border: #ccc;
}

/*
    Une ligne est un conteneur flex, ça permet de se simplifier la vie pour
    aligner les cellules. La conséquence ici est que les .board-cell seront
    affichées en lignes et non pas en colonnes.
*/
.board-line {
    display: flex;
}

/*
    Puis on définit la taille des cellules ainsi qu’une couleur de fond et
    de bordure pour les mettre en évidence.
*/
.board-cell {
    width: var(--cell-size);
    height: var(--cell-size);

    background-color: var(--color-floor);
    border: 1px solid var(--color-floor-border);
}
```

Avec ça on a un cadre que l’on va pouvoir améliorer par petites touches pour
arriver au jeu final. Le résultat final [est visible ici](une-histoire-de-pizzas/etape1.html)
(n’hésitez pas à ouvrir la console de votre navigateur pour regarder le code).

J’ai essayé ici de détailler au mieux ce que je faisais pour faciliter la
compréhension du code afin de rendre ma série d’articles accessible au plus de
monde possible, en particulier aux débutant·es. N’hésitez pas à me contacter si
des choses ne sont pas claires !

Le prochain article [explique comment afficher les éléments dans le
jeu](une-histoire-de-pizzas-2.html).
