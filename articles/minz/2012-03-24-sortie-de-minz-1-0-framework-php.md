Date: 2012-03-24 22:53
Title: Sortie de MINZ 1.0 - framework PHP
Slug: sortie-de-minz-1-0-framework-php
Tags: MINZ, php
Summary: MINZ (ou Minz) est un framework PHP que j’ai développé de 2011 à 2013. C’est probablement le projet qui m’a le plus fait progresser dans mes méthodes de développement bien que le projet reste imparfait par bien des aspects. Le code est toujours disponible [sur GitHub](https://github.com/marienfressinaud/MINZ).

Et bien voilà, après quasiment un an de développement plus qu’irrégulier, j’ai le plaisir d’annoncer la version 1.0 de mon framework PHP : [MINZ Is Not Zend](https://github.com/marienfressinaud/MINZ).

## Pour la petite histoire

J’ai commencé à développer ce framework à la base dans un but totalement personnel. Ayant pratiqué un petit peu Zend durant ma dernière année de DUT informatique, sa lourdeur m’a vite agacé. Puis voulant voir comment développer un framework, je suis parti des idées de Zend pour me bâtir ma propre architecture et manipuler un peu plus de PHP.

Ainsi MINZ a été à la base de mon site personnel depuis tout ce temps, me permettant de tester à plus ou moins grande échelle les fonctionnalités dont j’avais besoin.

## La présentation tirée du (court) wiki

MINZ est un framework PHP, c’est-à-dire qu’il propose une architecture particulière pour l’écriture d’applications PHP. On peut le voir comme un squelette, et l’application comme les muscles, cerveaux, peau, etc. Ce framework repose lui-même sur la modélisation MVC (pour Model View Controller). Le modèle MVC permet de séparer logiquement les données (un utilisateur avec un nom, un prénom, un mot de passe par exemple), leur représentation (la façon dont on va les afficher) et leur traitement. Cela permet d’avoir une application facile à maintenir.

MINZ est fortement inspiré de [Zend Framework](http://framework.zend.com/), qui est un autre framework PHP. Bien qu’inspiré, il s’en éloigne par bien des aspects, d’où son nom en clin d’oeil : MINZ Is Not Zend. Il se veut notamment bien plus léger et plus facile à mettre en place afin de faciliter le déploiement d’applications MINZ. De par ce soucis de légèreté, il existe quelques contraintes qui le rendent moins puissant, ce qui n’est pas vraiment un soucis pour des applications simples (un blog, un site ou une galerie photos par exemple). Si vous connaissez déjà Zend Framework, il est certain que vous y trouverez de nombreuses similitudes, notamment au niveau de l’architecture.

Le code de MINZ est sous licence [AGPL 3](https://www.gnu.org/licenses/agpl.html).

## Les fonctionnalités en vrac

- Routage des urls avec url rewriting géré par PHP. Cela permet de simplifier le processus de réécriture d’urls sans se prendre la tête avec Apache. En contrepartie, le système est moins puissant, même si j’ai pensé à quelques améliorations, qui ne verront sans doute jamais le jour...
- En complément du routage, j’ai mis en place un système d’écriture d’urls. Ainsi, on a la possibilité d’activer ou de désactiver l’url rewriting à la volée, sans casser les liens internes au site.
- L’historisation du parcours des visiteurs permet de garder un fil de leur visite, permettant de mettre en place facilement des liens de retour aux pages précédentes. Je ne suis pas bien sûr de conseiller cette fonctionnalité puisqu’elle est sujette à pas mal de difficultés de mise en place.
- La mise en cache des pages du site basé sur MINZ est gérée automatiquement par le framework sans avoir rien à faire.
- L’internationalisation des applications a aussi été prise en compte à travers une classe dédiée permettant de mettre facilement en place différentes langues pour le site.
- Un système a été développé pour faciliter la pagination de listes (d’articles de blog par exemple)
- Et enfin, une classe pour logguer les erreurs, warnings et autres actions nécessitant d’être logguées ;)

## Et la suite ?

Aujourd’hui j’ai sorti la version 1.0 après plus d’un mois en version alpha... J’avoue que j’ai pris mon temps : j’attendais d’avoir terminé la documentation (sous licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/)), et ça ne me motivait plus du tout. Alors la suite, je ne pense pas faire de gros changements au framework, seulement des mises à jour de bugs. Même si une roadmap est en place sur la page GitHub dédiée, ce qui y est indiqué ne verra sans doute pas le jour. Sauf si quelqu’un a envie de se lancer là-dedans ;)

À vrai dire je commence à en avoir marre de développer uniquement des applications web, et j’ai envie de passer à autre chose. J’ai d’autres projets qui cogitent, et j’ai envie de me lancer là-dedans.

## Au final et en guise de conclusion

Je suis vraiment content d’en être arrivé là avec mon "semblant de framework" comme je l’appelais il y a un an. Au départ c’était totalement un projet pour moi-même, pour voir ce dont j’étais capable et en étant persuadé que j’arrêrais 1 mois plus tard. C’est mon premier vrai gros projet qui peut avoir une réelle utilité. J’ai appris nettement plus de choses en développant mon framework que ce que tous les sites et profs ont pu essayer de m’apprendre en PHP (bien qu’ils m’aient appris beaucoup de choses !). Et puis ça fait toujours du bien d’arriver à mener un projet à son terme, surtout après un an de développement ☺
