Date: 2011-09-11 11:30
Title: Minz 0.3
Slug: minz-0-3
Tags: MINZ, php
Summary: MINZ (ou Minz) est un framework PHP que j’ai développé de 2011 à 2013. C’est probablement le projet qui m’a le plus fait progresser dans mes méthodes de développement bien que le projet reste imparfait par bien des aspects. Le code est toujours disponible [sur GitHub](https://github.com/marienfressinaud/MINZ).

Quelques jours seulement après la version 0.2, je sors cette version 0.3.

Quelles nouveautés alors ? Et bien deux gros morceaux que je voulais absolument implémenter et qui me limitaient un peu dans mes divers développement.

Tout d’abord, la réécriture d’url à travers un module de routes. Comment ça marche ? Et bien regardez l’url dans la barre de votre navigateur qui doit être ici du genre `http://marienfressinaud.fr/?c=blog`, ce qui signifie que le controller de l’application se nomme `blog`. Avec cette version, j’aurai la possibilité de transformer cette url en `http://marienfressinaud.fr/blog`. Ce n’est pas grand chose, et c’est facilement faisable avec un fichier `.htaccess`, mais j’avais la volonté de le faire directement à travers le framework. Le morceau de code qui fera ça est on ne peut plus simple :

```php
return array(
    array(
        'route'       => '/blog',
        'controller'  => 'blog',
        'action'      => 'index'
    )
);
```

avec possibilité de rajouter autant de routes que l’on veut évidemment ;) Ainsi, lorsque l’on regarde un article en particulier, par exemple à l’url `http://marienfressinaud.fr/?c=blog&a=voir&id=62`, on peut rajouter un peu de code :

```php
return array(
    array(
        'route'       => '/blog',
        'controller'  => 'blog',
        'action'      => 'index'
    ),
    array(
        'route'       => '/blog/voir',
        'controller'  => 'blog',
        'action'      => 'voir'
    )
);
```

et l'url `http://marienfressinaud.fr/blog/voir?id=62` emmènera au même article. On voit d'ailleurs ici la limitation avec les urls un peu dynamiques, je ne gère pas encore les paramètres supplémentaires comme ici `id`, mais j'y pense ;)
Bon, je n'utilise pas encore cette nouvelle version sur ce site parce que ça demandera un peu plus de travail pour l'intégrer et que ma connexion internet ne me le permet pas :p

Deuxième fonctionnalité, aussi importante à mes yeux, l'ajout d'un bootstrap d'application. À quoi il sert celui-là ? En fait ce bootstrap pourra être modifié directement par le développeur pour automatiser certaines tâches. Par exemple, dans chacun de mes controllers, j'ai les lignes (entre autres)

```php
$this->view->prependStyle(DOMAIN.'/themes/default/base.css');
$this->view->prependScript(DOMAIN.'/scripts/jquery.js');
$this->view->user = new User();
```

ce qui devient très vite rébarbatif. Et bien ce bootstrap me permettra de les charger automatiquement grâce à sa méthode `run()`. De ce fait, je n’aurai qu’à l’écrire une fois, et tous mes controllers seront impactés donc il y a quand même un certain gain en temps ☺

J’espère que ça intéressera quelqu’un, surtout que d’un point de vue personnel, je trouve mon framework très bien et de plus en plus puissant (oui oui, je suis très objectif ! :D)
