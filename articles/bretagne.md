---
title: Bretagne
date: 2019-09-16 12:00
---

Je pars lundi prochain (le 23 septembre) pour un petit mois pédestre en
Bretagne. Initialement prévu pour l’automne 2017, j’ai réussi à repousser ce
voyage pendant deux ans pour diverses raisons, mais cette fois c’est bon !

Mon trajet, qui évoluera sans doute en cours de route, consistera en :

- un passage (express) à Rennes ;
- une quinzaine de jours à marcher entre le Mont-Saint-Michel et Saint-Brieuc ;
- un passage à Brest ;
- une petite dizaine de jours à marcher sur la presqu’île de Crozon ;
- peut-être un saut à Vannes, puis à Nantes, avant de rentrer.

Les plus attentifs noteront que je vais plus ou moins suivre [le sentier des
douaniers](https://www.mongr.fr/trouver-prochaine-randonnee/itineraire/gr-34-le-plus-maritime-des-sentiers-de-grande-randonnee).
Je partirai sans PC et serai donc difficilement joignable. Si vous êtes sur la
route et voulez faire un coucou ou marcher un bout de chemin, n’hésitez pas à
[me contacter](contact.html) avant que je ne parte.

Je profiterai de ce voyage pour finaliser mon discours et mes idées autour de
[Flus](https://flus.fr), ce serait d’autant plus intéressant de pouvoir
rencontrer des gens en cours de route !
