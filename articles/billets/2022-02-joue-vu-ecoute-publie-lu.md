---
title: Joué, vu, écouté, publié, lu (février 2022)
date: 2022-03-04 14:39
---

Joué à [Hadès](https://www.supergiantgames.com/games/hades) en pointillé. J’ai quelque 340 heures de jeu dessus, mais je continue d’y trouver du plaisir (et de nouvelles lignes de dialogues !) Vraiment un très bon jeu. Officiellement le jeu auquel j’ai le plus joué sur Switch, devant [<i lang="en">Zelda Breath of the Wild</i>](https://www.zelda.com/breath-of-the-wild/).

Vu « [Media Crash, qui a tué le débat public ?](https://blogs.mediapart.fr/media-crash/blog/030222/media-crash-qui-tue-le-debat-public-mediapart-au-cinema-partir-du-16-fevrier) » de Mediapart. Je n’ai pas l’impression d’avoir appris grand-chose, je ne sais pas si j’étais le public visé. Ça reste une analyse indispensable sur la concentration des médias.

Écouté Dido, Coldplay, Muse et Pink une bonne partie du mois : l’impression d’être retombé dans mes années lycée. C’est l’effet [Lollypop](lollypop.html) et c’était pas désagréable.

Publié « [Pour bâtir un morceau de Web](pour-batir-un-morceau-de-web.html) ». C’est un article qui me trottait dans la tête depuis un moment et j’en suis content. J’ai malgré tout eu du mal à l’écrire, avec notamment une réécriture de zéro.

Lu « [<i lang="en">Contributing to BookStack (And Open Source)</i>](https://www.bookstackapp.com/blog/contributing-to-open-source/) » (en anglais). C’est un bon guide qui fait le tour des différentes contributions possibles autour d’un projet open-source. 