---
title: Technique de décrochage : le fichier hosts
date: 2022-01-25 09:00
---

Certains sites mal conçus ont tendance à trop facilement voler mon attention à l’instar de Twitter et Twitch. Comme je suis très sensible à ce type de mauvais design, j’ai tenté plusieurs approches pour décrocher de ces sites. Le plus efficace à ce jour repose sur un détournement du fichier [`/etc/hosts`](https://fr.wikipedia.org/wiki/Hosts).

Pour faire court, il s’agit d’un fichier Linux permettant de faire correspondre des noms de domaine avec des adresses IP sans passer par un serveur [<abbr>DNS</abbr>](https://fr.wikipedia.org/wiki/Domain_Name_System). Pour ma part, je fais pointer les noms de domaine des sites que je veux bloquer vers mon adresse <abbr>IP</abbr> locale (`127.0.0.1`). Ainsi, lorsque j’ouvre Twitch par exemple, je me retrouve avec une belle page d’erreur, me rappelant que j’avais pris la décision de ne plus y mettre les pieds.

La technique fonctionne bien car elle génère une friction importante pour la contourner, c’est-à-dire : ouvrir un terminal, taper la commande pour éditer le fichier et rentrer mon mot de passe. Par contre, elle ne fonctionne pas bien pour Twitter dans mon cas : j’ai besoin d’y accéder régulièrement [pour Flus](https://flus.fr). Ça fait partie des raisons qui m’ont poussé à [y fermer mon compte personnel](quitter-twitter.html).
