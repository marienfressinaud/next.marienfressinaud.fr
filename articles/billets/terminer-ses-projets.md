---
title: Terminer ses projets
date: 2022-01-28 10:22
tags: featured
---

L’année dernière, j’ai commencé à clore un certain nombre de logiciels que j’ai écrit dans le passé. C’est le cas de [Boop!](https://framagit.org/marienfressinaud/boop), mon générateur de sites statiques, ou encore mon sélecteur de couleurs [<i lang="en">Sweet Colors</i>](https://github.com/marienfressinaud/Sweet-colors).

Pour l’instant, j’ai adopté deux stratégies pour les clore. La première consiste à simplement prévenir que le logiciel n’est plus du tout maintenu ; c’est simple et explicite. La seconde stratégie consiste à annoncer qu’aucune nouvelle fonctionnalité ne sera ajoutée au logiciel, mais que je continuerai de le maintenir en cas de faille de sécurité ou de bug. Je pourrais également être amené à mettre la documentation à jour si je la trouve incomplète.

J’aimerais diffuser l’idée que terminer un logiciel peut être un signe de maturité du projet. Dans le cas de Boop!, par exemple, je considère que le projet a atteint le niveau fonctionnel que je lui souhaitais en le démarrant. Ça ne signifie pas qu’il répond à _tous_ les besoins, seulement qu’il est tel que je veux qu’il soit. **Il n’a pas besoin d’être amélioré parce qu’il répond aux besoins auxquels il était censé répondre. C’est un logiciel fonctionnellement stable.**

Terminer un projet est un acte positif.
