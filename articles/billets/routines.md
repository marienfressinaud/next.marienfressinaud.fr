---
title: Routines
date: 2022-04-25 20:15
---

L’année dernière, j’ai commencé à m’organiser avec des routines. Le principe est de me donner de courtes tâches à faire de manière régulière. Cela m’a permis de m’assurer que mes tâches administratives étaient bien faites, ou mes plantes bien arrosées par exemple.

Au début, tout était stocké dans un wiki. Il y a 2 mois et demi, j’ai poussé le principe un peu plus loin en les organisant dans un tableur. Ce changement m’a permis principalement d’ajouter la date de la dernière fois à laquelle j’effectue chaque tâche. Cette information, combinée à la fréquence (ex. 1, 7, 30 jours), permet d’afficher chaque jour les routines que j’ai à faire, ou celles qui prennent du retard.

Par rapport au wiki, ce système a plusieurs avantages :

- présentation plus adaptée, je vois toutes les routines en un coup d’œil, c’est plus agréable ;
- facile d’accès (2 clics) et accessible hors-ligne, je l’ai donc toujours sous les yeux et je ne l’oublie pas ;
- facile de modifier, je peux ajouter (ou supprimer) plus aisément de nouvelles routines, j’y gère donc plus de choses ;
- le tableur me demande d’être actif pour modifier les dates, je suis donc amené à l’utiliser quotidiennement ;
- en récompense visuelle, le tableur me montre en vert ce que j’ai fait dans la journée, ça me motive à réaliser mes routines.

Je ne crois généralement pas aux solutions magiques d’organisation, mais je suis particulièrement content de ce système. En revanche, il ne permet pas de se motiver à faire les choses pour lesquelles on traîne des pieds (comprendre « ça ne m’a pas motivé à [publier tous les jours](publier-souvent.html) »).

En bref, je n’aurai jamais autant pris soin de mon dos, nettoyé mon appartement ou appelé mes parents que ces derniers mois. C’est pas mal, non ?
