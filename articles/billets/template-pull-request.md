---
title: Template de pull requests
date: 2022-02-01 22:56
---

Je partage ici le <i lang="en">template</i> de <i lang="en">pull requests</i> (<abbr>PR</abbr>) que j’utilise pour [flusio](https://github.com/flusio/flusio), légèrement adapté et traduit. Son but est de me faire vérifier plusieurs points critiques avant de fusionner mon code dans la branche principale. Son contenu est rempli automatiquement lorsque j’ouvre une <abbr>PR</abbr> et je m’interdis de l’intégrer tant que chacun des points n’a pas été validé. Elle m’est d’autant plus importante que je travaille seul : je n’ai personne pour vérifier derrière moi que je n’oublie rien. J’imagine qu’elle pourrait être utile à d’autres.

---

Liste de vérifications :

- [ ] le code a été testé manuellement
- [ ] l’interface fonctionne sur mobile et grands écrans
- [ ] l’accessibilité a été testée
- [ ] les tests ont été mis à jour
- [ ] la traduction française a été synchronisée
- [ ] la documentation a été mise à jour (incluant les commentaires, messages de commit, notes de migrations…)

_Si l’un de ces points n’est pas applicable à la <abbr>PR</abbr>, merci de quand même cocher la case et d’ajouter `(N/A)` à la fin de la ligne._

---

Pour ajouter un <i lang="en">template</i> de <abbr>PR</abbr> à votre dépôt de code, vous pouvez suivre les instructions officielles pour [GitHub](https://docs.github.com/en/communities/using-templates-to-encourage-useful-issues-and-pull-requests/creating-a-pull-request-template-for-your-repository) ou pour [GitLab](https://docs.gitlab.com/ee/user/project/description_templates.html).
