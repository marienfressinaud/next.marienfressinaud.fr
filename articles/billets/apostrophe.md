---
title: Apostrophe, éditeur Markdown
date: 2022-01-26 20:10
---

Depuis quelques mois, j’utilise un éditeur Markdown pour rédiger mes articles et autres notes : [Apostrophe](https://world.pages.gitlab.gnome.org/apostrophe/).

Pour éditer du texte, j’utilise (Neo)Vim depuis des années, mais je ne l’ai jamais trouvé agréable pour rédiger des articles. C’est un <i lang="en">feeling</i> global que je n’arrive pas entièrement à expliquer.

Apostrophe me fournit une interface épurée. Le peu de menu se masque dès lors que je commence à taper du texte. De plus, les quelques options de configuration sont très peu nombreuses, ce qui est parfait pour mon usage. Enfin, j’apprécie fortement le mode focus qui permet de se concentrer sur la phrase que l’on écrit. Son plus gros défaut est peut-être qu’il ne permet d’ouvrir qu’un seul document à la fois… mais, à l’usage, ça en fait une fonctionnalité plutôt intéressante.

C’est en tout cas un éditeur de texte au parti pris très assumé, il ne conviendra certainement pas à tout le monde. Moi, je l’aime bien :)

![Capture du texte de cet article ouvert dans Apostrophe](images/apostrophe.png)