---
title: Logiciel libre et gratuit
date: 2022-03-29 15:33
---

On lit ou entend régulièrement[^1] qu’il y a confusion entre logiciel « libre » et « gratuit ». Il est alors généralement précisé qu’un logiciel libre n’est pas forcément gratuit, que cela viendrait du double sens en anglais du terme « <i lang="en">free</i> » (signifiant à la fois « libre » et « gratuit »).

[^1]: J’abuse un peu parce que je ne le lis plus tant que ça. Je l’ai seulement revu récemment — je ne sais plus où – et ça me trotte dans la tête depuis.

**Si je comprends la confusion dans le monde anglophone, cela ne l’explique pas ailleurs où le double sens n’existe pas.**

Hypothèse toute bête : si le grand public se méprend souvent sur le caractère _nécessairement_ gratuit du logiciel libre, c’est tout simplement que les logiciels libres le sont dans leur très grande majorité.

Réflexion supplémentaire : la gratuité ne facilite-t-elle pas la liberté en garantissant l’accès aux logiciels pour les budgets les plus serrés ? Ne ferait-on pas mieux de revendiquer ce caractère plutôt que de s’en défendre ?