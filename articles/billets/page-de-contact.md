---
title: Courriel et page de contact
date: 2022-04-11 18:15
---

Il y a 2 mois, une entreprise travaillant dans le <abbr>RGPD</abbr> m’a informé avoir collecté mon adresse courriel personnelle.

Je les ai donc contactés pour savoir pourquoi et comment ils l’avaient collectée. Ils m’ont répondu créer un annuaire de sociétés françaises à partir de données publiques à des fins marketing. Ils m’ont également précisé que « [mon] adresse commençant par le mot "contact", cette dernière rentre dans le cadre des adresses email génériques ».

**Mon adresse se terminant en "@prénomnom.fr", elle n’est évidemment pas du tout générique.** De plus, le concept d’adresse générique ne s’applique que dans un cadre <i lang="en">B to B</i> ; jusqu’à preuve du contraire, je ne suis pas une entreprise[^1].

Pour m’assurer que les choses soient claires, j’ai donc créé [une page de contact](contact.html) qui indique mon adresse tout en précisant que je refuse le démarchage commercial et que je refuse qu’elle soit stockée. J’ai également pris soin de virer l’adresse de toutes les autres pages du site afin qu’elle ne soit pas séparée de mes conditions.

Je ne suis pas naïf : ce genre d’entreprises collecte les adresses automatiquement et ne vérifient rien du tout manuellement. Mais je pourrai au moins leur mettre le nez dans leur caca désormais.

[^1]: En revanche ma micro-entreprise porte mon nom, ce que je trouve hyper relou. D’un point de vue <abbr>RGPD</abbr>, c’est naze.
