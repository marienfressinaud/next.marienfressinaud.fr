---
title: Publier sa veille Flus sur Mastodon
date: 2022-09-19 18:15
---

Aujourd’hui, je vous explique comment publier automatiquement un flux <abbr>RSS</abbr> (ou Atom) sur Mastodon. Cet article pointe quelques particularités liées à [Flus](https://flus.fr)[^1], mais la majorité des informations peuvent s’appliquer à n’importe quel flux.

[^1]: Je pensais initialement publier cet article dans le [carnet de Flus](https://flus.fr/carnet). Finalement, la méthode que je présente ici étant trop technique à mon goût et nécessitant l’utilisation de la ligne de commande, je me rabats sur mon blog perso.

Pour publier un flux sur Mastodon, vous avez besoin de 3 ingrédients :

1. un flux <abbr>RSS</abbr> (ici je vais prendre comme exemple le flux de mon profil Flus : `https://app.flus.fr/p/1670839367044869607/feed`) ;
2. un compte [Mastodon](https://joinmastodon.org/) (forcément) ;
3. et un outil qui va lire votre flux pour publier sur votre compte Mastodon : [**feediverse**](https://github.com/edsu/feediverse)[^2].

[^2]: Il existe aussi [feed2toot](https://gitlab.com/chaica/feed2toot/), mais je trouve son installation beaucoup plus compliquée.

feediverse étant un outil en ligne de commande, vous devrez vous sentir suffisamment à l’aise avec cette dernière. Eh oui, cet article est un peu technique.

## Installer feediverse

Pour installer feediverse, vous aurez besoin de [Python](https://www.python.org/) et de la commande `pip`. Vérifiez que `pip` est bien installé avec la commande :

```console
$ python -m pip --version
```

Si tout va bien, il vous affichera son numéro de version. En principe `pip` est disponible par défaut dans les dernières versions de Python. Si ce n’est pas le cas, vous pouvez suivre [la documentation pour l’installer.](https://pip.pypa.io/en/stable/installation/) 

Une fois cela fait, installez feediverse :

```console
$ python -m pip install feediverse
```

Puis configurez-le :

```console
$ feediverse
```

Il va vous demander un certain nombre d’informations ; notamment votre instance / serveur Mastodon, de quoi générer un jeton d’authentification et… l’adresse de votre flux <abbr>RSS</abbr>.

## Configurer le format des messages

Une fois cela fait, c’est presque terminé ! Vous aurez toutefois peut-être envie de configurer le format des messages postés sur Mastodon. Ouvrez le fichier de configuration `~/.feediverse` dans un éditeur de texte et adaptez la propriété `template` à votre convenance. Par exemple :

```yaml
feeds:
- template: "{title} {link} #veille"
  url: https://app.flus.fr/p/1670839367044869607/feed
```

Les liens (i.e. `{link}`) pointeront vers la page de vos commentaires Flus. Si vous préférez toutefois que les liens pointent directement vers les sites, vous pouvez modifier l’<abbr>URL</abbr> de votre flux en ajoutant le paramètre `?direct=true` (exemple : `https://app.flus.fr/p/1670839367044869607/feed?direct=true`).

## Exécuter feediverse

Il ne vous reste plus qu’à lancer feediverse pour qu’il poste sur Mastodon :

```console
$ feediverse
```

Pour l’exécuter automatiquement à intervalle régulier, créez une tâche Cron (`crontab -e`) :

```crontab
*/15 * * * * /home/USER/.local/bin/feediverse
```

Vous devrez probablement mettre le chemin absolu vers la commande (comme dans l’exemple ci-dessus). Vous le trouverez avec cette commande :

```console
$ whereis feediverse
feediverse: /home/USER/.local/bin/feediverse
```

## Bonus : Publier les liens Flus ET directs

Si vous voulez publier à la fois les liens vers vos commentaires Flus et vers les articles pointés, il existe une méthode. Mais attention, il va falloir se relever les manches du clavier et traficoter directement dans le code de feediverse[^3].

[^3]: J’ai ouvert une [<i lang="en">pull request</i>](https://github.com/edsu/feediverse/pull/30) pour que la modification soit intégrée directement dans feediverse, mais le projet semble en sommeil.

Commencez par trouver où il a été installé :

```console
$ python -m pip show feediverse | grep Location
Location: /home/USER/.local/lib/python3.10/site-packages
```

Ouvrez ensuite le fichier `/home/USER/.local/lib/python3.10/site-packages/feediverse.py` dans un éditeur de texte.

Trouvez la ligne `def get_entry(entry):` (ligne 85 chez moi), puis, quelques lignes plus bas, le `return { … }`. Entre les accolades, ajoutez cette ligne :

```python
return {
    # ...
	"links": entry.links,
}
```

Éditez maintenant le format de votre message (dans `~/.feediverse`) :

```yaml
feeds:
- template: "{title}\nLien : {links[1].href}\nCommentaires : {links[0].href}\n#veille"
  url: https://app.flus.fr/p/1670839367044869607/feed.atom.xml
```

Veillez à bien supprimer le paramètre `?direct=true` de l’adresse du flux.

Normalement, les prochains messages postés par feediverse sur votre compte Mastodon devraient contenir un lien vers Flus et un lien vers les articles pointés.
