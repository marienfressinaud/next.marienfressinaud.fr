---
title: Farandole de projets
date: 2023-02-10 08:30
---

**Je multiplie les projets ces derniers jours.**
[Exploratrices](https://framagit.org/marienfressinaud/exploratrices/) dont j’ai parlé [ici](exploratrices.html).
[Hitchhiker](https://framagit.org/marienfressinaud/hitchhiker/) dont j’ai parlé [sur le carnet de Flus](https://flus.fr/carnet/hitchhiker-generateur-de-planets-statiques.html).
[Sonktionary](https://framagit.org/marienfressinaud/sonktionary/) dont je n’ai pas encore parlé.
[Hermès](https://framagit.org/marienfressinaud/hermes) sur lequel je travaille actuellement.
Sans compter [Farandole](https://framagit.org/marienfressinaud/farandole) que je n’ai pas encore commencé.

_Ça fait franchement du bien d’expérimenter autant de choses._

Pour Exploratrices, c’était la spécification complète du projet avant de démarrer, ainsi qu’une approche technique très différente de ce dont j’ai l’habitude.

Pour Hitchhiker, c’était la contrainte minimaliste : faire autant que possible avec aussi peu de lignes de code que je pouvais me permettre.

Sonktionary : un projet rigolo et fonctionnel démarré en milieu de soirée sur un coup de tête.

Avec Hermès, je découvre [les attributs PHP](https://www.php.net/manual/language.attributes.php) et je complète largement mon framework, [Minz](https://github.com/flusio/Minz).
Je compte aussi explorer avec ce projet la dimension collaborative (et ses difficultés).

Je fais tout ça sans la contrainte de maintenance, car je ne développe que des prototypes de logiciels.
C’est une sorte d’exutoire pour se libérer l’esprit des projets qui me traînent dans la tête.

_Libre à vous de vous en emparer._
