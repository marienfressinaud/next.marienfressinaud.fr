# Sources de marienfressinaud.fr

Ce dépôt contient les sources de mon site personnel : [marienfressinaud.fr](https://marienfressinaud.fr)

Le site est généré à l’aide de [Boop!](https://framagit.org/marienfressinaud/boop),
mon générateur de sites statiques. Pour générer le site, il faut donc
l’installer au préalable.

Les commandes utiles pour générer et publier ce site sont déclarées dans le
fichier [`Makefile`](/Makefile).

## Travail en local

Pour avoir un aperçu global des fichiers sources :

```console
$ make tree
```

Pour générer le site en local :

```console
$ make boop
```

Pour ouvrir le site dans le navigateur :

```console
$ make open
```

Il est possible de générer facilement le site depuis Vim. À ajouter dans votre
`.vimrc` :

```vimrc
autocmd BufRead,BufNewFile */marienfressinaud.fr/* nnoremap <leader><leader> :w \| !boop.py --development<CR>
```

Pour générer le site, il suffit d’appuyer deux fois de suite sur la touche
`<leader>`. Pour ma part, il s’agit de la touche virgule (<kbd>,</kbd>).

## Mise en production

Avant de publier, il faut au préalable définir la variable d’environnement qui
va bien, dans un fichier `.env` par exemple :

```dotenv
SERVER_DEST=user@server.url:/path/to/website
```

Pour publier le site sur le serveur :

```console
$ make publish
```

Après la mise en production, [un serveur Websub](https://websub.flus.io/) est
notifié de la publication de nouveau contenu.

## Licence

Sauf mention contraire, le contenu du site est placé sous licence [CC BY
4.0](https://creativecommons.org/licenses/by/4.0/). Vous pouvez me créditer en
indiquant l’adresse de mon site ([marienfressinaud.fr](https://marienfressinaud.fr)).
